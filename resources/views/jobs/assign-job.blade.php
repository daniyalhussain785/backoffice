@extends('layouts.admin')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="content-body">
    <section class="users-view">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <h3 class="content-header-title mb-0 d-inline-block mb-1">Order Details (ID: {{$order->order_id}})</h3>
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>Order ID:</th>
                                        <td>{{$order->order_id}}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Address:</th>
                                        <td>{{$order->full_address}}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Date:</th>
                                        <td>{{date("l jS \of F Y h:i A", strtotime($order->create_at))}}</td>
                                    </tr>
                                    <tr>
                                        <th>Main Service:</th>
                                        <td>{{$order->service->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Services Name:</th>
                                        <td>
                                        @foreach($order->order_details as $order_details)
                                            {{ $order_details->service->name }} ({{$order_details->quantity}}) ,
                                        @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Order Status:</th>
                                        <td>
                                            <div class="badge badge-info">{{$order->status}}</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Total Amount:</th>
                                        <td>Rs. {{$order->total_amount}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body cleartfix">
                            <h3 class="content-header-title mb-0 d-inline-block mb-1">Customer Details</h3>
                            <a href="#" class="mr-1">
                                <div class="media align-items-center">
                                    <div class="align-self-center">
                                    @if($order->user->image != null)
                                    <img src="{{$order->user->image}}" alt="{{$order->user->fullName}}" class="users-avatar-shadow rounded-circle" height="100" width="100">  
                                    @else
                                    <img src="{{asset('images/noimage.png')}}" alt="{{$order->user->fullName}}" class="users-avatar-shadow rounded-circle" height="100" width="100">  
                                    @endif
                                    </div>
                                    <div class="media-body ml-1">
                                        <div class="upper-media-body">
                                            <h5>ID: <span>{{$order->user->regId}}</span></h5>
                                            <h5>Name: <span>{{$order->user->fullName}}</span></h5>
                                            <h5>Phone Number: <span>{{$order->user->phn_no}}</span></h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">  
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg nav-justified mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" id="active-tab1" data-toggle="tab" href="#interested" aria-controls="active1"
                                        aria-expanded="true">Interested Kamay</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="link-tab1" data-toggle="tab" href="#nearby" aria-controls="link1"
                                        aria-expanded="false">Near By (3 KM)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="linkOpt-tab1" data-toggle="tab" href="#linkOpt1"
                                        aria-controls="linkOpt1">All Kamay</a>
                                </li>
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane active" id="interested" aria-labelledby="active-tab1"
                                    aria-expanded="true">
                                    <div class="col-12">
                                        <div class="row">
                                            @foreach($order->interested as $interested)
                                            <div class="card col-4 col-md-4">
                                                <form action="{{route('unassigned-job-submit')}}" method="post" class="supervisor-form-box supervisor-form-box-white">
                                                    <input type="hidden" name="kama_id" value="{{$interested->kamay->regId}}">
                                                    <input type="hidden" name="order_id" value="{{$order->order_id}}">
                                                    <input type="hidden" name="customer_id" value="{{$order->user->regId}}">
                                                    @csrf
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body cleartfix">
                                                                <h4 class="mb-1">{{$interested->kamay->fullName}} <span class="main-star"></span>
                                                                </h4>
                                                                <div class="media align-items-center">
                                                                    <div class="align-self-center">
                                                                        <a href="#" class="mr-1">
                                                                            @if($interested->kamay->image != null)
                                                                            <img src="{{$interested->kamay->image}}" alt="{{$interested->kamay->fullName}}" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                            @else
                                                                            <img src="{{ asset('images/noimage.png')}}" alt="{{$interested->kamay->fullName}}" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                            @endif
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="upper-media-body">
                                                                            <h5>ID: <span>{{$interested->kamay->regId}}</span></h5>
                                                                            <h5>Phone Number: <span>{{$interested->kamay->phn_no}}</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="upper-media-body mt-1 mb-1">
                                                                    
                                                                </div>
                                                                <h5>Address: <span>{{$interested->kamay->territory->name}}, {{$interested->kamay->city->name}}</span></h5>
                                                                <button type="submit" class="btn btn-primary mt-1">Assign job</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="nearby" role="tabpanel" aria-labelledby="dropdownOpt1-tab1"
                                    aria-expanded="false">
                                    <div class="col-12">
                                        <div class="row">
                                        @for($i = 0 ; $i < count($getNearByKamay); $i++)
                                            <div class="card col-4 col-md-4">
                                                <form action="{{route('unassigned-job-submit')}}" method="post" class="supervisor-form-box supervisor-form-box-white">
                                                    <input type="hidden" name="kama_id" value="{{$getNearByKamay[$i]['cust_id']}}">
                                                    <input type="hidden" name="order_id" value="{{$order->order_id}}">
                                                    <input type="hidden" name="customer_id" value="{{$order->user->regId}}">
                                                    @csrf
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body cleartfix">
                                                                <h4 class="mb-1">{{$getNearByKamay[$i]['cust_name']}} <span class="main-star"></span>
                                                                </h4>
                                                                <div class="media align-items-center">
                                                                    <div class="align-self-center">
                                                                        <a href="#" class="mr-1">
                                                                        <img src="{{ asset($getNearByKamay[$i]['cust_image']) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="upper-media-body">
                                                                            <h5>ID: <span>{{$getNearByKamay[$i]['cust_id']}}</span></h5>
                                                                            <h5>Phone Number: <span>{{$getNearByKamay[$i]['cust_number']}}</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="upper-media-body mt-1 mb-1">
                                                                    <h5>Address: <span>{{$getNearByKamay[$i]['terr_name']->name}}, {{$getNearByKamay[$i]['city_name']->name}}</span></h5>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-primary mt-1">Assign job</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endfor
                                        </div>
                                      </div>
                                </div>
                                <div class="tab-pane" id="linkOpt1" role="tabpanel" aria-labelledby="linkOpt-tab1" aria-expanded="false">
                                    <div class="col-12">
                                        <div class="row">
                                        @foreach($allKamay as $getkamas)
                                            <div class="card col-4 col-md-4">
                                                <form action="{{route('unassigned-job-submit')}}" method="post" class="supervisor-form-box supervisor-form-box-white">
                                                    <input type="hidden" name="kama_id" value="{{$getkamas->cust_id}}">
                                                    <input type="hidden" name="order_id" value="{{$order->order_id}}">
                                                    <input type="hidden" name="customer_id" value="{{$order->user->regId}}">
                                                    @csrf
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body cleartfix">
                                                                <h4 class="mb-1">{{$getkamas->cust_name}} <span class="main-star"></span>
                                                                </h4>
                                                                <div class="media align-items-center">
                                                                    <div class="align-self-center">
                                                                        <a href="#" class="mr-1">
                                                                        <img src="{{ asset($getkamas->cust_image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="upper-media-body">
                                                                            <h5>ID: <span>{{$getkamas->cust_id}}</span></h5>
                                                                            <h5>Phone Number: <span>{{$getkamas->cust_number}}</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="upper-media-body mt-1 mb-1">
                                                                    
                                                                </div>
                                                                <h5>Address: <span>{{$getkamas->terr_name->name}}, {{$getkamas->city->name}}</span></h5>
                                                                <button type="submit" class="btn btn-primary mt-1">Assign job</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('scripts')
@endpush