@extends('layouts.admin')
   
@section('content')
     
       
          
        <div class="row">


          <div id="recent-sales" class="col-12 col-md-12">

           @if($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{$message}}</p>
          </div>
         @endif

          <form action="{{route('jobs.jobs-report-range')}}" method="POST" enctype="multipart/form-data" >
             @csrf 
          <div class="row">
            
              <div class="col-md-4">
                <input type="date" name="from_date"  class="form-control" placeholder="From Date"  />
              </div>
              <div class="col-md-4">
                  <input type="date" name="to_date"  class="form-control" placeholder="To Date"  />
              </div>
              <div class="col-md-4">
                  <button type="submit" name="filter" class="btn btn-primary">Filter</button>
                  <br>
              <br>
              </div>

          </div>
          </form>

            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{$from_date}} &nbsp; {{$to_date}}</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                <div class="table-responsive" style="padding: 2%;">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th class="border-top-0">ID</th>
                        <th class="border-top-0">Customer Name</th>
                        <th class="border-top-0">Address</th>
                        <th class="border-top-0">Services</th>
                        <th class="border-top-0">Assign To</th>
                        <th class="border-top-0">Expected Earning</th>
                        <th class="border-top-0">Status</th>
                        <th class="border-top-0">Job Date</th>
                        <th class="border-top-0">Edit</th>
                        <th class="border-top-0">Invoice</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($data as $jobs)
                       <tr>
                          <td class="text-truncate">{{$jobs->order_id}}</td>
                          <td class="text-truncate">{{$jobs->customername}}</td>
                          <td class="text-truncate">{{$jobs->cust_address}}</td>
                          <td class="text-truncate">{{$jobs->service_name}}</td>
                          <td class="text-truncate">{{$jobs->workername}}</td>
                          <td class="text-truncate">{{$jobs->total_amount}}</td>
                          <td class="text-truncate">{{$jobs->status}}</td>
                          <td class="text-truncate">{{ date("d-m-Y", strtotime($jobs->sdate)) }}</td>
                          <td align="center"> 
                            <div class="fonticon-wrap">
                                <a href="#"><i class="ft-edit"></i></a>
                            </div>
                          </td>
                          <td>
                              <div class="buy-now"><a href="{{ route('jobs.show', $jobs->order_id) }}" class="btn btn-sm btn-outline-info round" >View Details</a></div>
                          </td>
                         
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
</div>

   


@endsection

@push('scripts')





@endpush