@extends('layouts.admin')
   
@section('content')
     
         

         @if($errors->any())
        <div class="alert alert-danger">
         <ul>
          @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
         </ul>
        </div>
        @endif
        <div class="content-body"><!-- users view start -->
              <section class="users-view">
                <!-- users view media object start -->
                <div class="row">
                  <div class="col-xl-2 col-md-6 col-12"> </div>
                  <div class="col-xl-4 col-md-6 col-12">
                    <div class="card profile-card-with-stats box-shadow-2">
                      <div class="text-center">
                        <div class="card-body">
                          <img src="{{ $customer->image}}" class="rounded-circle height-150" alt="Card image">
                          
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">{{$customer->name}}</h4>
                          <ul class="list-inline list-inline-pipe list-group-item">
                            <li>@ {{$customer->regId}}</li>
                            <li>Customer</li>
                          </ul>
                         
                        </div>
                        
                        <div class="card-body">
                          <ul class="list-inline list-inline-pipe">
                            <li>{{$customer->email}}</li>
                            <li>{{$customer->cust_number}}</li>
                          </ul>
                          <a href="{{route('customer.show', $customer->regId)}}"><button type="button" class="btn btn-outline-primary btn-md btn-square mr-1"><i class="ft-user"></i>
                            Profile</button></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  
                  
                </div>
                <!-- users view media object ends -->
                <!-- users view card data start -->
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-12 col-md-12">
                          <h5 class="mb-1"><i class="ft-info"></i> Schedule Order Details: # {{$order->order_id}}</h5>
                          <table class="table table-borderless">
                            <tbody>
                             
                              <tr>
                                <td>Order Date:</td>
                                <td>{{$order->service_date}}</td>
                              </tr>
                              <tr>
                                <td>Main Service:</td>
                                <td>{{$order->service_name}}</td>
                              </tr>
                              <tr>
                                <td>Services Name:</td>
                                <td>{{$service_details[0]->name}}</td>
                              </tr>
                              <tr>
                                <td>Payment method:</td>
                                <td>{{$order->payment_method}}</td>
                              </tr>
                              <tr>
                                <td>Total Amount:</td>
                                <td>Rs- {{$order->total_amount}}</td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                        
                        
                      </div>
                    </div>
                    
                  </div>
                </div>
                <!-- users view card data ends -->
                <!-- users view card details start -->
                <!-- users view card details ends -->
              </section>
              <!-- users view ends -->
            </div>
                  
       
@endsection
@push('scripts')

@endpush

