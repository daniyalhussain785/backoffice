@extends('layouts.admin')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="content-body">
    <!-- users view start -->
    <section class="users-view">
        <div class="row match-height">
            <div class="col-xl-4 col-lg-4">
               <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-body pt-25">
                                    <h3 class="media-heading"><span class="users-view-name">Order Details</span><span class="text-muted font-medium-1"> #</span><span class="users-view-username text-muted font-medium-1 ">{{$data->order_id}}</span></h3>
                                    <span class="btn-info btn-sm mt-0 d-inline-block">{{date("l jS \of F Y h:i:s A", strtotime($data->create_at))}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(count($data->panic) != 0)
            @foreach($data->panic as $panic)
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="media-body text-left">
                                    @if($panic->role_id == 1)
                                    <h3 class="{{$panic->status == 1 ? 'success' : 'danger' }}">Kama Panic</h3>
                                    @else
                                    <h3 class="{{ $panic->status == 1 ? 'success' : 'danger' }}">Customer Panic</h3>
                                    @endif
                                    <h6>{{ $panic->reason }}</h6>
                                    @if($panic->status == 1)
                                    <span class="badge badge-success">Resolved</span>
                                    @else
                                    <span class="badge badge-danger">Un-Resolved</span>
                                    @endif
                                </div>
                                <div>
                                    <i class="icon-info font-large-2 float-right danger"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-xl-4 col-lg-4">
               <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-body pt-25">
                                    <h3 class="media-heading"><span class="users-view-name">No Panic Found</span></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-xl-4 col-lg-4">
               <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-body pt-25">
                                    <h3 class="media-heading">
                                        <span class="users-view-name">
                                        @if($data->status != 'Completed')
                                        Update Order Status
                                        @else
                                        Order Status
                                        @endif
                                        </span>
                                    </h3>
                                    <div class="input-group mt-1">
                                        @if($data->status != 'Completed')
                                        <form action="{{ route('update.status', $data->order_id) }}" method="post" class="w-100 d-flex">
                                            @csrf
                                            <select name="status" id="" class="form-control" required>
                                                <option value="{{$data->status}}" selected disabled>{{$data->status}}</option>
                                                @if($data->status == 'OnHold')
                                                <option value="continue">Continue Job</option>
                                                @else
                                                <option value="OnHold">On Hold</option>
                                                @endif
                                            </select>
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit">Go</button>
                                            </div>
                                        </form>
                                        @else
                                        <span class="btn-info btn-sm mt-0 d-inline-block">{{$data->status}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- users view media object start -->
        <div class="row match-height">
          <div class="col-md-4 supervisor-form-box">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body cleartfix">
                          <h4 class="mb-1">Customer Name:<br> {{$data->user->fullName}}</h4>
                          <div class="media align-items-center">
                              <div class="align-self-center">
                                  <a href="#" class="mr-1">
                                  @if($data->user->image != '')
                                  <img src="{{$data->user->image}}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                  @else
                                  <img src="{{ asset('images/noimage.png') }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                  @endif
                                  </a>
                              </div>
                              <div class="media-body">
                                  <div class="upper-media-body">
                                      <h5>Customer ID: <span>{{$data->user->regId}}</span></h5>
                                      @if($data->user->email != null)
                                      <h5>Email: <span>{{$data->user->email}}</span></h5>
                                      @endif
                                      <h5>Phone Number: <span>{{$data->user->phn_no}}</span></h5>
                                  </div>
                              </div>
                          </div>
                          <div class="upper-media-body mt-1 mb-1">
                              <span class="btn-success btn-sm">{{$data->user->city->name}} | {{$data->user->territory->name}}</span>
                          </div>
                          <h5>Address: <span>{{$data->user->address[0]->address}}</span></h5>
                          <a href="{{route('customer.show', $data->user->regId)}}" class="btn btn-primary mt-1">Profile</a>
                      </div>
                  </div>
              </div>
          </div>
          @if(!is_null($data->assign_to))
          <div class="col-md-4 supervisor-form-box">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body cleartfix">
                          <h4 class="mb-1">Worker Name: <br>{{$data->assign_person->fullName}}</h4>
                          <div class="media align-items-center">
                              <div class="align-self-center">
                                  <a href="#" class="mr-1">.
                                  @if($data->user->image != '')
                                  <img src="{{$data->assign_person->image}}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                  @else
                                  <img src="{{ asset('images/noimage.png') }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                  @endif
                                  </a>
                              </div>
                              <div class="media-body">
                                  <div class="upper-media-body">
                                      <h5>Worker ID: <span>{{$data->assign_person->regId}}</span></h5>
                                      @if($data->assign_person->email != null)
                                      <h5>Email: <span>{{$data->assign_person->email}}</span></h5>
                                      @endif
                                      <h5>Phone Number: <span>{{$data->user->phn_no}}</span></h5>
                                  </div>
                              </div>
                          </div>
                          <div class="upper-media-body mt-1 mb-1">
                              <span class="btn-success btn-sm">{{$data->assign_person->city->name}} | {{$data->assign_person->territory->name}}</span>
                          </div>
                          <div class="upper-media-body mt-1 mb-1">
                              <span class="btn-info btn-sm">
                              @foreach($data->assign_person->user_services as $user_service)
                              {{$user_service->name}} |
                              @endforeach
                              </span>
                          </div>
                          @if(count($data->assign_person->address) != 0)
                          <h5>Address: <span>{{$data->assign_person->address[0]->address}}</span></h5>
                          @endif
                          <a href="{{route('kama.show', $data->assign_person->regId)}}" class="btn btn-primary mt-1">Profile</a>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-4 supervisor-form-box">
              <div class="card">
                  <div class="card-content">
                      <div class="card-body cleartfix">
                          <h4 class="mb-1">Supervisor Name: <br>{{$data->assign_person->supervisor->fullName}}</h4>
                          <div class="media align-items-center">
                              <div class="align-self-center">
                                  <a href="#" class="mr-1">
                                  @if($data->user->image != '')
                                  <img src="{{$data->assign_person->supervisor->image}}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                  @else
                                  <img src="{{ asset('images/noimage.png') }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                  @endif
                                  </a>
                              </div>
                              <div class="media-body">
                                  <div class="upper-media-body">
                                      <h5>Supervisor ID: <span>{{$data->assign_person->supervisor->regId}}</span></h5>
                                      @if($data->assign_person->email != null)
                                      <h5>Email: <span>{{$data->assign_person->supervisor->email}}</span></h5>
                                      @endif
                                      <h5>Phone Number: <span>{{$data->user->phn_no}}</span></h5>
                                  </div>
                              </div>
                          </div>
                          <div class="upper-media-body mt-1 mb-1">
                              <span class="btn-success btn-sm">{{$data->assign_person->city->name}} | {{$data->assign_person->territory->name}}</span>
                          </div>
                          <div class="upper-media-body mt-1 mb-1">
                              <span class="btn-info btn-sm">
                              @foreach($data->assign_person->user_services as $user_service)
                              {{$user_service->name}} |
                              @endforeach
                              </span>
                          </div>
                          @if(count($data->assign_person->address) != 0)
                          <h5>Address: <span>{{$data->assign_person->address[0]->address}}</span></h5>
                          @endif
                          <a href="{{route('kama.show', $data->assign_person->regId)}}" class="btn btn-primary mt-1">Profile</a>
                      </div>
                  </div>
              </div>
          </div>
          @endif
        </div>

        <div class="row match-height">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <h3 class="content-header-title mb-0 d-inline-block mb-1">Order: #{{$data->order_id}}</h3>
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>Order Date</th>
                                        <td><span class="btn-success btn-sm">{{date("l jS \of F Y h:i:s A", strtotime($data->create_at))}}</span></td>
                                    </tr>
                                    <tr>
                                        <th>Main Service</th>
                                        <td>{{$data->service->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Status</th>
                                        <td><span class="btn-success btn-sm">{{$data->status}}</span></td>
                                    </tr>
                                    <tr>
                                        <th>Order Address</th>
                                        <td>{{$data->full_address}}</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Method</th>
                                        <td>{{$data->payment_method}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Amount</th>
                                        <td>Rs. {{$data->total_amount}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-content pb-1">
                        <div class="card-body">
                            <h3 class="content-header-title mb-0 d-inline-block mb-1">Shares</h3>
                            @if(count($data->orderShare))
                            <table class="table table-striped">
                                <tbody>
                            @for($i = 0; $i < count($data->orderShare); $i++)
                                    <tr>
                                        <th>{{$data->orderShare[$i]->share->label}}</th>
                                        <td><span class="btn-success btn-sm">Rs. {{$data->orderShare[$i]->total_amount}}</span></td>
                                    </tr>
                            @endfor
                                </tbody>
                            </table>
                            @else
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td><span class="btn-danger btn-sm">No Shares Found</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($data->order_details)
        @php
            $counter = 0;
        @endphp
        @foreach($data->order_details as $order_detail)
        @php
            $counter++;;
        @endphp
        <div class="row match-height">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <h3 class="content-header-title mb-0 d-block mb-1">Order Details: #{{$order_detail->order_details_id}} <span class="float-right btn btn-info btn-sm">Number : {{$counter}}</span></h3>
                            <table class="table table-striped">
                                <tbody>
                                    @if($order_detail->add_work != 0)
                                    <tr>
                                        <th>Additional Work</th>
                                        <td><span class="badge badge-success">true</span></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th>Order Details Date</th>
                                        <td><span class="btn-success btn-sm">{{date("l jS \of F Y h:i:s A", strtotime($order_detail->created_at))}}</span></td>
                                    </tr>
                                    <tr>
                                        <th>Service</th>
                                        <td>{{$order_detail->service->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Quantity</th>
                                        <td>{{$order_detail->quantity}}</td>
                                    </tr>
                                    <tr>
                                        <th>Amount</th>
                                        <td>Rs. {{$order_detail->amount}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-content pb-1">
                        <div class="card-body">
                            <h3 class="content-header-title mb-0 d-inline-block mb-1">Shares</h3>
                            @if(count($order_detail->order_share) != 0)
                            <table class="table table-striped">
                                <tbody>
                                @for($i = 0; $i < count($order_detail->order_share); $i++)
                                    <tr>
                                        <th>{{$order_detail->order_share[$i]->share->label}}</th>
                                        <td><span class="btn-success btn-sm">Rs. {{$order_detail->order_share[$i]->price}}</span></td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                            @else
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td><span class="btn-danger btn-sm">No Shares Found</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </section>
</div>
@endsection
@push('scripts')
@endpush