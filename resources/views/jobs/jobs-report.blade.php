@extends('layouts.admin')
   
@section('content')
     
       
          
        <div class="row">


          <div id="recent-sales" class="col-12 col-md-12">

           @if($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{$message}}</p>
          </div>
         @endif

         

            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Select date range</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                 <form action="{{route('jobs.jobs-report-range')}}" method="POST" enctype="multipart/form-data" >
             @csrf 
          <div class="row">
            
              <div class="col-md-4">
                <input type="date" name="from_date"  class="form-control" placeholder="From Date"  />
              </div>
              <div class="col-md-4">
                  <input type="date" name="to_date"  class="form-control" placeholder="To Date"  />
              </div>
              <div class="col-md-4">
                  <button type="submit" name="filter" class="btn btn-primary">Filter</button>
              </div>
          </div>
          </form>
              </div>
            </div>
            </div>
          </div>
        </div>
</div>

   


@endsection

@push('scripts')





@endpush