@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Edit Sub Service</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Services</li>
                    <li class="breadcrumb-item active">Sub Services</li>
                    <li class="breadcrumb-item active">Edit Sub Service</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Sub Service Update</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')  
                                <div class="form-body">
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label for="services">Add Services <span>*</span></label>
                                             <input type="text" id="services_name" class="form-control" placeholder="Services Name" name="services_name" required="required" value="{{$data->name}}">
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label for="services">Select Main Service <span>*</span></label>
                                             <select class="form-control" name="main_service_id" required>
                                             @foreach($categories as $category)
                                                <option value="{{$category->s_id}}" data-hours="{{$category->is_hourly}}" {{  $category->s_id ==  $data->parent_id ? 'selected' : ''  }}>{{$category->name}}
                                                </option>
                                             @endforeach
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label for="services_price">Services Price <span>*</span></label>
                                             <input type="number" id="services_price" class="form-control" placeholder="Services Price" name="services_price" required="required" value="{{$data->price}}">
                                             <span id="is_hourly_text" class="badge badge-info" style="margin-top: 6px;"></span>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label for="services">Services Image <span>*</span></label>
                                             <input type="file" class="form-control browse btn" placeholder="Upload File" name="image">
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label for="services">Select Status <span>*</span></label>
                                             <select name="status" id="status" class="form-control" >
                                                <option value="">Select Status</option>
                                                <option value="1" {{  $data->status == 1 ? 'selected' : ''  }}>Active</option>
                                                <option value="0" {{  $data->status == 0 ? 'selected' : ''  }}>Deactive</option>                         
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                            <div class="form-group">
                                                <img src="{{ $data->service_image }}" width ="120">
                                                <input type="hidden" id="custId" name="custId" value="3487">
                                            </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="row">
                                             <div class="col-md-8">
                                                <div class="form-group">
                                                   <label for="services">Add Shares</label>
                                                   <select class="form-control" id="shares_value">
                                                      <option value="" disabled selected>Select Shares</option>
                                                      @foreach($share_view as $share_views)
                                                      @php
                                                         $active = ''; 
                                                      @endphp
                                                      @foreach($data->service_share as $service_shares)
                                                         @if($service_shares->share->id == $share_views->id)
                                                         @php
                                                         $active = 'disabled';
                                                         @endphp
                                                         @break
                                                         @endif
                                                      @endforeach
                                                      <option value="{{$share_views->id}}" {{$active}}>{{$share_views->label}}</option>
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-4" style="margin-top: 28px;">
                                                <button class="btn btn-success btn-block" type="button" id="add_share">Add Shares</button>
                                             </div>
                                          </div>
                                       </div>


                                    </div>
                                    <div class="row" id="share-form">
                                    @foreach($data->service_share as $service_shares)
                                       <div class="col-md-6">
                                          <input type="hidden" name="share_id[{{$service_shares->share->id}}]">                                           
                                          <div class="form-group" data-value="{{$service_shares->share->id}}">
                                             <label for="services">{{$service_shares->share->label}} <span>*</span></label><span class="remove" onclick="removeShare(this)"><i class="fa fa-close"></i></span>                                              
                                             <div class="row">
                                                <div class="col-md-6 pr-0">
                                                   <input type="number" class="form-control brr-0" name="share_value[{{$service_shares->share->id}}][price]" required="required" value="{{$service_shares->share_price}}">
                                                </div>
                                                <div class="col-md-6 pl-0">
                                                   <select class="form-control brl-0" name="share_key[{{$service_shares->share->id}}][key]">
                                                      <option value="1" {{$service_shares->share_key == 1 ? 'selected' : ''}}>Price (Rs.)</option>
                                                      <option value="2" {{$service_shares->share_key == 2 ? 'selected' : ''}}>Percentage (%)</option>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                    <i class="la la-check-square-o"></i> Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-colored-form-control">Information</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body pt-0" id="error_box">
                            @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if (\Session::has('error'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{!! \Session::get('error') !!}</li>
                                </ul>
                            </div>
                            @endif
                            @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop
@section('script')
<script>
    $('#add_share').click(function(){
       $('#shares_value').removeClass('alert alert-danger');
       var shares_value = $('#shares_value').children("option:selected:not([disabled])").val();
       var shares_html = $('#shares_value').children("option:selected:not([disabled])").html();
       $('#shares_value').children("option:selected").prop('disabled', true);
       if(shares_html != undefined){
          $('#share-form').append('<div class="col-md-6">\
                                           <input type="hidden" name="share_id['+shares_value+']">\
                                           <div class="form-group" data-value="'+shares_value+'">\
                                              <label for="services">'+shares_html+' <span>*</span></label>\
                                              <span class="remove" onclick="removeShare(this)"><i class="fa fa-close"></i></span>\
                                              <div class="row">\
                                                 <div class="col-md-6 pr-0">\
                                                    <input type="number" class="form-control brr-0" name="share_value['+shares_value+'][price]" required="required">\
                                                 </div>\
                                                 <div class="col-md-6 pl-0">\
                                                    <select class="form-control brl-0" name="share_key['+shares_value+'][key]">\
                                                       <option value="1">Price (Rs.)</option>\
                                                       <option value="2">Percentage (%)</option>\
                                                    </select>\
                                                 </div>\
                                              </div>\
                                           </div>\
                                        </div>');
       }else{
          $('#shares_value').addClass('alert alert-danger');
       }
    });
    $('.form').on('submit', function(event){
       event.preventDefault();
       var service_price = parseFloat($('#services_price').val());
       var total_share = 0;
       $('#share-form .form-group').each(function(){
          var number_count = parseFloat($(this).find('input[type=number]').val());
          var value_percentage = $(this).find('select').children("option:selected").val();
          if(value_percentage == 2){
             total_share += (service_price/100) * number_count;
          }else{
             total_share += number_count; 
          }
       });
       if(service_price != total_share){
            $('#error_box').html('');
            $('#error_box').html('<div class="alert alert-danger"><ul><li>Service Price is not equal to the Share Prices</li></ul></div>');
            window.scrollTo(0, 0);
            return false;
       }else{
          var formData = new FormData(this);
          _ajax.postFormData("{{route('subservices.update', $data)}}",formData , function(response) {
             if(response.status){
                $('#error_box').html('');
                $('#error_box').html('<div class="alert alert-success"><ul><li>'+response.message+'</li></ul></div>');
                $('.form').trigger("reset");
             }else{
                var errorString = '<div class="alert alert-danger"><ul>';
                $.each( response.data, function( key, value) {
                      errorString += '<li>' + value + '</li>';
                });
                errorString += '</ul></div>';
                $('#error_box').html('');
                $('#error_box').html(errorString);
             }
             window.scrollTo(0, 0);
          })
       }
    });
    
    function removeShare(a){
      var value_id = $(a).parent().data('value');
      $("#shares_value option[value=" + value_id + "]").prop('disabled', false);
      $(a).parent().parent().remove();

    }

    $('select[name="main_service_id"]').on('change', function() {
        var hourly = $(this).find(':selected').data('hours');
        if(hourly == 1){
            $('#is_hourly_text').text('Price Effect on hourly Basis i.e 1 hour');
        }else{
            $('#is_hourly_text').text('');
        }
    });
</script>
@endsection