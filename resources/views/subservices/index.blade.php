@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">My Services</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Services
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Services List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Service Image</th>
                                <th>Sub Services</th>
                                <th>Sub Services Share</th>
                                <th>Services</th>
                                <th>Service Price</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $services)
                            <tr>
                                <td class="text-truncate">{{$services->s_id}}</td>
                                <td class="text-truncate" align="center">
                                    <img src="{{ $services->service_image }}" width ="80">
                                </td>
                                <td class="text-truncate">{{$services->name}}</td>
                                <td>
                                @if(count($services->service_share))
                                @for($i = 0; $i < count($services->service_share); $i++)
                                <span class="badge badge-info w-100 text-left badge-md text-uppercase">{{$services->service_share[$i]->share->label}} <br> Rs. {{$services->service_share[$i]->share_actual_price}} </span> <hr style="margin: 5px 0px;">
                                @endfor
                                @endif
                                </td>
                                <td class="text-truncate">{{$services->main_service->name}}</td>
                                <td class="text-truncate">{{$services->price}}</td>
                                <?php
                                    if ($services->status == 1) {
                                      $status = "<span class='badge badge-approved'>Active</span>";
                                    
                                    }elseif ($services->status == 0) {
                                      $status = "<span class='badge badge-pending'>Deactive</span>";
                                    }
                                    ?>
                                <td>{!!$status!!}</td>
                                <td>
                                    <div class="fonticon-wrap">
                                        <a href="{{ route('subservices.edit', $services->s_id) }}" class="btn btn-primary btn-sm"><i class="ft-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush