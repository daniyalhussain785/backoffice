@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">My Notification</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Notification
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Notification List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>title</th>
                                <th>body</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $notification)
                            <tr>
                                <td class="text-truncate">{{$notification->id}}</td>
                                <td class="text-truncate">{{$notification->title}}</td>
                                <td class="text-truncate">{{ \Illuminate\Support\Str::limit($notification->body, 90, $end='...') }}</td>
                                <td class="text-truncate">
                                    @if($notification->r_id == 1)
                                    <span class="badge badge-secondary">Kama</span>
                                    @elseif($notification->r_id == 2)
                                    <span class="badge badge-secondary">Supervisor</span>
                                    @else
                                    <span class="badge badge-secondary">Customer</span>
                                    @endif
                                </td>
                                <td align="center">
                                    <div class="fonticon-wrap">
                                        <a href="{{ route('notification.edit', $notification->id) }}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush