@extends('layouts.admin')
@section('pageTitle', 'Review by supervisor')  
@section('content')
     
         
          <!-- <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-content">
                  <div class="row">
                    <div class="col border-right-blue-grey border-right-lighten-4">
                      <div class="card-body text-center">
                        <h1 class="display-4"> {{$kamaCounter[0]->AllWorker}}</h1>
                        <span>All Workers</span>
                      </div>  
                    </div>
                    <div class="col border-right-blue-grey border-right-lighten-4">
                      <div class="card-body text-center">
                        <h1 class="display-4"> {{$kamaCounter[0]->pending}}</h1>
                        <span>Pending Workers</span>
                      </div>
                    </div>
                    <div class="col border-right-blue-grey border-right-lighten-4">
                      <div class="card-body text-center">
                        <h1 class="display-4"> {{$kamaCounter[0]->approved}}</h1>
                        <span>Approved Workers</span>
                      </div>
                    </div>
                    <div class="col border-right-lighten-4">
                      <div class="card-body text-center">
                        <h1 class="display-4">{{$kamaCounter[0]->newKama}}</h1>
                        <span>New Worker Requests</span>
                      </div>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          @include('inc.kamay-counter')

        <div class="row">

 

          <div id="recent-sales" class="col-12 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Review by supervisor </h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                <div class="table-responsive" style="padding: 2%;">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th class="border-top-0">Worker ID</th>
                        <th class="border-top-0">Full name</th>
                        <th class="border-top-0">Mobile no</th>
                        <th class="border-top-0">Services offered</th>
                        <th class="border-top-0">City</th>
                        <th class="border-top-0">Complete profile status</th>
                        <th class="border-top-0">Supervisor review status</th>
                        <th class="border-top-0">Duration</th>
                        <th class="border-top-0">Date</th>
                        <th class="border-top-0">Supervisor</th>
                       </tr>
                    </thead>
                    <tbody>
                      <?php $i = 0; ?>
                       @foreach($data as $kama)
                       
                      <tr>
                          <td class="text-truncate">{{$kama->regId}}</td>
                          <td class="text-truncate"><a href="{{route('kama.show', $kama->regId)}}" target="_blank">{{$kama->fullName}}</a></td>
                          <td class="text-truncate">{{$kama->phn_no}}</td>
                          <td><span class="badge badge-primary">{{$kama->expertise}}</span></td>
                          <td class="text-truncate">{{$kama->city_name}}</td>
                          <td class="text-truncate">  
                            <span class="badge badge-{{str_replace(' ', '', $kama->status)}}">{{$kama->status}}</span>
                          </td>
                          <?php if ($kama->application_status != '0') {?>
                            <td>
                              <span class="badge badge-primary">{{$kama->application_status}}</span></td>
                            
                         <?php }else{?>
                          <td><span class="badge badge-primary">No contact</span></td>
                        <?php } ?>
                          

                          <td class="text-truncate">{{$kama->assign_date}}</td>
                          <td class="text-truncate">{{date("d-m-Y", strtotime($kama->created_at))}}</td>
                          <td class="text-truncate">{{$kama->supervisorNames}}</td>
                          
                      </tr>
                      <?php $i++; ?>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
</div>
@endsection
@push('scripts')
<!-- <script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('kamayview') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'regId', name: 'regId'},
            {data: 'fullName', name: 'fullName'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script> -->
@endpush

