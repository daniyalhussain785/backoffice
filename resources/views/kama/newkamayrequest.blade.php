@extends('layouts.admin')
@section('pageTitle', 'Assign Supervisor')  
@section('content')
@include('inc.kamay-counter')
<div class="row">
    <!--  -->
    <div id="recent-sales" class="col-12 col-md-12">
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p class="mb-0">{{$message}}</p>
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Assign Supervisor  </h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered kamay-table">
                        <thead>
                            <tr>
                                <th class="border-top-0">Worker ID</th>
                                <th class="border-top-0">Name</th>
                                <th class="border-top-0">Mobile No</th>
                                <th class="border-top-0">NIC</th>
                                <th class="border-top-0">Services Offered</th>
                                <th class="border-top-0">City</th>
                                <th class="border-top-0">Area</th>
                                <th class="border-top-0">Assign Supervisor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $kama)
                            <tr>
                                <td class="text-truncate">{{$kama->regId}}</td>
                                <td class="text-truncate"><a href="{{route('kama.show', $kama->regId)}}" target="_blank">{{$kama->fullName}}</a></td>
                                <td class="text-truncate">{{$kama->phn_no}}</td>
                                <td class="text-truncate">{{$kama->cnic}}</td>
                                <td><span class="badge badge-primary">{{$kama->expertise}}</span></td>
                                <td class="text-truncate">{{$kama->city_name}}</td>
                                <td class="text-truncate">{{$kama->territory_name}}</td>
                                <td align="center">
                                    <!-- <a href="#" class="assignSupervisor btn btn-sm btn-outline-danger round" data-id="{{$kama->regId}}">Assign Supervisor</a> -->
                                    <a href="{{ route('assigned-supervisor-list', $kama->regId) }}" class="btn btn-sm btn-outline-danger round">supervisor list</a>
                                    <!--  <button type="button" class="btn btn-sm btn-outline-danger round" data-toggle="modal" data-target="#assignForm"></button> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal animated slideInDown text-left show" id="assignForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" style="display: none; padding-right: 17px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel77">Assign Supervisor To Kamay</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <ul class="nav flex-row wrap-border mb-1 scroll-model-wrapper">
                <li class="nav-header">
                </li>

                @foreach($getSupervisor as $getData)
                <form action="{{url('assign-supervisor')}}" method="post" class="supervisor-form-box">
                    @csrf
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body cleartfix">
                                <div class="media align-items-center">
                                    <div class="align-self-center">
                                        <a href="#" class="mr-1">
                                        <img src="{{ asset($getData->image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <div class="upper-media-body">
                                            <h4>{{$getData->fullName}} <span class="main-star">
                                                <?php 
                                                    for ($i=0; $i < 5; $i++){
                                                          if (round($getData->supervisor_ratings) > $i) {?>
                                                <span class="fa fa-star checked"></span>
                                                <?php
                                                    }else{?>
                                                <span class="fa fa-star"></span>
                                                <?php  }
                                                    }?></span>
                                            </h4>
                                            <h5>Supervisor ID: <span>{{$getData->regId}}</span></h5>
                                            <h5>Assigned worker: <span>{{$getData->worker_count}}</span></h5>
                                            <h5>Phone Number: <span>{{$getData->phn_no}}</span></h5>
                                            <h5>Address: <span>{{$getData->pri_add}}</span></h5>
                                        </div>
                                        <div class="upper-media-body">
                                            <div class="getId"></div>
                                            <input type="hidden" value="{{$getData->regId}}" name="supervisorId">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Assign</button>
                            </div>
                        </div>
                    </div>
                </form>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
      $(".assignSupervisor").click(function(){
        var id = $(this).data('id');
        $(".getId").html('');
        //alert(id);
        $('#assignForm').modal('show');
        $(".getId").append('<input type="hidden" name="kamaId" value="'+id+'" />')
      });
    });

    $(".kamay-table").DataTable({
        order:[[0,"desc"]]
    });
    
</script>
@endpush