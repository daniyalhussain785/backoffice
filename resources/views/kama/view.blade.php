@extends('layouts.admin')
@section('content')
<div class="content-body">
    <!-- users view start -->
    <section class="users-view">
        <!-- users view media object start -->
        <div class="row">
            <div class="col-12 col-sm-8">
                <div class="media mb-2">
                    <a class="mr-1" href="#">
                        @if($data[0]->cust_image != null)
                        <img src="{{ asset($data[0]->cust_image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                        @else
                        <img src="{{ asset('images/noimage.png') }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                        @endif
                    </a>
                    <div class="media-body pt-25">
                        <h4 class="media-heading"><span class="users-view-name">{{$data[0]->fullName}}</span></h4>
                        <span>ID:</span>
                        <span class="users-view-id">{{$data[0]->regId}}</span><br>
                        <span class="main-star">
                        <?php 
                            for ($i=0; $i < 5; $i++){
                                  if (round($kamaRating) > $i) {?>
                        <span class="fa fa-star checked"></span>
                        <?php
                            }else{?>
                        <span class="fa fa-star"></span>
                        <?php  }
                            }?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 px-0 d-flex justify-content-end align-items-center px-1 mb-2">
                <a href="#getNumber" data-target="#getNumber" data-toggle="modal" class="btn btn-sm mr-25 border" style="margin-right: 10px;"><i class="la la-phone"></i></a>
                <a href="https://wa.me/{{$data[0]->phn_no}}" class="btn btn-sm mr-25 border" style="margin-right: 10px;"><i class="la la-whatsapp"></i></a>
                <?php if ($data[0]->supervisorName != '') { ?>
                <span class="float-right">
                    <ul class="list-inline mb-0">
                        <li><a href="#" class="assignSupervisor btn btn-sm btn-warning box-shadow-2 round btn-min-width pull-right" data-id="{{$data[0]->regId}}">Change supervisor</a></li>
                    </ul>
                </span>
                <?php } ?>
            </div>
        </div>
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <h3 class="content-header-title mb-0 d-inline-block mb-1">Kamay Info</h3>
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>Services</th>
                        <td><span class="btn-success btn-sm">{{$getexpertise[0]->expertise}}</span></td>
                      </tr>
                      <tr>
                        <th>Registered</th>
                        <td>{{date("l jS \of F Y h:i:s A", strtotime($data[0]->rdate))}}</td>
                      </tr>
                      <tr>
                        <th>Complete profile status</th>
                        <td><span class="btn-success btn-sm">{{$data[0]->status}}</span></td>
                      </tr>
                      <tr>
                        <th>Review status by Supervisor</th>
                        <td>
                          @if ($data[0]->application_status != '0')
                          <span class="btn-primary btn-sm">{{$data[0]->application_status}}</span>
                          @else
                          <span class="btn-primary btn-sm">No contact</span>
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <th>Role</th>
                        <td>{{$data[0]->role_name}}</td>
                      </tr>
                      <tr>
                        <th>Avalaible for job</th>
                        <td>
                        @if ($data[0]->job_status == 1)
                        <span class="btn-success btn-sm">Active</span>
                        @else
                        <span class="btn-danger btn-sm">Not Active</span>
                        @endif
                        </td>
                      </tr>
                      <tr>
                        <th>CNIC</th>
                        <td>{{ substr($data[0]->cnic, 0, 5)."-".substr($data[0]->cnic,5,7)."-".substr($data[0]->cnic,12,1) }}</td>
                      </tr>
                      <tr>
                        <th>Registered mobile number</th>
                        <td>{{ substr($data[0]->phn_no, 0, 3)."-".substr($data[0]->phn_no,3,3)."-".substr($data[0]->phn_no,6,7) }}</td>
                      </tr>
                      <tr>
                        <th>Email</th>
                        @if($data[0]->email != '')
                        <td>{{ $data[0]->email }}</td>
                        @else
                        <td>No Email Found</td>
                        @endif
                      </tr>
                      <tr>
                        <th>Address</th>
                        <td>{{ $data[0]->pri_add }}</td>
                      </tr>
                      <tr>
                        <th>City</th>
                        <td>{{ $data[0]->city_name }}</td>
                      </tr>
                      <tr>
                        <th>Territory Name</th>
                        <td>{{ $data[0]->territory_name }}</td>
                      </tr>
                      <tr>
                        <th>Backoffice Approval</th>
                        @if ($data[0]->final_status == 1)
                        <td><span class="btn btn-success btn-sm">Approved</span></td>
                        @else
                        <td><span class="btn btn-danger btn-sm">Not Approved</span></td>
                        @endif
                      </tr>
                      <tr>
                        <th>Backoffice Comments</th>
                        @if($data[0]->comments != '')
                        <td>{{ $data[0]->comments }}</td>
                        @else
                        <td>No Comments</td>
                        @endif
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <h3 class="content-header-title mb-0 d-inline-block mb-1">Review checklist From Supervisors</h3>
                  <p>Supervisors use this checklist to complete all steps to qualify and approve a new Kamay who has signed up or registered.</p>
                  <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Review Checklist</th>
                                    <th>Actions to be taken by Supervisor</th>
                                    <th>Current Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reviewCheckList as $reviewCheckLists)
                                <tr>
                                    <td>{{$reviewCheckLists->doc_name}}</td>
                                    <td><i>{{$reviewCheckLists->status_list}}</i></td>
                                    <td>
                                        @if($reviewCheckLists->status_count == 1)
                                        <button class="btn btn-success btn-sm">{{$reviewCheckLists->active_status_name}}</button>
                                        @else
                                        <button class="btn btn-danger btn-sm">{{$reviewCheckLists->active_status_name}}</button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="card-content bg-hexagons pb-1">
                <div class="card-body">
                  <h3 class="content-header-title mb-0 d-inline-block mb-1">Information</h3>
                  <div class="chartjs">
                    <canvas id="hit-rate-doughnut" height="200"></canvas>
                  </div>
                </div>
              </div>
            </div>
            @if($check_approval != false)
            <div class="card">
              <div class="card-content pb-1">
                <div class="card-body">
                  <h3 class="content-header-title mb-0 d-inline-block mb-0">Update Status</h3>
                  <hr>
                  @if (\Session::has('success'))
                      <div class="alert alert-success">
                          <ul>
                              <li>{!! \Session::get('success') !!}</li>
                          </ul>
                      </div>
                  @endif
                  @if (\Session::has('error'))
                      <div class="alert alert-danger">
                          <ul>
                              <li>{!! \Session::get('error') !!}</li>
                          </ul>
                      </div>
                  @endif
                  <form class="form" method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                          <label for="nic">CNIC number <span>*</span></label>
                          <input type="text" class="form-control" data-inputmask="'mask': '99999-9999999-9'"  placeholder="XXXXX-XXXXXXX-X" name="cnic_number" id="cnic_number" required="required" value="{{old('cnic_number', $data[0]->cnic)}}" id="cnic_number">
                      </div>
                      <div class="form-group">
                          <label for="projectinput1">Vehicle number </label>
                          <input type="text" id="vehicle" class="form-control" placeholder="Vehicle number" name="vehicle_no" maxlength="10" value="{{old('vehicle_no', $data[0]->vehicle_no)}}">
                      </div>
                      <div class="form-group">
                          <label for="projectinput1">Status <span>*</span></label>
                          <select class="hide-search form-control select2-hidden-accessible" id="hide_search" name="status" data-select2-id="hide_search" tabindex="-1" aria-hidden="true" required>
                              <option value="">Update Status</option>
                              <option value="approved" {{ old('status') || $data[0]->kamay_application_status == 'approved' ? 'selected' : ''  }}>Approved</option>
                              <option value="not_approved" {{  old('status') || $data[0]->kamay_application_status == 'not_approved' ? 'selected' : ''  }}>Not Approved</option>
                              <option value="on_hold" {{  old('status') || $data[0]->kamay_application_status == 'on_hold' ? 'selected' : ''  }}>On Hold</option>
                              <option value="deactivate" {{  old('status') || $data[0]->kamay_application_status == 'deactivate' ? 'selected' : ''  }}>Deactivate</option>
                              <option value="expired" {{  old('status') || $data[0]->kamay_application_status == 'expired' ? 'selected' : ''  }}>Expired (for deceased cases)</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="comments">Comments</label>
                          <input type="text" id="comments" class="form-control" placeholder="Comments" name="comments" value="{{old('comments', $data[0]->comments)}}">
                      </div>
                      <div class="form-actions text-right">
                          <button type="submit" class="btn btn-primary w-100">
                          <i class="la la-check-square-o"></i> Update application status
                          </button>
                      </div>
                  </form>
                  <div id="error_box">
                  
                  </div>
                </div>
              </div>
            </div>
            @endif
            @if ($data[0]->supervisorName != '')
            <div class="supervisor-form-box">
              <div class="card">
                <div class="card-content">
                    <div class="card-body cleartfix">
                        <h3 class="content-header-title mb-0 d-inline-block mb-0">Supervisor Details</h3>
                        <hr>
                        <h4 class="mb-1">{{$data[0]->supervisorName}}
                          <span class="main-star">
                            @for($i=0; $i < 5; $i++)
                              @if(round($supRatings) > $i)
                              <span class="fa fa-star checked"></span>
                              @else
                              <span class="fa fa-star"></span>
                              @endif
                            @endfor
                            </span>
                        </h4>
                        <div class="media align-items-center">
                            <div class="align-self-center">
                                <a href="#" class="mr-1">
                                <img src="{{ asset($data[0]->image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="upper-media-body">
                                    <h5>ID: <span>{{$data[0]->supervisor_id}}</span></h5>
                                    <h5><span>{{$data[0]->supervisor_nic}}</span></h5>
                                    <h5><span>{{$data[0]->supervisor_number}}</span></h5>
                                </div>
                            </div>
                        </div>
                        <div class="upper-media-body mt-1 mb-1">
                            @if($getSupervisorExpertise->expertise != '')
                                <span class="btn-success btn-sm">{{$getSupervisorExpertise->expertise}}</span>
                            @endif
                        </div>
                        <h5>Email: <span>{{$data[0]->supervisor_email}}</span></h5>
                        <h5>Address: <span>{{$data[0]->supervisor_address}}</span></h5>
                    </div>
                </div>
              </div>
            </div>
            @endif
          </div>
        </div>
        <!-- users view media object ends -->
        <!-- users view card data start -->
        <div class="modal animated slideInDown text-left show" id="getNumber" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" style="display: none; padding-right: 17px;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel77">
                            Call kama: 
                            <spen style="margin-left: 30px"> <i class="la la-phone"></i> {{$data[0]->phn_no}}</spen>
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- users view card data ends -->
        <!-- users view card details start -->
        <!-- users view card details ends -->
        <div class="modal animated slideInDown text-left show" id="assignForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" style="display: none; padding-right: 17px;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel77">Assign Supervisor To Kamay</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <ul class="nav flex-row wrap-border mb-1 scroll-model-wrapper">
                        <li class="nav-header">
                        </li>
                        @foreach($getSupervisorAssigned as $getData)
                        <form action="{{url('assign-supervisor')}}" method="post" class="supervisor-form-box">
                            @csrf
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body cleartfix">
                                        <div class="media align-items-center">
                                            <div class="align-self-center">
                                                <a href="#" class="mr-1">
                                                <img src="{{ asset($getData->image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <div class="upper-media-body">
                                                    <h4>{{$getData->fullName}} <span class="main-star">
                                                        <?php 
                                                            for ($i=0; $i < 5; $i++){
                                                                  if (round($getData->supervisor_ratings) > $i) {?>
                                                        <span class="fa fa-star checked"></span>
                                                        <?php
                                                            }else{?>
                                                        <span class="fa fa-star"></span>
                                                        <?php  }
                                                            }?></span>
                                                    </h4>
                                                    <h5>Supervisor ID: <span>{{$getData->regId}}</span></h5>
                                                    <h5>Assigned worker: <span>{{$getData->worker_count}}</span></h5>
                                                    <h5>Phone Number: <span>{{$getData->phn_no}}</span></h5>
                                                    <h5>Address: <span>{{$getData->pri_add}}</span></h5>
                                                </div>
                                                <div class="upper-media-body">
                                                    <span class="btn-success btn-sm float-right">{{$getData->supervisor_expertise}}</span>
                                                    <div class="getId"></div>
                                                    <input type="hidden" value="{{$getData->regId}}" name="supervisorId">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" name="updateSupervisor" value="updateSupervisor" class="btn btn-primary float-right">Assign</button>
                                    </div>
                                </div>
                            </div>
                            <!-- <li class="nav-item">
                                <a class="nav-link">
                                  <div class="img-box">
                                    <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png">
                                  </div>
                                  <span>{{$getData->fullName}}</span>
                                  <div id="getId"></div>
                                  <input type="hidden" value="{{$getData->regId}}" name="supervisorId">
                                  <button type="submit" class="btn btn-primary float-right">Assign</button>
                                </a>
                                </li> -->
                        </form>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- users view ends -->
</div>
@endsection
@push('scripts')
<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
    $(":input").inputmask();
</script>
<!-- Chart code -->
<script>
     $(document).ready(function () {
        $(".assignSupervisor").click(function(){
          var id = $(this).data('id');
          $(".getId").html('');
          //alert(id);
          $('#assignForm').modal('show');
          $(".getId").append('<input type="hidden" name="kamaId" value="'+id+'" />')
        });
      });
</script>
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/chart.min.js"></script>
<script>
var success_count = {!! json_encode($success_count) !!};
success_count = success_count * 10; 
var unsuccess_count = 100 - success_count;
console.log(success_count);
console.log(unsuccess_count);
$(window).on("load", (function() {
    Chart.defaults.hitRateDoughnut = Chart.defaults.doughnut;
    t = Chart.controllers.doughnut.prototype.draw;
    var r = Chart.controllers.doughnut.extend({
        draw: function() {
            t.apply(this, arguments);
            var e = this.chart.chart.ctx,
                o = e.fill,
                a = this.chart.width,
                r = this.chart.height,
                i = (r / 114).toFixed(2);
            this.chart.ctx.font = i + "em Verdana", this.chart.ctx.textBaseline = "middle";
            var l = success_count + "%",
                n = Math.round((a - this.chart.ctx.measureText(l).width) / 2),
                d = r / 2;
            this.chart.ctx.fillStyle = "#28D094", "rtl" == $("html").data("textdirection") && (n = Math.round((a - this.chart.ctx.measureText(l).width) / 1.15)), this.chart.ctx.fillText(l, n, d), e.fill = function() {
                e.save(), e.shadowColor = "#bbbbbb", e.shadowBlur = 30, e.shadowOffsetX = 0, e.shadowOffsetY = 12, o.apply(this, arguments), e.restore()
            }
        }
    });
    Chart.controllers.hitRateDoughnut = r;
    o = document.getElementById("hit-rate-doughnut"), new Chart(o, {
        type: "hitRateDoughnut",
        data: {
            labels: ["Completed", "Remain"],
            datasets: [{
                label: "Favourite",
                backgroundColor: ["#28D094", "#FF4961"],
                data: [success_count, unsuccess_count]
            }]
        },
        options: {
            responsive: !0,
            title: {
                display: !1
            },
            legend: {
                display: !1
            },
            layout: {
                padding: {
                    left: 16,
                    right: 16,
                    top: 16,
                    bottom: 16
                }
            },
            cutoutPercentage: 92,
            animation: {
                animateScale: !1,
                duration: 2500
            }
        }
    });
}));
</script>
<script>
    $('.form').on('submit', function(event){
        event.preventDefault();
        $('input').removeClass('alert-danger');
        $('select').removeClass('alert-danger');
        $('#error_box').html('');
        $error = '<div class="alert alert-danger"><ul>';
        $error_count = 0;
        var cnic_number = $('#cnic_number').val();
        cnic_number = cnic_number.split('_').join('');
        if(cnic_number.length != 15){
            $('#cnic_number').addClass('alert-danger');
            $error += "<li>CNIC Number Invalid</li>";
            $error_count = 1;
        }
        $error += '</ul></div>';
        if($error_count != 1){
          var formData = new FormData(this);
            _ajax.postFormData("{{route('final-approval',$data[0]->regId)}}",formData , function(response) {
              if(response.status){
                  $('#error_box').html('');
                  $('#error_box').html('<div class="alert alert-success"><ul><li>'+response.message+'</li></ul></div>');
                  $('.form').trigger("reset");
              }else{
                  console.log(response);
                  var errorString = '<div class="alert alert-danger"><ul>';
                  $.each( response.data, function( key, value) {
                      errorString += '<li>' + value + '</li>';
                  });
                  errorString += '</ul></div>';
                  $('#error_box').html('');
                  $('#error_box').html(errorString);
              }
          })
        }else{
            $('#error_box').html($error);
            $("input.alert-danger").first().focus();
            $("select.alert-danger").first().focus();
            return false;
        }
    });
</script>
@endpush