@extends('layouts.admin')
@section('pageTitle', 'All workers')  
@section('content')
@include('inc.kamay-counter')
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">All workers</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                    <div class="table-responsive" style="padding: 2%;">
                        <table class="table table-striped table-bordered kamay-table">
                            <thead>
                            <tr>
                            <th class="border-top-0">
                            Worker ID</th>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Mobile No</th>
                            <th class="border-top-0">Services Offered</th>
                            <th class="border-top-0">City</th>
                            <th class="border-top-0">Complete profile status</th>
                            <th class="border-top-0">Registration date</th>
                            <!-- <th class="border-top-0">Supervisor</th> -->
                            <th class="border-top-0">Edit profile</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $kama)
                                <tr>
                                    <td class="text-truncate">{{$kama->regId}}</td>
                                    <td class="text-truncate"><a href="{{route('kama.show', $kama->regId)}}" target="_blank"> {{$kama->fullName}}</a></td>
                                    <td class="text-truncate">{{$kama->phn_no}}</td>
                                    <td><span class="badge badge-primary">{{$kama->expertise}}</span></td>
                                    <td class="text-truncate">{{$kama->city_name}}</td>
                                    <td class="text-truncate">
                                    <span class="badge badge-{{str_replace(' ', '', $kama->status)}}">{{$kama->status}}</span></td>
                                    <td class="text-truncate">{{date("d-m-Y", strtotime($kama->created_at))}}</td>
                                    <td align="center">
                                        <div class="fonticon-wrap">
                                            <a href="{{ route('kama.edit', $kama->regId) }}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
                                        </div>
                                    </td>
                                    <!-- <td>    
                                        <div class="fonticon-wrap">
                                            <a href="#"><i class="ft-trash-2"></i></a>
                                        </div>
                                        
                                        </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script>
    $(".kamay-table").DataTable({
      order:[[0,"desc"]]
    })
</script>
@endpush