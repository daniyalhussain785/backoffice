@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Assign Supervisor</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Kama
                    </li>
                    <li class="breadcrumb-item active">Assign Supervisor
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- users view start -->
    <section class="users-view">
        <!-- users view media object start -->
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                    <div class="media">
                      <a class="mr-1" href="#">
                      <img src="{{$data[0]->image}}" alt="users view avatar"
                          class="users-avatar-shadow rounded-circle" height="64" width="64">
                      </a>
                      <div class="media-body pt-25">
                          <h4 class="media-heading"><span class="users-view-name">{{$data[0]->fullName}} </span></h4>
                          <span>ID:</span>
                          <span class="users-view-id">{{$data[0]->regId}}</span> | 
                          <span>PHONE:</span>
                          <span class="users-view-id">{{$data[0]->phn_no}}</span> | 
                          <span>CITY:</span>
                          <span class="users-view-id">{{$data[0]->city_name}}</span> | 
                          <span>TERRITORY NAME: </span>
                          <span class="users-view-id">{{$data[0]->territory_name}}</span> 
                          @if($data[0]->cnic != null)
                          |
                          <span>CNIC:</span>
                          <span class="users-view-id">{{$data[0]->cnic}}</span> | <br>
                          @endif
                          <span class="users-view-id btn-success btn-sm d-inline-block" style="margin-top: 6px;s">{{$data[0]->expertise}}</span><br>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
       </div>
       <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg nav-justified mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" id="active-tab1" data-toggle="tab" href="#active1" aria-controls="active1"
                                        aria-expanded="true">Relevant</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="link-tab1" data-toggle="tab" href="#area" aria-controls="area"
                                        aria-expanded="false">Area Wise</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="link-tab1" data-toggle="tab" href="#link1" aria-controls="link1"
                                        aria-expanded="false">All</a>
                                </li>
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane active" id="active1" aria-labelledby="active-tab1"
                                    aria-expanded="true">
                                    <div class="col-md-12 p-0">
                                        <div class="row">
                                            @foreach($getFilterSupervisor as $getData)
                                            <div class="col-md-4">
                                                <form action="{{url('assign-supervisor')}}" method="post" class="supervisor-form-box supervisor-form-box-white">
                                                    @csrf
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body cleartfix">
                                                                <h4 class="mb-1">{{$getData->fullName}} <span class="main-star">
                                                                        <?php 
                                                                            for ($i=0; $i < 5; $i++){
                                                                                if (round($getData->supervisor_ratings) > $i) {?>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <?php
                                                                            }else{?>
                                                                        <span class="fa fa-star"></span>
                                                                        <?php  }
                                                                            }?></span>
                                                                </h4>
                                                                <div class="media align-items-center">
                                                                    <div class="align-self-center">
                                                                        <a href="#" class="mr-1">
                                                                        <img src="{{ asset($getData->image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="upper-media-body">
                                                                            <h5>Supervisor ID: <span>{{$getData->regId}}</span></h5>
                                                                            <h5>Assigned worker: <span>{{$getData->worker_count}}</span></h5>
                                                                            <h5>Phone Number: <span>{{$getData->phn_no}}</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="upper-media-body mt-1 mb-1">
                                                                    @if($getData->supervisor_expertise != '')
                                                                        <span class="btn-success btn-sm">{{$getData->supervisor_expertise}}</span>
                                                                    @endif
                                                                    <input type="hidden" value="{{$data[0]->regId}}" name="kamaId">
                                                                    <input type="hidden" value="{{$getData->regId}}" name="supervisorId">
                                                                </div>
                                                                <h5>Address: <span>{{$getData->pri_add}}</span></h5>
                                                                <button type="submit" class="btn btn-primary mt-1">Assign</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="area" aria-labelledby="active-area"
                                    aria-expanded="true">
                                    <div class="col-md-12 p-0">
                                        <div class="row">
                                            @foreach($getAreaFilterSupervisor as $getData)
                                            <div class="col-md-4">
                                                <form action="{{url('assign-supervisor')}}" method="post" class="supervisor-form-box supervisor-form-box-white">
                                                    @csrf
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body cleartfix">
                                                                <h4 class="mb-1">{{$getData->fullName}} <span class="main-star">
                                                                        <?php 
                                                                            for ($i=0; $i < 5; $i++){
                                                                                if (round($getData->supervisor_ratings) > $i) {?>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <?php
                                                                            }else{?>
                                                                        <span class="fa fa-star"></span>
                                                                        <?php  }
                                                                            }?></span>
                                                                </h4>
                                                                <div class="media align-items-center">
                                                                    <div class="align-self-center">
                                                                        <a href="#" class="mr-1">
                                                                        <img src="{{ asset($getData->image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="upper-media-body">
                                                                            <h5>Supervisor ID: <span>{{$getData->regId}}</span></h5>
                                                                            <h5>Assigned worker: <span>{{$getData->worker_count}}</span></h5>
                                                                            <h5>Phone Number: <span>{{$getData->phn_no}}</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="upper-media-body mt-1 mb-1">
                                                                    @if($getData->supervisor_expertise != '')
                                                                        <span class="btn-success btn-sm">{{$getData->supervisor_expertise}}</span>
                                                                    @endif
                                                                    <input type="hidden" value="{{$data[0]->regId}}" name="kamaId">
                                                                    <input type="hidden" value="{{$getData->regId}}" name="supervisorId">
                                                                </div>
                                                                <h5 class="mb-1">Address: <span>{{$getData->pri_add}}</span></h5>
                                                                <span class="btn-info btn-sm">{{$getData->per_terri_name}}</span>
                                                                <span class="btn-info btn-sm">{{$getData->kama_territory_name}}</span>
                                                                <button type="submit" class="btn btn-primary mt-1">Assign</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="link1" role="tabpanel" aria-labelledby="link-tab1" aria-expanded="false">
                                <div class="col-md-12 p-0">
                                        <div class="row">
                                            @foreach($getSupervisor as $getData)
                                            <div class="col-md-4">
                                                <form action="{{url('assign-supervisor')}}" method="post" class="supervisor-form-box supervisor-form-box-white">
                                                    @csrf
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body cleartfix">
                                                                <h4 class="mb-1">{{$getData->fullName}} <span class="main-star">
                                                                        <?php 
                                                                            for ($i=0; $i < 5; $i++){
                                                                                if (round($getData->supervisor_ratings) > $i) {?>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <?php
                                                                            }else{?>
                                                                        <span class="fa fa-star"></span>
                                                                        <?php  }
                                                                            }?></span>
                                                                </h4>
                                                                <div class="media align-items-center">
                                                                    <div class="align-self-center">
                                                                        <a href="#" class="mr-1">
                                                                        <img src="{{ asset($getData->image) }}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100">
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <div class="upper-media-body">
                                                                            <h5>Supervisor ID: <span>{{$getData->regId}}</span></h5>
                                                                            <h5>Assigned worker: <span>{{$getData->worker_count}}</span></h5>
                                                                            <h5>Phone Number: <span>{{$getData->phn_no}}</span></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="upper-media-body mt-1 mb-1">
                                                                    @if($getData->supervisor_expertise != '')
                                                                        <span class="btn-success btn-sm">{{$getData->supervisor_expertise}}</span>
                                                                    @endif
                                                                    <input type="hidden" value="{{$data[0]->regId}}" name="kamaId">
                                                                    <input type="hidden" value="{{$getData->regId}}" name="supervisorId">
                                                                </div>
                                                                <h5>Address: <span>{{$getData->pri_add}}</span></h5>
                                                                <button type="submit" class="btn btn-primary mt-1">Assign</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
</div>
</section>
<!-- users view ends -->
</div>
@endsection
@push('scripts')
<script>
$('.supervisor-form-box').submit(function(){
    $(this).find('button').attr('disabled', true);
});
</script>
@endpush