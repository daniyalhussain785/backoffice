@extends('layouts.admin')
@section('pageTitle', 'Worker growth potential')  
@section('content')

@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Worker growth potential</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                    <div class="table-responsive" style="padding: 2%;">
                        <table class="table table-striped table-bordered kamay-table">
                            <thead>
                            <tr>
                            <th class="border-top-0">
                            Worker ID</th>
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Mobile No</th>
                            <th class="border-top-0">Services Offered</th>
                            <th class="border-top-0">City</th>
                            <th class="border-top-0">Complete profile status</th>
                            <th class="border-top-0">Rating</th>
                            <th class="border-top-0">No of orders</th>
                            <th class="border-top-0">Total earning</th>
                            <!-- <th class="border-top-0">Amount receivable from Kamay</th> -->
                            <th class="border-top-0">Registration date</th>
                           
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $kama)
                                <tr>
                                    <td class="text-truncate">{{$kama->regId}}</td>
                                    <td class="text-truncate"><a href="{{route('kama.show', $kama->regId)}}" target="_blank"> {{$kama->fullName}}</a></td>
                                    <td class="text-truncate">{{$kama->phn_no}}</td>
                                    <td>
                                        <span class="badge badge-primary">
                                        @foreach($kama->user_services as $user_services)
                                        {{$user_services->name}},
                                        @endforeach
                                        </span>
                                    </td>
                                    <td class="text-truncate">{{$kama->city->name}}</td>
                                    <td class="text-truncate">
                                    @if($kama->status == 1)
                                    <span class="badge badge-success">Approved</span>
                                    @else
                                    <span class="badge badge-danger">Not Approved</span>
                                    @endif
                                    </td>
                                    <td> 
                                        {{$kama->kamay_rating()}}<br>
                                        <span class="main-star">

                                            <?php
                                            for ($i=0; $i < 5; $i++){
                                                  if (round($kama->kamay_rating()) > $i) {?>
                                        <span class="fa fa-star checked"></span>
                                        <?php
                                            }else{?>
                                        <span class="fa fa-star"></span>
                                        <?php  }
                                            }?>
                                        </span>
                                    </td>
                                    <td>{{$kama->nooforders()}}</td>
                                    <td>
                                        Rs. {{$kama->totalamount()}}
                                    </td>
                                    <!-- <td>
                                        Rs-{{$kama->amount_received}}
                                    </td> -->
                                    <td class="text-truncate">{{date("d-m-Y", strtotime($kama->created_at))}}</td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script>
    $(".kamay-table").DataTable({
      order:[[0,"desc"]]
    })
</script>
@endpush