@extends('layouts.admin')
   @section('pageTitle', 'Kamay Worker Profile')  
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Kamay Worker Profile</h3>
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a>
                  </li>
<!--                   <li class="breadcrumb-item active">Create Booking
                  </li> -->
                </ol>
              </div>
            </div>
        </div>
    </div>
    @if($errors->any())
        <div class="alert alert-danger">
         <ul>
          @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
         </ul>
        </div>
        @endif
    <div class="content-body">
        <section id="basic-form-layouts">
            <div class="row match-height">
             <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                      <h4 class="card-title" id="basic-layout-form">Kamay Details</h4>
                      <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                         <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                         </ul>
                      </div>
                   </div>
                   <div class="card-content collapse show">
                      <div class="card-body">
                           <form class="form" action="#" method="POST" enctype="multipart/form-data" id="booking_form">
                            @csrf   
                            <div class="form-body">
                               <div class="row">
                                 
                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="name">Full Name</label>
                                        <input type="text" id="name" class="form-control" placeholder="Kamay Name" name="name">
                                     </div>
                                  </div>
                                 <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="nic">Kamay Worker NIC</label>
                                        <input type="cnic_number" id="nic" class="form-control" placeholder="CNIC Number" name="nic">
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" id="address" class="form-control" placeholder="Address" name="address">
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="cell_no">Mobile Number</label>
                                        <input type="text" id="cell_no" class="form-control" placeholder="Cell No" name="cell_no">
                                     </div>
                                  </div>
                                <!--  <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Alternate Cell No</label>
                                        <input type="text" id="alternate_cell_no" class="form-control" placeholder="Alternate Cell No" name="alternate_cell_no">
                                     </div>
                                  </div> -->

                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" id="city" class="form-control" placeholder="City" name="city">
                                     </div>
                                  </div>



                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="projectinput1">Email Address</label>
                                        <input type="email" id="email_id" class="form-control" placeholder="Email" name="email_id">
                                     </div>
                                  </div>

                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="area">Area</label>
                                        <input type="text" id="area" class="form-control" placeholder="Area" name="area">
                                     </div>
                                  </div>

                                 <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="expertise">Expertise</label>
                                        <input type="text" id="expertise" class="form-control" placeholder="Expertise" name="expertise">
                                     </div>
                                  </div>

                                 <!--  <div class="w-100" style="margin-top:20px;"></div>
                                  <div class="col-md-12">
                                    <h4 class="card-title" id="basic-layout-form">Address Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="form-actions text-right">                                   
                                    </div>
                                  </div>



                                  <div class="w-100"></div>
                                  <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">House No</label>
                                        <input type="text" id="house_no" class="form-control" placeholder="House No" name="house_no">
                                     </div>
                                  </div>

                                   <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Street/Road</label>
                                        <input type="text" id="street_road" class="form-control" placeholder="Street/Road" name="street_road">
                                     </div>
                                  </div>

                                 <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Block Sector Phase</label>
                                        <input type="text" id="block_sector_phase" class="form-control" placeholder="Block Sector Phase" name="block_sector_phase">
                                     </div>
                                  </div>

                                  <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Town</label>
                                        <input type="text" id="town" class="form-control" placeholder="Town" name="town">
                                     </div>
                                  </div>

                                 <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="projectinput1">Area</label>
                                        <input type="text" id="area" class="form-control" placeholder="Area" name="area">
                                     </div>
                                  </div>

                                   <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="projectinput1">Address</label>
                                        <input type="text" id="address" class="form-control" placeholder="Address" name="address">
                                     </div>
                                  </div>

                                   <div class="w-100" style="margin-top:20px;"></div>
                                  <div class="col-md-12">
                                    <h4 class="card-title" id="basic-layout-form">Booking Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="form-actions text-right">
                                    
                                    </div>
                                  </div>
                                  <div class="w-100"></div>

                                    <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Hydrant Name</label>
                                        <input type="text" id="projectinput1" class="form-control" placeholder="Hydrant Name" name="hydrant_name">
                                     </div>
                                  </div>

                                 <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Tanker Service</label>
                                        <input type="text" id="projectinput1" class="form-control" placeholder="Tanker Service" name="tanker_service">
                                     </div>
                                  </div>

                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="projectinput1">Select Tanker Service</label>
                                       <select name="selected_tanker_service" class="form-control select2">
                                          <option value="">Select Tanker Service</option>
                                        
                                          <option value="GPS">GPS</option>
                                          <option value="Commercial">Commercial</option>
                                        
                                      </select>     
                                     </div>
                                  </div>

                                 <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Gallons</label>
                                        <input type="text" id="projectinput1" class="form-control" placeholder="Gallons" name="gallons">
                                     </div>
                                  </div>

                                  <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="projectinput1">Estimated Charges</label>
                                        <input type="text" id="projectinput1" class="form-control" placeholder="Estimated Charges" name="estimated_charges">
                                     </div>
                                  </div>
                                  
                                  
                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="projectinput1">CSR Comments</label>
                                        <input type="text" id="projectinput1" class="form-control" placeholder="CSR Comments" name="csr_comments">
                                     </div>
                                  </div>
                                 
                                  -->
                               </div>
                            </div> 
                            <div class="form-actions text-right">
                               <button type="submit" class="btn btn-primary">
                               <i class="la la-check-square-o"></i> Save
                               </button>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
            
          </div>
       </section>
      
</div>

@stop

@section('script')
  
@endsection
