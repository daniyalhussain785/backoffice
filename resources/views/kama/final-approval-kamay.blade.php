@extends('layouts.admin')
   @section('pageTitle', 'Backoffice approvel')
@section('content')
     
         
          @include('inc.kamay-counter')

        <div class="row">

 

          <div id="recent-sales" class="col-12 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Backoffice approvel </h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                <div class="table-responsive" style="padding: 2%;">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th class="border-top-0">Worker ID</th>
                        <th class="border-top-0">Name</th>
                        <th class="border-top-0">Mobile No</th>
                        <th class="border-top-0">Services Offered</th>
                        <th class="border-top-0">City</th>
                        <th class="border-top-0">Registration date</th>
                        <th class="border-top-0">Assigned supervisor</th>
                        <th class="border-top-0">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 0; ?>
                       @foreach($data as $kama)
                       <tr>
                        <td class="text-truncate">{{$kama->regId}}</td>
                          <td class="text-truncate"><a href="{{route('kama.show', $kama->regId)}}" target="_blank">{{$kama->fullName}}</a></td>
                          <td class="text-truncate">{{$kama->phn_no}}</td>
                          <td><span class="badge badge-primary">{{$kama->expertise}}</span></td>
                          <td class="text-truncate">{{$kama->city_name}}</td>
                          
                          <td class="text-truncate">{{date("d-m-Y", strtotime($kama->created_at))}}</td>
                          <td class="text-truncate">{{$kama->supervisorNames}}</td>
                          <td align="center"> 
                              <div class="fonticon-wrap">
                                    <a href="{{ route('kama.edit', $kama->regId) }}"><i class="ft-edit"></i></a>
                                </div>
                          </td>
                      </tr>
                      <?php $i++; ?>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
</div>
@endsection
@push('scripts')
<!-- <script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('kamayview') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'regId', name: 'regId'},
            {data: 'fullName', name: 'fullName'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script> -->
@endpush

