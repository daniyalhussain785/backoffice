@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Assign Badges</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Assign Badges</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Assign Badges Details</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" action="{{route('assign-badges.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf   
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="p_id">User Name <span>*</span></label>
                                                <select name="p_id" id="p_id" class="form-control searcher" required>
                                                    <option value="" disabled selected>Select User<option>
                                                    @foreach($user as $users)
                                                    <option value="{{$users->regId}}">{{$users->fullName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="b_id">Badges Name <span>*</span></label>
                                                <select name="b_id[]" id="b_id" class="form-control select2" multiple required>
                                                    @foreach($badges as $badge)
                                                    <option value="{{$badge->b_id}}">{{$badge->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
               <div class="card">
                  <div class="card-header">
                     <h4 class="card-title" id="basic-layout-colored-form-control">Information</h4>
                     <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                     <div class="heading-elements">
                        <ul class="list-inline mb-0">
                           <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                           <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                           <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                           <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="card-content collapse show">
                     <div class="card-body pt-0">
                        @if($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                              @foreach($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('error'))
                        <div class="alert alert-danger">
                           <ul>
                              <li>{!! \Session::get('error') !!}</li>
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                           <ul>
                              <li>{!! \Session::get('success') !!}</li>
                           </ul>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
        </div>
    </section>
</div>
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Assign Badge List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Badges Name</th>
                                <th>Added By</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($perbadges as $perbadge)
                            <tr>
                                <td class="text-truncate">{{$perbadge->pb_id}}</td>
                                <td class="text-truncate">{{$perbadge->user->fullName}}</td>
                                <td class="text-truncate">
                                    @foreach($perbadge->user->badges as $badges)
                                    <span class="badge badge-newkamay badge-md">{{$badges->badge->name}}</span>
                                    @endforeach
                                </td>
                                <td class="text-truncate">
                                    @foreach($perbadge->user->badges as $badges)
                                    <span class="badge badge-info badge-md">
                                        @if($badges->callcenter != null)
                                        {{$badges->callcenter->name}}
                                        @endif
                                    </span>
                                    @endforeach
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('#p_id').change(function(){
        $.ajax({
            url: "{{url('get-badges-by-user')}}",
            type: "POST",
            data: {
                user_id: $(this).val(),
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function(result) {
                if(result.status == true){

                    if(result.data.length != 0){
                        var selectedValues = new Array();
                        for(var i = 0; i < result.data.length; i++){
                            selectedValues[i] = result.data[i].b_id;
                        }
                        $('#b_id').val(selectedValues);
                        $('#b_id').trigger('change');
                    }else{
                        $('#b_id').val('');
                        $('#b_id').trigger('change');
                    }
                }else{
                    $('#b_id').val('');
                    $('#b_id').trigger('change');
                }
                console.log(result);
            }
        });

    })
</script>
@endpush