@extends('layouts.admin')
@section('content')
@if($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="content-body">
    <!-- users view start -->
    <section class="users-view">
        <!-- users view media object start -->
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media">
                                <a class="mr-1" href="#">
                                    <img src="{{$data->image}}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="64" width="64">
                                </a>
                                <div class="media-body pt-25">
                                    <h4 class="media-heading"><span class="users-view-name">{{$data->fullName}} </span></h4>
                                    <span>ID:</span>
                                    <span class="users-view-id">{{$data->regId}}</span><br>
                                    <span>PHONE:</span>
                                    <span class="users-view-id">{{$data->phn_no}}</span>
                                </div>
                            </div>
                            <span class="badge badge-info badge-md mt-1">{{$data->city->name}} | {{$data->territory->name}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card pull-up mb-1">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Total Order</h6>
                                            <h3>{{ count($data->customer_order) }}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-check success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card pull-up mb-1">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Order Completed</h6>
                                            <h3>{{ $data->completed_order }}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-check success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card pull-up mb-1">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Order Not Completed</h6>
                                            <h3>{{ $data->completed_not_order }}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-check success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card pull-up mb-1">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Total Amount </h6>
                                            <h3>Rs. {{$data->total_amount}}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-credit-card success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card pull-up mb-1">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Amount Paid </h6>
                                            <h3>Rs. {{$data->amount_recieved}}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-credit-card success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Amount Not Paid </h6>
                                            <h3>Rs. {{$data->amount_not_recieved}}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-credit-card success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- users view media object ends -->
        <!-- users view card data start -->
        <div class="row">
            <div id="recent-sales" class="col-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="content-header-title mb-0 d-inline-block">Order List</h3>
                    </div>
                    <div class="card-content">
                        <div class="table-responsive pt-0" style="padding: 2%;">
                            <table class="table table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Service</th>
                                        <th>Total Amount</th>
                                        <th>Order Status</th>
                                        <th>Created Date</th>
                                        <th>Assigned To</th>
                                        <th>Payment Method</th>
                                        <th>Amount Received</th>
                                        <th>Address</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data->customer_order as $orders)
                                    <tr>
                                        <td class="text-truncate">{{$orders->order_id}}</td>
                                        <td class="text-truncate">
                                          <span class="badge badge-info">{{$orders->service->name}}</span>
                                        </td>
                                        <td class="text-truncate">Rs. {{$orders->total_amount}}</td>
                                        <td class="text-truncate"><span class="badge badge-info">{{$orders->status}}</span></td>
                                        <td class="text-truncate">{{date('l jS \of F Y h:i:s A', strtotime($orders->create_at))}}</td>
                                        <td class="text-truncate">
                                            @if($orders->assign_to != null)
                                            <span class="badge badge-success">{{$orders->assign_person->fullName}}</span>
                                            @else
                                            <span class="badge badge-danger">No Assigned</span>
                                            @endif
                                        </td>
                                        <td class="text-truncate">{{$orders->payment_method}}</td>
                                        <td class="text-truncate">
                                            @if($orders->amount_received != null)
                                            <span class="badge badge-success">Rs. {{$orders->amount_received}}</span>
                                            @else
                                            <span class="badge badge-danger">No Received</span>
                                            @endif
                                        </td>
                                        <td class="text-truncate">{{$orders->full_address}}</td>
                                        <td>
                                            <div class="buy-now">
                                                <a href="{{ route('jobs.show', $orders->order_id) }}" class="btn btn-sm btn-outline-info round" >View Details</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- users view card details ends -->
    </section>
    <!-- users view ends -->
</div>
@endsection
@push('scripts')
<script>
    
</script>
@endpush