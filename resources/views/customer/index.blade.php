@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">My Customers</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Customers
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Customers List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Full Name</th>
                                <th>Registered Mobile number</th>
                                <th>Completed order</th>
                                <th>Area</th>
                                <th>City</th>
                                <!-- 	<th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $customers)
                            <tr>
                                <td class="text-truncate">{{$customers->regId}}</td>
                                <td class="text-truncate"><a href="{{route('customer.show', $customers->regId)}}">{{$customers->fullName}}</a></td>
                                <td class="text-truncate">{{$customers->phn_no}}</td>
                                <td class="text-truncate">{{$customers->orders_count}}</td>
                                <td class="text-truncate">{{$customers->area}}</td>
                                <td class="text-truncate">{{$customers->city}}</td>
                                <!-- <td>
                                    <div class="buy-now"><a href="{{ route('customer.edit', $customers->regId) }}" class="btn btn-info btn-glow round px-1" style="  padding: 0.5rem 0.5rem; font-size: 0.5rem;">Edit Profile</a></div>
                                    </td> -->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush