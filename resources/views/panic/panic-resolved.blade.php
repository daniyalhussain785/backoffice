@extends('layouts.admin')
   
@section('content')
     
       
          
        <div class="row">


          <div id="recent-sales" class="col-12 col-md-12">

           @if($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{$message}}</p>
          </div>
         @endif
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Resolved Panic Lists  </h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                <div class="table-responsive" style="padding: 2%;">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th class="border-top-0">Panic ID</th>
                        <th class="border-top-0">Job ID</th>
                        <th class="border-top-0">Reported By</th>
                        <th class="border-top-0">Reporter Type</th>
                        <th class="border-top-0">Address</th>
                        <th class="border-top-0">Contact</th>
                        <th class="border-top-0">Reason</th>
                        <th class="border-top-0">Status</th>
                        <th class="border-top-0">Job Date</th>
                        <th class="border-top-0">View Details</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($data as $panic)
                       <tr>
                          <td class="text-truncate">{{$panic->pid}}</td>
                          <td class="text-truncate">{{$panic->job_id}}</td>
                          <td class="text-truncate">{{$panic->fullName}}</td>
                          <td class="text-truncate">{{$panic->role_name}}</td>
                          <td class="text-truncate">{{$panic->pri_add}}</td>
                          <td class="text-truncate">{{$panic->phn_no}}</td>
                          <td class="text-truncate">{{$panic->panic_reason}}</td>
                          <td class="text-truncate"><spen class="badge badge-primary">{{$panic->panic_name}}</spen></td>
                          <td class="text-truncate">{{ date("d-m-Y", strtotime($panic->pdate)) }}</td>
                          <td>
                          <div><a href="{{ route('panic.show', $panic->pid) }}" class="btn btn-sm btn-outline-info round">View Details</a></div>
                          </td>
                         
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
</div>

   


@endsection

@push('scripts')

@endpush