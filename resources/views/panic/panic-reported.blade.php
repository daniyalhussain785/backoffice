@extends('layouts.admin')
@section('content')
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Active Panic Reported List  </h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                    <div class="table-responsive" style="padding: 2%;">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th class="border-top-0">Panic ID</th>
                                    <th class="border-top-0">Job ID</th>
                                    <th class="border-top-0">Reported By</th>
                                    <th class="border-top-0">Time Passed Since Reported</th>
                                    <th class="border-top-0">Reason</th>
                                    <th class="border-top-0">Status</th>
                                    <th class="border-top-0">Assigned Supervisor</th>
                                    <th class="border-top-0">View Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $panic)
                                <tr>
                                    <td class="text-truncate">{{$panic->id}}</td>
                                    <td class="text-truncate">{{$panic->order_id}}</td>
                                    <td class="text-truncate">
                                    @if($panic->role_id == 1)
                                        Panic by Worker, <a href="{{route('kama.show', $panic->user->regId)}}">{{$panic->user->fullName}}</a><br>
                                        Mobile: {{$panic->user->phn_no}}<br><br>
                                        Customer, <a href="{{route('customer.show', $panic->user->regId)}}">{{$panic->order->user->fullName}}</a><br>
                                        Location: {{$panic->order->full_address }}<br>
                                        Job# {{$panic->order->order_id }}: {{$panic->order->service->name}} booked for<br> {{date("l jS \of F Y h:i A", strtotime($panic->order->create_at))}}
                                    @else
                                        Panic by Customer, <a href="{{route('customer.show', $panic->user->regId)}}">{{$panic->user->fullName}}</a><br>
                                        Mobile: {{$panic->user->phn_no}}<br>
                                        Location: {{$panic->order->full_address }}<br>
                                        Time: {{$panic->created_at->format('g:i A, d-M-Y')}}<br><br>
                                        Assigned Worker: <a href="{{route('kama.show', $panic->order->assign_person->regId)}}">{{$panic->order->assign_person->fullName}}</a><br>
                                        Job# {{$panic->order->order_id }}: {{$panic->order->service->name}} booked for<br> {{date("l jS \of F Y h:i A", strtotime($panic->order->create_at))}}
                                    @endif
                                    </td>
                                    <td class="text-truncate">
                                      <span class="badge badge-info badge-md">{{ Carbon\Carbon::parse($panic->created_at)->diffForHumans()}}</span>
                                    </td>
                                    <td class="text-truncate">{{$panic->reason}}</td>
                                    <td class="text-truncate">
                                        <spen class="badge badge-primary">{{($panic->status == 2 ? 'Un-Resolved' : 'Resolved')}}</spen>
                                    </td>
                                    <td class="text-truncate"><a href="{{ route('supervisor.show', $panic->order->assign_person->supervisor->regId) }}">{{$panic->order->assign_person->supervisor->fullName}}</a></td>
                                    <td>
                                        <div class="buy-now"><a href="{{ route('panic.show', $panic->id) }}" class="btn btn-sm btn-outline-info round" >View Details</a></div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
@endpush