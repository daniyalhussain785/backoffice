@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">My Faqs</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">List Faqs
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Faqs List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($faqs as $faq)
                            <tr>
                                <td class="text-truncate">{{$faq->id}}</td>
                                <td class="text-truncate">{{$faq->question}}</td>
                                <td class="text-truncate">{{ \Illuminate\Support\Str::limit($faq->answer, 60, $end='...') }}</td>
                                <td class="text-truncate">
                                    @if($faq->role_id == 1)
                                    <!-- kamay -->
                                    <button class="btn btn-sm btn-secondary">Kamay</button>
                                    @elseif($faq->role_id == 2)
                                    <!-- supervisor -->
                                    <button class="btn btn-sm btn-warning">Supervisor</button> 
                                    @else
                                    <!-- customer -->
                                    <button class="btn btn-sm btn-primary">Customer</button>
                                    @endif
                                </td>
                                <td align="center">
                                    <div class="fonticon-wrap">
                                        <a href="{{ route('faqs.edit', $faq->id) }}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush