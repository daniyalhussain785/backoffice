@extends('layouts.app')
    @section('pageTitle', 'Login')
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="row flexbox-container">
              <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                  <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                    <div class="card-header border-0">
                      <div class="card-title text-center">
                        <a class="navbar-brand" href="index.html">
                        <img class="brand-logo" alt="modern admin logo" src="{{ asset('admin/img/kwsb-logo.png') }}" width="120">
                      </a>
                      </div>
                      
                    </div>
                    <div class="card-content">
                      
                      <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using Account
                          Details</span></p>
                      <div class="card-body">
                        <form method="POST" class="form-horizontal" action="{{ route('login') }}" >
                           @csrf
                          <fieldset class="form-group position-relative has-icon-left">
                            

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" id="user-name" name="email" value="{{ old('email') }}" placeholder="Your Email" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <div class="form-control-position">
                              <i class="la la-user"></i>
                            </div>
                          </fieldset>
                          <fieldset class="form-group position-relative has-icon-left">
                            
                             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter Password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <div class="form-control-position">
                              <i class="la la-key"></i>
                            </div>
                          </fieldset>
                          <div class="form-group row">
                            <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                              <fieldset>
                                
                                 <input class="chk-remember" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember-me"> {{ __('Remember Me') }}</label>
                              </fieldset>
                            </div>
                            <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right">
                                @if (Route::has('password.request'))
                                    <a class="card-link" href="#">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif</div>
                          </div>
                          <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i>{{ __('Login') }}</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

        </div>
      </div>
    </div>
    <!-- END: Content-->

@endsection