@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Payments</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Payments
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Payment Options</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        @foreach($payments as $payments)
                        <div class="card-body">
                            <form class="form" action="{{route('admin.payment.update', $payments->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf   
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Name <span>*</span></label>
                                                <input type="text" id="name" class="form-control" placeholder="Payment Name" name="name" required="required" value="{{$payments->name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Select Status <span>*</span></label>
                                                <select name="status" id="status" class="form-control">
                                                    @if($payments->status == 1)
                                                    <option value="1" selected>Active</option>
                                                    <option value="0">Deactive</option>
                                                    @else
                                                    <option value="1">Active</option>
                                                    <option value="0" selected>Deactive</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <textarea name="description" id="description"  rows="5" class="form-control">{{$payments->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                    <i class="la la-check-square-o"></i> Update
                                    </button>
                                </div>
                            </form>
                        </div>
                        <hr>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-4">
               <div class="card">
                  <div class="card-header">
                     <h4 class="card-title" id="basic-layout-colored-form-control">Information</h4>
                     <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                     <div class="heading-elements">
                        <ul class="list-inline mb-0">
                           <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                           <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                           <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                           <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="card-content collapse show">
                     <div class="card-body pt-0">
                        @if($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                              @foreach($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                           <ul>
                              <li>{!! \Session::get('success') !!}</li>
                           </ul>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
        </div>
    </section>
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">JazzCash Credentials</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" action="{{route('admin.payment.update', $jazzcash_credentials->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf   
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="merchant_id">MERCHANT_ID <span>*</span></label>
                                                <input type="text" id="merchant_id" class="form-control" placeholder="MERCHANT_ID" name="merchant_id" required="required" value="{{$jazzcash_credentials->merchant_id}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="password">PASSWORD <span>*</span></label>
                                                <input type="text" id="password" class="form-control" placeholder="PASSWORD" name="password" required="required" value="{{$jazzcash_credentials->password}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="integrity_salt">INTEGRITY_SALT <span>*</span></label>
                                                <input type="text" id="integrity_salt" class="form-control" placeholder="INTEGRITY_SALT" name="integrity_salt" required="required" value="{{$jazzcash_credentials->integrity_salt}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="currency_code">CURRENCY_CODE <span>*</span></label>
                                                <input type="text" id="currency_code" class="form-control" placeholder="CURRENCY_CODE" name="currency_code" required="required" value="{{$jazzcash_credentials->currency_code}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="version">VERSION <span>*</span></label>
                                                <input type="text" id="version" class="form-control" placeholder="VERSION" name="version" required="required" value="{{$jazzcash_credentials->version}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="language">LANGUAGE <span>*</span></label>
                                                <input type="text" id="language" class="form-control" placeholder="LANGUAGE" name="language" required="required" value="{{$jazzcash_credentials->language}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-inline">
                                                <label for="live">CLICK CHECKBOX FOR LIVE SANDBOX URL <span>*</span></label>
                                                <input type="checkbox" id="live" class="ml-1 form-control" name="sand_box" {{$jazzcash_credentials->sand_box == 1 ? 'checked' : ''}}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                    <i class="la la-check-square-o"></i> Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush