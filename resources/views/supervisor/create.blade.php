@extends('layouts.admin')
@push('styles')
    <style>
        .pac-container {
            z-index: 9999999 !important;
            top: 65px !important;
            left: 15px !important;
        }
        #address-input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: red;
            opacity: 1; /* Firefox */
        }

        #address-input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: red;
        }

        #address-input::-ms-input-placeholder { /* Microsoft Edge */
            color: red;
        }
    </style>
@endpush
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Add Supervisor</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Register supervisor</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Supervisor Details</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" action="{{route('supervisor.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf   
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Full Name <span class="required">*</span></label>
                                                <input type="text" id="name" class="form-control" placeholder="Supervisor Name" name="supervisor_name" maxlength="50" value="{{ old('supervisor_name') }}" required="required" onKeyPress="return ValidateAlpha(event);">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">CNIC <span class="required">*</span></label>
                                                <input type="text" data-inputmask="'mask': '99999-9999999-9'"  placeholder="XXXXX-XXXXXXX-X" class="form-control" id="cnic_number" name="cnic_number" value="{{ old('cnic_number') }}" required="required">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Email address</label>
                                                <input type="email" id="email_id" class="form-control" placeholder="Email address" name="email_id" value="{{ old('email_id') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Mobile number <span class="required">*</span></label>
                                                <input type="text" data-inputmask="'mask': '+\\92-999-9999999'"  class="form-control" placeholder="+92-XXX-XXXXXXX" id="cell_no" name="cell_no" required value="{{Request::old('cell_no')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Expertise <span class="required">*</span></label>
                                                <select name="selected_expertise[]" id="expertise_id" class="form-control select2" multiple required>
                                                @foreach($expertise as $ex)
                                                    @if (old('selected_expertise'))
                                                        <option value="{{ $ex->s_id }}" {{ in_array($ex->s_id, old('selected_expertise')) ? 'selected' : '' }}>{{ $ex->name}}</option>   
                                                    @else
                                                        <option value="{{ $ex->s_id }}">{{ $ex->name}}</option>
                                                    @endif 
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload profile image <span class="required">*</span></label>
                                                <input type="file" class="form-control browse btn" placeholder="Upload File" name="image" required value="{{ old('image') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="projectinput1">Address</label>
                                                <div class="form-input">
                                                    <input type="text" id="address-input" name="address" class="form-control map-input pac-target-input" placeholder="Enter a location" autocomplete="off" value="{{ old('address') }}">
                                                    <i class="icofont-location-pin"></i>
                                                    <input type="hidden" name="lat" id="address-latitude" value="{{ old('lat') }}">
                                                    <input type="hidden" name="long" id="address-longitude" value="{{ old('long') }}">
                                                    <input type="hidden" name="city" id="address-city" value="{{ old('city') }}">
                                                    <input type="hidden" name="area" id="address-area" value="{{ old('area') }}">
                                                </div>
                                                <div id="address-map-container" style="width:100%;height:150px;margin-bottom: 15px;">
                                                    <div style="width: 100%; height: 100%" id="address-map"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Select City <span class="required">*</span></label>
                                                <input type="text" readonly id="selected_city" class="form-control" required value="{{ old('city') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Select Area <span class="required">*</span></label>
                                                <input type="text" readonly id="selected_home_area" class="form-control" required value="{{ old('area') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="projectinput1">Select territory</label>
                                                <select name="selected_terr[]" id="selected_terr" class="form-control select2" multiple>
                                                
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
               <div class="card">
                  <div class="card-header">
                     <h4 class="card-title" id="basic-layout-colored-form-control">Information</h4>
                     <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                     <div class="heading-elements">
                        <ul class="list-inline mb-0">
                           <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                           <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                           <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                           <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="card-content collapse show">
                     <div class="card-body pt-0" id="error_box">
                        @if($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                              @foreach($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('error'))
                        <div class="alert alert-danger">
                           <ul>
                              <li>{!! \Session::get('error') !!}</li>
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                           <ul>
                              <li>{!! \Session::get('success') !!}</li>
                           </ul>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
        </div>
    </section>
</div>
@stop
@section('script')
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
    $(":input").inputmask();
</script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAggB51ZRomTQ28c6SIlT0Zkjtd2boQ3V0&libraries=places&callback=initialize" async defer></script>
<script>
    var get_city = {!! json_encode($cityData->toArray()) !!};
    function initialize() {
    
    	$('form').on('keyup keypress', function(e) {
    		var keyCode = e.keyCode || e.which;
    		if (keyCode === 13) {
    			e.preventDefault();
    			return false;
    		}
    	});
    	const locationInputs = document.getElementsByClassName("map-input");
    
    	const autocompletes = [];
    	const geocoder = new google.maps.Geocoder;
    	for (let i = 0; i < locationInputs.length; i++) {
    
    		const input = locationInputs[i];
    		const fieldKey = input.id.replace("-input", "");
    		const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';
    
    		const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -33.8688;
    		const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 151.2195;
    
    		const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
    			center: {lat: latitude, lng: longitude},
    			zoom: 13
    		});
			console.log(map);
    		const marker = new google.maps.Marker({
    			map: map,
    			position: {lat: latitude, lng: longitude},
    		});
    
    		marker.setVisible(isEdit);
    
    		const autocomplete = new google.maps.places.Autocomplete(input);
    		autocomplete.key = fieldKey;
    		autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
    	}
    
    	for (let i = 0; i < autocompletes.length; i++) {
    		const input = autocompletes[i].input;
    		const autocomplete = autocompletes[i].autocomplete;
    		const map = autocompletes[i].map;
    		const marker = autocompletes[i].marker;
    
    		google.maps.event.addListener(autocomplete, 'place_changed', function () {
    			marker.setVisible(false);
    			const place = autocomplete.getPlace();
    
    			geocoder.geocode({'placeId': place.place_id}, function (results, status) {
    				if (status === google.maps.GeocoderStatus.OK) {
    					const lat = results[0].geometry.location.lat();
						const lng = results[0].geometry.location.lng();
                        var sublocality;
						for (var i = 0; i < results[0].address_components.length; i++) {
							if (results[0].address_components[i].types[0] == "locality") {
								//this is the object you are looking for City
								city = results[0].address_components[i];
							}
                            var component = results[0].address_components[i];
                            if (!sublocality && component.types.indexOf("sublocality_level_1") > -1){
                                sublocality = component.long_name;
                            }else if(component.types.indexOf("neighborhood") > -1){
                                sublocality = component.long_name;
                            }
                        }
                    
                        var checker = 0;
                        for(var i = 0; i < get_city.length; i++){
                            if(get_city[i].name == city.long_name){
                                checker = 1;
                            }
                        }
						if(checker == 1){
                            setLocationCoordinates(autocomplete.key, lat, lng, city, sublocality);
                            $('#address-input').removeClass('error');
						}else{
							$('#address-input').attr('placeholder','Sorry, Right Now, we are not Operational in this City');
                            $('#address-input').val('');
							$('#address-latitude').val('0');
							$('#address-longitude').val('0');
							$('#address-input').addClass('error');
							$('#address-city').val('');
                            $('#selected_city').val('')
                            $('#selected_terr').html('')
						}
    				}
    			});
    
    			if (!place.geometry) {
    				window.alert("No details available for input: '" + place.name + "'");
    				input.value = "";
    				return;
    			}
    
    			if (place.geometry.viewport) {
    				map.fitBounds(place.geometry.viewport);
    			} else {
    				map.setCenter(place.geometry.location);
    				map.setZoom(17);
    			}
    			marker.setPosition(place.geometry.location);
    			marker.setVisible(true);
    
    		});
    	}
    	}
    
    	function setLocationCoordinates(key, lat, lng, city, sublocality) {
			const latitudeField = document.getElementById(key + "-" + "latitude");
			const longitudeField = document.getElementById(key + "-" + "longitude");
			const cityField = document.getElementById(key + "-" + "city");
            const areaField = document.getElementById(key + "-" + "area");
            const selectedCityField = document.getElementById("selected_city");
            const selectedHomeArea = document.getElementById("selected_home_area");
			latitudeField.value = lat;
			longitudeField.value = lng;
			cityField.value = city.long_name;
            areaField.value = sublocality;
            selectedCityField.value = city.long_name;
            selectedHomeArea.value = sublocality;
            $.ajax({
                url: "{{url('get-area-by-cityname')}}",
                type: "POST",
                data: {
                    city_id: city.long_name,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    console.log(result);
                    $.each(result.area, function(key, value) {
                        $("#selected_area").append('<option value="' + value.terr_id + '">' + value.name + '</option>');
                        $("#selected_terr").append('<option value="' + value.terr_id + '">' + value.name + '</option>');
                    });
                    //$('#city').html('<option value="">Select City First</option>');
                }
            });
		}
		
		var addressInputElement = $('#address-input');
			addressInputElement.on('focus', function () {
			var pacContainer = $('.pac-container');
			$(addressInputElement.parent()).append(pacContainer);
		})

		$(document).ready(function () {
			$('#nav-reg11 .mregisterform input[type=file]').change(function () {
				$('#btnUpload').show();
				$('#divFiles').html('');
				for (var i = 0; i < this.files.length; i++) {
					console.log(URL.createObjectURL(this.files[i]));
					var fileId = i;
					$("#divFiles").append('<li>' +
											'<img src="' + URL.createObjectURL(this.files[i]) + '">' +
										'</li>');
				}
			});
            $('.form').on('submit', function(){
                $('input').removeClass('alert-danger');
                $('select').removeClass('alert-danger');
                var minimum = 4;
                $error = '<div class="alert alert-danger"><ul>';
                $error_count = 0;
                if(minimum <= $("#expertise_id").select2('data').length){
                    $error += "<li>Maximun 3 Expertise</li>";
                    $error_count = 1;
                    $('#expertise_id').addClass('alert-danger');
                }
                var cnic_number = $('#cnic_number').val();
                cnic_number = cnic_number.split('_').join('');
                if(cnic_number.length != 15){
                    $('#cnic_number').addClass('alert-danger');
                    $error += "<li>CNIC Number Invalid</li>";
                    $error_count = 1;
                }
                var cell_no = $('#cell_no').val();
                cell_no = cell_no.split('_').join('');
                if(cell_no.length != 15){
                    $('#cell_no').addClass('alert-danger');
                    $error += "<li>Mobile number Invalid</li>";
                    $error_count = 1;
                }
                if($('#email_id').val() != ''){
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(regex.test($('#email_id').val()) == false){
                        $('#email_id').addClass('alert-danger');
                        $error += "<li>Email Address Invalid</li>";
                        $error_count = 1;
                    }
                }
                $error += '</ul></div>';
                if($error_count != 1){
                    return true;
                }else{
                    $('#error_box').html($error);
                    $("input.alert-danger").first().focus();
                    $("select.alert-danger").first().focus();
                    return false;
                }

            });
            $("#cnic_number").blur(function () {
                var cnic_number = $(this).val();
                cnic_number = cnic_number.split('_').join('');
                if (cnic_number.length != 15){
                    $(this).addClass('alert-danger');
                }else{
                    $(this).removeClass('alert-danger');
                }
            });
            $("#cell_no").blur(function () {
                var cell_no = $(this).val();
                cell_no = cell_no.split('_').join('');
                if(cell_no.length != 15){
                    $(this).addClass('alert-danger');
                }else{
                    $(this).removeClass('alert-danger');
                }
            });
            $('#email_id').blur(function(){
                if($(this).val() != ''){
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(regex.test($(this).val()) == false){
                        $(this).addClass('alert-danger');
                    }else{
                        $(this).removeClass('alert-danger');
                    }
                }
            })
		});

        function ValidateAlpha(evt)
        {
            var keyCode = (evt.which) ? evt.which : evt.keyCode
            if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
             
            return false;
                return true;
        }

	</script>
@endsection