@extends('layouts.admin')
   
@section('content')
     
         
          

        <div class="row">

 

          <div id="recent-sales" class="col-12 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">
                  Assigned workers of supervisor: <strong>{{$supervisorName->fullName}}</strong>
                </h4>
                <br>
                <p>When clicked on assigned worker, it will take backoffice to the list of workers assigned to that supervisor.</p>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="card-body card-dashboard">
                <div class="table-responsive" style="padding: 2%;">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th class="border-top-0">Worker ID</th>
                        <th class="border-top-0">Name</th>
                        <th class="border-top-0">Mobile no</th>
                        <th class="border-top-0">Services offered</th>
                        <th class="border-top-0">City</th>
                        <th class="border-top-0">Status</th>
                        <th class="border-top-0">Date of registration</th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach($data as $kama)
                       <tr>
                        <td class="text-truncate">{{$kama->regId}}</td>
                          <td class="text-truncate"><a href="{{route('kama.show', $kama->regId)}}"> {{$kama->fullName}}</a></td>
                          <td class="text-truncate">{{$kama->phn_no}}</td>
                          <td><span class="badge badge-primary">{{$kama->expertise}}</span></td>
                          <td class="text-truncate">{{$kama->city_name}}</td>
                          <td class="text-truncate">  <span class="badge badge-{{str_replace(' ', '', $kama->status)}}">{{$kama->status}}</span></td>
                          
                          <td class="text-truncate">{{date("d-m-Y", strtotime($kama->created_at))}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
</div>
@endsection
@push('scripts')
<!-- <script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('kamayview') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'regId', name: 'regId'},
            {data: 'fullName', name: 'fullName'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script> -->
@endpush

