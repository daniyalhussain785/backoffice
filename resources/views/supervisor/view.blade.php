@extends('layouts.admin')
   
@section('content')
     
         

       
        <div class="content-body"><!-- users view start -->
              <section class="users-view">
                <!-- users view media object start -->
                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="media mb-2">
                      <a class="mr-1" href="#">
                        <img src="{{ asset($data[0]->image) }}" alt="Upload image" class="users-avatar-shadow rounded-circle" height="64" width="64">
                      </a>
                      <div class="media-body pt-25">
                        <h4 class="media-heading"><span class="users-view-name">@ {{$data[0]->fullName}}</span></h4>
                        <span>ID:</span>
                        <span class="users-view-id">{{$data[0]->regId}}</span>
                         <?php for ($i=0; $i < 5; $i++){
                                    if (round($ratings) > $i) {?>
                                    <span class="fa fa-star checked"></span>
                                    <?php
                                    }else{?>
                                      <span class="fa fa-star"></span>
                                  <?php  }
                                }?>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 col-lg-6 col-12">
                    <a href="#assignForm" data-target="#assignForm" data-toggle="modal" class="assignSupervisor btn btn-sm mr-25 border" style="margin-right: 10px;"><i class="la la-phone"></i></a>
                    <a href="https://wa.me/{{$data[0]->phn_no}}" class="btn btn-sm mr-25 border" style="margin-right: 10px;"><i class="la la-whatsapp"></i></a>
                  </div>

                  <div class="col-xl-4 col-lg-6 col-12">
                    <a href="{{ route('supervisor.assigned-kamay-to-supervisor', $data[0]->regId) }}">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="warning display-4">{{$kamayCount}}</h3>
                                            <h6>Assigned Workers</h6>
                                        </div>
                                        <div>
                                            <i class="icon-user-follow warning font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                     <ul>
                      @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                     </ul>
                    </div>
                    @endif
                <!-- users view media object ends -->
                <!-- users view card data start -->
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-12 col-md-7">
                          <table class="table table-borderless">
                            <tbody>
                              <tr>
                                <td colspan="2">
                                  <span class="btn-success btn-sm float-left"> 
                                    <?php echo $getSupervisorExpertise->expertise; ?>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td>Registered:</td>
                                <td>{{date("d-m-Y", strtotime($data[0]->register_date))}}</td>
                              </tr>
                              
                              
                               <tr>
                                <td>CNIC:</td>
                                <td class="users-view-role">{{$data[0]->cnic}}</td>
                              </tr>
                              <tr>
                                <td>Email:</td>
                                <td class="users-view-role">{{$data[0]->email}}</td>
                              </tr>
                              
                              <tr>
                                <td>City:</td>
                                <td>{{$data[0]->city}}</td>
                              </tr>
                              <tr>
                                <td>Area:</td>
                                <td>{{$data[0]->area}}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        
                      </div>
                      
                    </div>
                  </div>
                </div>
                <!-- users view card data ends -->
                <!-- users view card details start -->
                
                <!-- users view card details ends -->
                <div class="modal animated slideInDown text-left show" id="assignForm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" style="display: none; padding-right: 17px;">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">   
                      <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel77">Call supervisor: <spen style="margin-left: 30px"> <i class="la la-phone"></i> {{$data[0]->phn_no}}</spen></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      
                    </div>
                  </div>
                </div>
                

              </section>
              <!-- users view ends -->
            </div>
                  
       
@endsection
@push('scripts')

@endpush

