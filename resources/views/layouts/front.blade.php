<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kamay - @yield('title')</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/main.css')}}" id="color-switcher-link">
    <link rel="stylesheet" href="{{ asset('css/animations.css')}}">
    <link rel="stylesheet" href="{{ asset('css/fonts.css')}}">

    <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js')}}"></script>

 <script type="text/javascript">
        function zoom() {
            document.body.style.zoom = "90%" 
        }
</script>
<style>
        html {
          scroll-behavior: smooth;
        }
</style>
</head>


<body onload="zoom()">
    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <div class="widget widget_search">
            <form method="get" class="searchform form-inline" action="/">

                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form>
        </div>
    </div>

    @include('inc.nav')



        
    @yield('content')
        
    </div>
            <footer class="page_footer ds section_padding_top_65 section_padding_bottom_50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 to_animate" data-animation="scaleAppear">
                            <div class="widget widget_text footer-email">
                                <p>
                                    <img src="images/logo_bottom.png" alt="">
                                </p>
                                <p>34/J/3, Block-06, PECHS </p>
                                <p>Karachi</p>
                                <p>
                                    <a href="info@kamay.pk">info@Kamay.pk</a>
                                </p>
                                <p>+92-21-34301488 / 89</p>
                                <ul id="menu-social-menu-short-footer" class="social-navigation social-footer">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-783">
                                        <a href="#" class="social-icon bg-icon rounded-icon soc-facebook"></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-782">
                                        <a href="#" class="social-icon bg-icon rounded-icon soc-twitter"></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-784">
                                        <a href="#" class="social-icon bg-icon rounded-icon soc-skype"></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-784">
                                        <a href="#" class="social-icon bg-icon rounded-icon soc-pinterest"></a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-784">
                                        <a href="#" class="social-icon bg-icon rounded-icon soc-linkedin"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 to_animate">
                            <div class="widget_text">
                                <h5 class="widget-title m-uppercase w-hours-block">Working Hours</h5>
                                <div class="border-paragraphs-footer">
                                    <p>Monday – Friday
                                        <span class="float-right">09.00 – 17.00</span>
                                    </p>
                                    <p>Saturday
                                        <span class="float-right">09.00 – 15.00</span>
                                    </p>
                                    <p>Sunday
                                        <span class="float-right">Closed</span>
                                    </p>
                                    <br>
                                    <div class="widget widget_mailchimp">

                                        <h4 class="m-uppercase widget-title">Newsletter Signup</h4>

                                        <form class="signup form-inline" action="/" method="get">
                                            <div class="form-group">
                                                <input name="email" type="email" class="mailchimp_email form-control footer-mailchimp" placeholder="your@email.com">
                                            </div>
                                            <button type="submit" class="theme_button_footer">Send</button>
                                            <div class="response"></div>
                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 to_animate" data-animation="scaleAppear">
                            <div class="discount-block">
                                <a href="#">
                                    <span>Get
                                        <span>10%</span> off</span>
                                    <h5>Plumbing Services</h5>
                                    <p>Redeem Code: wannadiscount</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <section class="ds page_copyright table_section section_padding_25">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p>&copy; Powered
                                <!--<i class="rt-icon2-heart highlight"></i>-->
                                <a href="#">by HTA</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>



        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <script src="{{ asset('js/compressed.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>


</body>

</html>
