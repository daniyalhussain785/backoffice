<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <title>{{ config('app.name', 'Kamay Backoffice') }}</title> -->
    <title>@yield('pageTitle') - Kamay Backoffice</title> 
    <!-- Scripts -->
    <link rel="apple-touch-icon" href="{{ asset('admin/img/favicon.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('admin/img/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
    rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
    rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{asset('admin/css/datatables.min.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/app.css') }}">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vertical-menu-modern.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/simple-line-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/jquery-ui.min.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- END Page Level CSS-->
    
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}">
    <style type="text/css">
        .file {
              visibility: hidden;
              position: absolute;
            }
            .checked {
              color: orange;
            }

            #chartdiv {
              width: 100%;
              height: 500px;
            }
    </style>
    @stack('styles')
</head>
<body class="vertical-layout vertical-menu-modern 2-columns  menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    @include('inc.nav')
    <div class="app-content content">
        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">Copyright &copy; <script>document.write(new Date().getFullYear())</script> <a class="text-bold-800 grey darken-2" href=""
                target="_blank">HTA </a>, All rights reserved. </span>
            <!-- <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span> -->
        </p>
    </footer>
   
    <script src="{{ asset('admin/js/vendors.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/js/app-menu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/customizer.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatable-basic.min.js') }}"></script>
    <script src="{{ asset('admin/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('admin/js/ajaxHelper.js') }}"></script>
    <script src="{{ asset('admin/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('admin/js/highlight.js') }}"></script>
    <script src="{{ asset('admin/js/custom_script.js') }}"></script>
     <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>
    
    @yield('script')

 


    @stack('scripts')
     <script>
        
        $(document).ready(function() {
            $(document).on('click', '#notf_user', function() {
                $('#user-notf-show').load($("#user-notf-show").data('href'));
            });
            $(document).on('click', '#panic', function() {
                $('#panic-notf-show').load($("#panic-notf-show").data('href'));
            });
            $(document).on('click', '#final-approval', function() {
                $('#final-approval-notf-show').load($("#final-approval-notf-show").data('href'));
            });
            $('#city').on('change', function() {
                var city_id = this.value;
                $("#selected_area").html('');
                $.ajax({
                    url: "{{url('get-area-by-city')}}",
                    type: "POST",
                    data: {
                        city_id: city_id,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function(result) {
                      console.log(result);
                        $.each(result.area, function(key, value) {
                            $("#selected_area").append('<option value="' + value.terr_id + '">' + value.name + '</option>');
                            $("#selected_terr").append('<option value="' + value.terr_id + '">' + value.name + '</option>');
                        });
                        //$('#city').html('<option value="">Select City First</option>');
                    }
                });
            });
        }); 
  </script>


<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script>
    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyCkZtXbTKSrhUDKUy2uciZ5ICpIp4_LYts",
            authDomain: "kamaycallcenter.firebaseapp.com",
            projectId: "kamaycallcenter",
            storageBucket: "kamaycallcenter.appspot.com",
            messagingSenderId: "777535574944",
            appId: "1:777535574944:web:8346187e7b7ff201f28ac5"
        };
        
        firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();
    
        function initFirebaseMessagingRegistration() {
                messaging
                .requestPermission()
                .then(function () {
                    return messaging.getToken()
                })
                .then(function(token) {
                    console.log(token);
    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        url: '{{ route("save-token") }}',
                        type: 'POST',
                        data: {
                            token: token
                        },
                        dataType: 'JSON',
                        success: function (response) {
                            console.log('Token saved successfully.');
                        },
                        error: function (err) {
                            console.log('User Chat Token Error'+ err);
                        },
                    });
    
                }).catch(function (err) {
                    console.log('User Chat Token Error'+ err);
                });
        }  
        
        messaging.onMessage(function(payload) {
            console.log(payload.notification);
            const noteTitle = payload.notification.title;
            const noteOptions = {
                body: payload.notification.body,
                icon: payload.notification.icon,
                click_action: payload.notification.click_action, // To handle notification click when notification is moved to notification tray
                data: {
                    click_action: payload.notification.click_action
                }
            };
            notification = new Notification(noteTitle, noteOptions);
            notification.onclick = function(event) {
               event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open(payload.notification.click_action, '_blank');
            }
        });

        initFirebaseMessagingRegistration();
        
    </script>

</body>
</html>
