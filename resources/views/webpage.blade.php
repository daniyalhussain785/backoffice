@extends('layouts.front')

@section('title', 'Home')

@section('content')


        <!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
<div id="box_wrapper">
<!-- template sections -->
<section class="intro_section page_mainslider ds ">
   <div class="main-slider__scroll m-uppercase" id="main-slider__scroll">
      <a href="#scroll-down">
      <i class="fa fa-arrow-down"></i>
      <span>scroll</span>
      </a>
   </div>
   <div class="flexslider "  >
   <ul class="slides">
      <li>
         <img src="{{ asset('images/slide01.png')}}" alt="">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="slide_description_wrapper">
                     <div class="top-corner">
                     </div>
                     <div class="col-md-10 slide_description">
                        <div class="intro_slider_alt_navigation">
                           <ul class="flex-direction-nav">
                              <li class="flex-nav-prev">
                                 <a class="flex-prev" href="#"></a>
                              </li>
                              <li class="flex-nav-next">
                                 <a class="flex-next" href="#"></a>
                              </li>
                           </ul>
                        </div>
                        <div class="intro-layer" data-animation="slideExpandUp">
                           <h3  class="text-uppercase topmargin_40" style="
                              color: black;
                              ">
                              Looking for 
                              <br/> kaam kay log?
                           </h3>
                        </div>
                        <div class="intro-layer" data-animation="slideExpandUp">
                           <p class="grey bottommargin_40" style="
                              color: black;
                              " >
                              Connect with thousands of pre-screened, <br/>certified Kamay workers to do your job.
                           </p>
                        </div>
                        <div class="col-sm-12">
                           <a href= "#" class=" theme_button" href="#" style="font-size:15px;">Learn More</a>
                        </div>
                     </div>
                     <!-- eof .slide_description -->
                  </div>
                  <!-- eof .slide_description_wrapper -->
               </div>
               <!-- eof .col-* -->
            </div>
            <!-- eof .row -->
         </div>
         <!-- eof .container -->
      </li>
      <li>
         <img src="{{ asset('images/test image 2-13.png')}}" alt="">
         <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="slide_description_wrapper">
                  <div class="top-corner">
                  </div>
                  <div class="col-md-10 slide_description">
                     <div class="intro_slider_alt_navigation">
                        <ul class="flex-direction-nav">
                           <li class="flex-nav-prev">
                              <a class="flex-prev" href="#"></a>
                           </li>
                           <li class="flex-nav-next">
                              <a class="flex-next" href="#"></a>
                           </li>
                        </ul>
                     </div>
                     <div class="intro-layer" data-animation="slideExpandUp">
                        <h3 class="text-uppercase topmargin_60" style="
                           color: white;
                           ">
                           Download app
                           <br/> Get work done
                        </h3>
                     </div>
                     <div class="intro-layer" data-animation="slideExpandUp">
                        <p class="grey bottommargin_40" style="
                           color: white;
                           ">
                           Kamay app makes your life easy. 
                           Book service and track it being done.
                        </p>
                     </div>
                     <div>
                        <div class="row">
                           <div class="col-md-2 text-center col-xs-6" >
                              <img src="images/download button/get it button-06.png" alt="">
                           </div>
                           <div class="col-md-2 text-center col-xs-6" >
                              <img src="images/download button/get it button-07.png" alt="">
                           </div>
                        </div>
                        <!-- eof .slide_description -->
                     </div>
                     <!-- eof .slide_description_wrapper -->
                  </div>
                  <!-- eof .col-* -->
               </div>
               <!-- eof .row -->
            </div>
            <!-- eof .container -->
      </li>
  <li>
   <img src="{{ asset('images/home_slider_3.png')}}" alt="">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="slide_description_wrapper">
               <div class="top-corner">
               </div>
               <div class="col-md-10 slide_description">
                  <div class="intro_slider_alt_navigation">
                     <ul class="flex-direction-nav">
                        <li class="flex-nav-prev">
                           <a class="flex-prev" href="#"></a>
                        </li>
                        <li class="flex-nav-next">
                           <a class="flex-next" href="#"></a>
                        </li>
                     </ul>
                  </div>
                  <div class="intro-layer" data-animation="slideExpandUp">
                     <h3 class="col-md-8 text-uppercase topmargin_40" style="
                        color: black;">Buy hardware & tools on kamay
                     </h3>
                  </div>
                  <div class="intro-layer" data-animation="slideExpandUp">
                     <p class="col-md-6 grey bottommargin_40" style="
                        color: black;
                        ">   Electrical wire, auto part or paints – a wide range of accessories available at good price 
                     </p>
                  </div>
                  <div class="col-sm-12">
                     <a class="theme_button" href="/shop-right" style="font-size:15px;">Shop now</a>
                  </div>
               </div>
               <!-- eof .slide_description -->
            </div>
            <!-- eof .slide_description_wrapper -->
         </div>
         <!-- eof .col-* -->
      </div>
      <!-- eof .row -->
   </div>
   <!-- eof .container -->
</li>
</ul>
</div>
   <!-- eof flexslider -->
</section>
<section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h1>How It Works</h1>
               <div class="fw-heading">
                  <h5>Simple. Secure. Convenient</h5>
               </div>
            </div>
            <div class="row">
               <div class="col-md-3 col-sm-6">
                  <div class="teaser text-center with_background" >
                     <div class="teaser_icon highlight rounded-icon">
                        <i class="rt-icon2-download-outline"></i>
                     </div>
                     <h3 class="m-uppercase text-left " style="font-size:19px;">Download app</h3>
                     <p style="margin-bottom:-10px;">Using Kamay app is super convenient. Download from your app store.</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6">
                  <div class="teaser text-center with_background">
                     <div class="teaser_icon highlight rounded-icon">
                        <i class="rt-icon2-book"></i>
                     </div>
                     <h3 class="m-uppercase">Book service</h3>
                     <p style="margin-bottom:-10px;"> Select your service, get estimate, provide contact details & confirm booking.</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6">
                  <div class="teaser text-center with_background">
                     <div class="teaser_icon highlight rounded-icon">
                        <i class="rt-icon2-location"></i>
                     </div>
                     <h3 class="m-uppercase">Track Kamay</h3>
                     <p style="margin-bottom:-10px; margin-right:10px;">See who is coming. Track your kamay (worker) & job from start to end.</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-8">
                  <div class="teaser text-center with_background">
                     <div class="teaser_icon highlight rounded-icon">
                        <i class="rt-icon2-lightbulb3"></i>
                     </div>
                     <h3 class="m-uppercase">Pay & review</h3>
                     <p style="margin-bottom:-17px; margin-top:10px;">Once your job ends, pay with cash, card or online. And rate service.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="ls parallax section_padding_bottom_100 section_padding_top_100 services-section">
   <div class="container-fluid">
      <div class="container to_animate">
         <div class="heading-b">
            <h3>1000+ services available </h3>
            <p>
               Book unlimited blue-collar services for your home, business or community.
            </p>
         </div>
        <div class="row to_animate animated scaleAppear" data-animation="scaleAppear">
                <div class="col-sm-3 button-block col-xs-6">
                    <a href="/detail">
                       <i class= "rt-icon2-spanner-outline"></i>
                       <br/> Plumber
                   </a>
                </div>
                <div class="col-sm-3 button-block col-xs-6">
                   <a href="/detail">
                   <i class="rt-icon2-home-outline"></i>
                   <br/> Carpenter 
                   </a>
                </div>
                <div class="col-sm-3 button-block col-xs-6">
                   <a href="/detail">
                   <i class="rt-icon2-plug"></i>
                   <br/> Car mechanic 
                   </a>
                </div>
                <div class="col-sm-3 button-block col-xs-6" style="margin-bottom: 0px;">
                   <a href="/detail">
                   <i class="rt-icon2-paintbrush"></i>
                   <br/>
                   Painter
                   </a>

                </div>
                <div class="col-sm-3 button-block col-xs-6">
                    <a href="/detail">
                    <i class="rt-icon2-washingmachine"></i>
                    <br/> Appliance 
                    <br/>mechanic
                    </a>
                 </div>
                <div class="col-sm-3 button-block col-xs-6">
                    <a href="/detail">
                    <i class="fa fa-medkit"></i>
                    <br/> Nurse 
                    <br>attendant
                    </a>
                 </div>
                 <div class="col-sm-3 button-block col-xs-6">
                    <a href="/detail">
                    <i class="rt-icon2-scissors-outline"></i>
                    <br/> Salon 
                    <br>at home  
                    </a>
                 </div>
                <div class="col-sm-3 button-block col-xs-6">
                    <a href="/detail">
                    <i class="rt-icon2-lightbulb3"></i>
                    <br/> Cleaning 
                    <br> staff
                    </a>
                 </div>
            </div>
        <div class="text-center">
          <a href="/services" class="theme_button round-icon round-icon-big colordark" style="
             margin-top: 70px; font-size:15px;
             " >More Services
          <i class="rt-icon2-arrow-right-outline"></i>
          </a>
       </div>
   </div>
</section>
<section class="ls ms page_portfolio section_padding_top_40 section_padding_bottom_40 columns_padding_0" style="background:#fff;">
   <div class="container-fluid" style="padding-top:120px;">
      <div class="row">
         <div class="col-sm-12 to_animate">
            <h3  class="heading" style="text-align:center;">Buy hardware, tools & accessories</h3>
            <div class="fw-heading">
               <h5 style="text-align:center; ">Wide range. Top brands. Good price.</h5>
            </div>
            <!-- <p style="text-align:center;    ">Wide range. Top brands. Good price</p> -->
            <div class="isotope_container isotope row masonry-layout" data-filters=".isotope_filters">
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 plumbing">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{ asset ('images/gallery/new-01.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/new-01.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Upholstery</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 plumbing">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{ asset ('images/gallery/new-02.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/02.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Auto Parts</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 plumbing roofing other">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{ asset ('images/gallery/new-03.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/03.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Hardware & Tools</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 roofing renovating other">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{asset ('images/gallery/new-04.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/08.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4> Sanitary Fitting</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 roofing">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{asset ('images/gallery/new-05.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/06.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Paints</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 carpenting renovating">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{asset ('images/gallery/new-06.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{ asset('images/gallery/04.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Beauty Products</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 carpenting">
                  <div class="vertical-item gallery-title-item content-absolute other">
                     <div class="item-media">
                        <img src="{{ asset('images/gallery/new-07.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{ asset('images/gallery/05.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Wood Material</h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="isotope-item col-lg-3 col-md-4 col-sm-6 renovating other">
                  <div class="vertical-item gallery-title-item content-absolute">
                     <div class="item-media">
                        <img src="{{ asset('images/gallery/new-08.png')}}" alt="">
                        <div class="media-links">
                           <div class="links-wrap">
                              <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{ asset('images/gallery/07.png')}}"></a>-->
                              <a class="p-link" title="" href="/shop-right"></a>
                              <h4>Electrical Items    </h4>
                              <p>Shop Now</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               </a>
            </div>
            <div class="text-center" style="padding-top: 0px;">
           
              <a href="/shop-right" class="theme_button round-icon round-icon-big colordark" style="
                 margin-top: 70px; font-size:15px;  border: solid 1.5px; top:1px;
                 " >Shop Now
              <i class="rt-icon2-arrow-right-outline"></i>
              </a>
           </div>
            <!-- eof .isotope_container.row -->
         </div>
      </div>
   </div>
</section>

<section id="experience-main" class="ls ms section_padding_top_100 section_padding_bottom_80 hello_section">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-lg-8 to_animate">
            <div>
               <div class="title-block">
                  <span class="prefix">#1</span>
                  <h3 class="sub">Kamay</h3>
                  <h1 class="title">Experience</h1>
               </div>
            </div>
            <div class="fw-heading">
               <h5>Kamay is a social enterprise powered by robust technology platform to bridge the gap 
                  between people who seek blue-collar services & those who provide them.
               </h5>
            </div>
            <div class="fw-divider-line">
               <hr>
            </div>
            <div class="fw-iconbox clearfix ib-type1">
               <div class="fw-iconbox-image">
                  <i class="fa fa-file-text-o"></i>
               </div>
               <div class="fw-iconbox-aside">
                  <div class="fw-iconbox-title">
                     <h3>CERTIFICATION</h3>
                  </div>
                  <div class="fw-iconbox-text">
                     <p>Kamay (workers) are certified & skilled.</p>
                  </div>
               </div>
            </div>
            <div class="fw-iconbox clearfix ib-type1">
               <div class="fw-iconbox-image">
                  <i class="fa fa-clock-o"></i>
               </div>
               <div class="fw-iconbox-aside">
                  <div class="fw-iconbox-title">
                     <h3>24/7 Available</h3>
                  </div>
                  <div class="fw-iconbox-text">
                     <p>Book your service for any time of the day.</p>
                  </div>
               </div>
            </div>
            <div class="fw-iconbox clearfix ib-type1">
               <div class="fw-iconbox-image">
                  <i class="fa fa-thumbs-o-up"></i>
               </div>
               <div class="fw-iconbox-aside">
                  <div class="fw-iconbox-title">
                     <h3>Fair Prices</h3>
                  </div>
                  <div class="fw-iconbox-text">
                     <p>We aren’t the cheapest but we won’t ruin your wallet either.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="ls ls ms whychooseus">
   <div class="container-fluid">
      <div class="section_padding_top_100 section_padding_bottom_85 row">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 to_animate">
                  <div class="col-sm-8 row">
                     <div class="title-block">
                        <span class="prefix">4</span>
                        <h3 class="sub" style="color:white;">Reasons</h3>
                        <h1 class="title" style="color:white;">Why to use kamay 
                        </h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="whychooseus-desc">
         <div class="container padding-top-30">
            <div class="row">
               <div class="col-xs-7 col-md-6 col-lg-6 to_animate">
                  <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                     <div class="fw-iconbox-image">
                        <i class="rt rt-icon2-chart-line-outline"></i>
                     </div>
                     <div class="fw-iconbox-aside">
                        <div class="fw-iconbox-title">
                           <h3 style="color:#f8d808;">Non-stop sahulat with Kamay</h3>
                        </div>
                        <div class="fw-iconbox-text">
                           <p>Kamay helps you find all types of workers whenever your home or business needs it. Be it electrician, carpenter, mechanic, cleaner or thousand others. What more, you can buy hardware and material like paints, upholstery, auto parts and a lot more.</p>
                        </div>
                     </div>
                  </div>
                  <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                     <div class="fw-iconbox-image">
                        <i class="rt rt-icon2-speech-bubble"></i>
                     </div>
                     <div class="fw-iconbox-aside">
                        <div class="fw-iconbox-title">
                           <h3 style="color:#f8d808;">Instantly book</h3>
                        </div>
                        <div class="fw-iconbox-text">
                           <p>No more calls or hassle – choose a date and time, and we will take care of the rest.
                              Whether you are looking for one or 100 workers, or if it’s last-minute or weeks away, we can help you find the right people for your work.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                     <div class="fw-iconbox-image">
                        <i class="rt rt-icon2-watch"></i>
                     </div>
                     <div class="fw-iconbox-aside">
                        <div class="fw-iconbox-title">
                           <h3 style="color:#f8d808;">Pre-screened, professional Kamay</h3>
                        </div>
                        <div class="fw-iconbox-text">
                           <p>Workers using the Kamay platform are experienced, friendly and background checked. 
                              We will automatically match you with relevant worker. Our professionals have an average rating of 4 out of 5 stars.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                     <div class="fw-iconbox-image">
                        <i class="rt rt-icon2-watch"></i>
                     </div>
                     <div class="fw-iconbox-aside">
                        <div class="fw-iconbox-title">
                           <h3 style="color:#f8d808;">On-the-go Information</h3>
                        </div>
                        <div class="fw-iconbox-text">
                           <p> See cost estimates before you confirm. Access Kamay worker profiles and ratings with a click. 
                              Check the progress of your booking (for eg: when en-route), and track hours from start to finish on our mobile app.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xs-7 col-md-6 col-lg-6 to_animate">
                  <img src="{{ asset('images/gallery/4_reasons_745x914-11.png')}}" alt="" style="margin-left: 10%;margin-top: 4%;">
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="ds call-to-action-alt parallax section_padding_top_100 section_padding_bottom_100">
   <div class="container-fluid">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <div class="fw-call-to-action">
                  <div>
                     <h2>Get the Kamay App</h2>
                     <br>
                     <p>Book. Track. Pay. Rate</p>
                  </div>
                  <div class="fw-action-btn">
                     <a href="#" class="fw-btn fw-btn-1" target="_self">
                     <span>
                     <i class="fa fa-mobile"></i>
                     </span>
                     </a>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 text-center" >
                     <img src="images/download button/get it button-06.png" alt="">
                  </div>
                  <div class="col-xs-6 text-center" >
                     <img src="images/download button/get it button-07.png" alt="">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="ls ms section_padding_top_65 section_padding_bottom_100 recent-posts-carousel">
   <div class="container">
      <div class="row">
      </div>
      <!-- .row -->
      <div class="ls">
         <div class="row">
            <div class="col-sm-12 text-center banner-bottom to_animate">
               <i class="rt-icon2-arrow-left-outline"></i><a href="/services">
               <img href="/services"src="{{ asset('images/banner.png')}}" alt="">
               <i class="rt-icon2-arrow-right-outline"></i></a>
            </div>
         </div>
      </div>
   </div>
</section>




@endsection()


