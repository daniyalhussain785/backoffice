
  @extends('layouts.front')
    @section('title', 'Kama')
  @section('content')


  	<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
   <div id="box_wrapper">
      <!-- template sections -->

      <section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40" style="background: url('images/kama_slider.png'); height:600px;">
         <div class="main-slider__scroll m-uppercase" id="main-slider__scroll">
            <!--<a id="scroll-down">-->
            <!--	<i class="fa fa-arrow-down"></i>-->
            <!--	<span>scroll</span>-->
            <!--</>-->
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 0;"></span>scroll</a>
         </div>
         <div class="flexslider" >
            
                  <!-- <img src="{{ asset('images/kama_slider.png')}}" alt=""> -->
                  <div class="container">
                     <div class="row">
                        <div class="slide_description col-md-5 col-sm-12">
                           <br/>
                           <div class="slide_description_wrapper">
                              <div class="slide_description">
                                 <div class="intro-layer animated slideExpandUp" data-animation="slideExpandUp" style="visibility: hidden;" data-delay="200">
                                    <div class="col-md-12 " >
                                       <div class="teaser text-center with_background" style="padding: 10px 55px;">
                                          <div class="row">
                                              <h3 style="margin-bottom:30px">Sign up as kama</h3>
                                             <div class="col-sm-12 to_animate">
                                                <form class="contact-form width-6-col columns_padding_5"  method="post" action="/">
                                                   <div>
                                                      
                                                      <p class="contact-form-name">
                                                         <input style="text-align:left;" style="font-size:18px;" type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control lf" placeholder="Full Name">
                                                      </p>
                                                      
                                                      <p class="contact-form-email">
                                                         <select style="font-size:17px;" id="area" placeholder="">
                                                            <option class="area" >Select City</option>
                                                            <option>Karachi</option>
                                                            <option>Lahore</option>
                                                            <option>Islamabad</option>
                                                            <option>Peshawar</option>
                                                         </select>
                                                      </p>
                                                      <p class="contact-form-email">
                                                         <select style="font-size:17px;" id="area" placeholder="Select Area">
                                                            <option>Select Area</option>
                                                            <option>Malir Cantt</option>
                                                            <option>F.B Area</option>
                                                            <option>Saddar</option>
                                                            <option>SITE Area</option>
                                                         </select>
                                                      </p>
                                                      <p class="contact-form-email">
                                                         <select style="font-size:17px;" id="area" placeholder="Select Area">
                                                            <option>Select Services</option>
                                                            <option>Electrician</option>
                                                            <option>Plumber</option>
                                                            <option>Painter</option>
                                                            <option>Cook</option>
                                                         </select>
                                                      </p>
                                                      <!--<p class="contact-form-email">-->
                                                      <!-- <select id="area" placeholder="Select Area">-->
                                                      <!--    <option>Select Sub-Services</option>-->
                                                      <!--    <option>Electrician</option>-->
                                                      <!--    <option>Plumber</option>-->
                                                      <!--    <option>Painter</option>-->
                                                      <!--  <option>Cook</option>-->
                                                      <!--</select>-->
                                                      <!--</p>-->
                                                      <p class="">
                                                         <input style="text-align:left;" type="number" aria-required="true" size="30" value="" name="phone_no" id="email" class="form-control lf" placeholder="Phone Number">
                                                      </p>
                                                      <p class="contact-form-submit text-center topmargin_30">
                                                         <button type="submit" 
                                                            id="my_form" name="contact_submit" class="theme_button round-icon round-icon-big colordark" style="font-size:15px;">Register Now
                                                         <i class="rt-icon2-arrow-right-outline"></i>
                                                         </button>
                                                      </p>
                                                      <div id="myModal" class="modal">
                                                         Modal content 
                                                         <div class="modal-content">
                                                            <span class="close">&times;</span>
                                                            <p>Enter Code</p>
                                                            <input type="text" aria-required="true" size="30" value="" name="otp_code" class="form-control lf" placeholder="Data Inserted Successfully">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- eof .slide_description -->
                           </div>
                           <!-- eof .slide_description_wrapper -->
                        </div>
                        <!-- eof .col-* -->
                <!--        <div class="col-md-7 col-sm-12 to_animate animated slideRight" data-animation="slideRight" data-delay="50">-->
                <!--    <h3>.fadeInRight</h3>-->
                <!--    <p>Just add class <strong>"to_animate"</strong>, attributes <strong>"data-animation"</strong> with this class, and <strong>"data-delay"</strong> with number in milliseconds to any HTML element and you're done.</p>-->

                <!--</div>-->
                        
                        <!--<div class="col-md-7 col-sm-12 to_animate animated slideRight" data-animation="slideRight" data-delay="1000">-->
                            
                        <!--   <br/><br><br><br><br><br><br>-->
                        <!--   <h3 style="color:white;" class="text-uppercase">Earn income, respect-->
                        <!--   <br><br> & skills with Kamay</h3>-->
                        <!--</div>-->
                        <div class="col-md-7 col-sm-12 to_animate animated slideRight" data-animation="slideRight" data-delay="150" style="padding: 15% 6%;">
                           <h2 style="color:white;font-size: 40px;"> Earn income, respect <br></h2>
                           <h2 style="color:white;font-size: 40px;" > & skills with Kamay</h2>
                        </div>
                     </div>
                     <!-- eof .row -->
                  </div>
                  <!-- eof .container -->
               
         </div>
         <!-- eof flexslider -->
      </section>
      <section class="ls parallax section_padding_90 dark_testimonials_section">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <h3 class="text-center">Why become a Kama?</h3>
               </div>
               <div class="container-fluid super-ds">
                  <div class="row">
                     <div class="col-sm-4 to_animate">
                        <div class="teaser text-center padding-top-70 padding-bottom-70">
                           <div class="teaser_icon highlight size_normal">
                              <i class="fa fa-dollar"></i>
                           </div>
                           <h3 class="m-uppercase">Increase your income</h3>
                           <p>100s of nearby jobs available. Earn in addition to your existing work.</p>
                           <div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 to_animate left-right-borders padding-top-70 padding-bottom-70">
                        <div class="teaser text-center">
                           <div class="teaser_icon highlight size_normal">
                              <i class="rt-icon2-pencil3"></i>
                           </div>
                           <h3 class="m-uppercase">Improve your skills</h3>
                           <p>Grow professionally. Kamay invests on your training and certification</p>
                           <div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 to_animate padding-top-70 padding-bottom-70">
                        <div class="teaser text-center">
                           <div class="teaser_icon highlight size_normal">
                              <i class="rt-icon2-medal"></i>
                           </div>
                           <h3 class="m-uppercase">Earn prestige</h3>
                           <p>Work in a structure which provides you safety, security and respect.</p>
                           <div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .col- -->
            </div>
            <!-- .row -->
         </div>
      </section>
      <section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <div class="text-center" style="padding-bottom: 6%;">
                     <h1>Requirements</h1>
                     <div class="fw-heading">
                        <h5>Prerequisite for applying as a Kama (worker) </h5>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-4 to_animate">
                        <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                           <div class="fw-iconbox-image">
                              <i class="rt rt-icon2-speech-bubble"></i>
                           </div>
                           <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Age</h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>You should be 18 years and above.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 to_animate">
                        <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                           <div class="fw-iconbox-image">
                              <i class="rt rt-icon2-speech-bubble"></i>
                           </div>
                           <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Valid NIC</h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>You should be a Pakistani national with valid National Identity Card (NIC).</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 to_animate">
                        <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                           <div class="fw-iconbox-image">
                              <i class="rt rt-icon2-speech-bubble"></i>
                           </div>
                           <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>No criminal record</h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>You should not have any criminal record.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-4 to_animate">
                        <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                           <div class="fw-iconbox-image">
                              <i class="rt rt-icon2-speech-bubble"></i>
                           </div>
                           <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Relevant skills</h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>You should have experience and interest in skills being offered as service.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 to_animate">
                        <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                           <div class="fw-iconbox-image">
                              <i class="rt rt-icon2-speech-bubble"></i>
                           </div>
                           <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Smartphone </h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>It is preferred you have a smartphone or know how to use it.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 to_animate">
                        <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                           <div class="fw-iconbox-image">
                              <i class="rt rt-icon2-speech-bubble"></i>
                           </div>
                           <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Screening</h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>You will go through multiple screening and documentation steps as per standard Kamay policy </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>
   </section>
   <section id="experience-main" class=" section_padding_top_100 section_padding_bottom_80 hello_section">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-lg-7 to_animate">
               <div>
                  <div class="title-block">
                     <h1 class="tit	le">How to get started</h1>
                  </div>
               </div>
               <div class="fw-heading">
                  <h5>Kamay believes in adding honest, hardworking and motivated pool of workers. For this, we take our applicants through a number of steps.</h5>
               </div>
               <div class="fw-divider-line">
                  <hr>
               </div>
               <div class="fw-iconbox clearfix ib-type1">
                  <div class="fw-iconbox-image">
                     <i class="fa fa-file-text-o"></i>
                  </div>
                  <div class="fw-iconbox-aside">
                     <div class="fw-iconbox-title">
                        <h3>Register</h3>
                     </div>
                     <div class="fw-iconbox-text">
                        <p>Create an account as a Kama on our website or mobile app.</p>
                     </div>
                  </div>
               </div>
               <div class="fw-iconbox clearfix ib-type1">
                  <div class="fw-iconbox-image">
                     <i class="fa fa-clock-o"></i>
                  </div>
                  <div class="fw-iconbox-aside">
                     <div class="fw-iconbox-title">
                        <h3>	
                           Qualify 
                        </h3>
                     </div>
                     <div class="fw-iconbox-text">
                        <p>Successfully complete steps like interview, background check, documentation and training.</p>
                     </div>
                  </div>
               </div>
               <div class="fw-iconbox clearfix ib-type1">
                  <div class="fw-iconbox-image">
                     <i class="fa fa-thumbs-o-up"></i>
                  </div>
                  <div class="fw-iconbox-aside">
                     <div class="fw-iconbox-title">
                        <h3>Start work</h3>
                     </div>
                     <div class="fw-iconbox-text">
                        <p>If you qualify, your Kama account will be activated, and you can find instant work. </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="text-center">
                  <h2>Our Kama Safety is Our Priority</h2>
                  <div class="fw-heading">
                     <h5>We care about our Kama safety.</h5>
                  </div>
               </div>
               <div class="row">
                  <div class="container-fluid super-ds">
                     <div class="row">
                        <div class="col-sm-4 to_animate">
                           <div class="teaser text-center padding-top-70 padding-bottom-70">
                              <div class="teaser_icon highlight size_normal">
                                 <i class="rt-icon2-user"></i>
                              </div>
                              <h3 class="m-uppercase">Supervisor</h3>
                              <p>Every kama gets assigned a supervisor who mentors, manages and helps in troubleshooting.</p>
                              <div>
                                 <button  class="btn">
                                 <span>
                                 <i class="rt-icon2-check"></i>
                                 </span>
                                 </button>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                           <div class="teaser text-center padding-top-70 padding-bottom-70">
                              <div class="teaser_icon highlight size_normal">
                                 <i class="rt-icon2-add"></i>
                              </div>
                              <h3 class="m-uppercase">Panic button </h3>
                              <p>Kamay (workers) can alert our helpline for action in case of any emergency.</p>
                              <div>
                                 <button  class="btn">
                                 <span>
                                 <i class="rt-icon2-check"></i>
                                 </span>
                                 </button>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                           <div class="teaser text-center padding-top-70 padding-bottom-70">
                              <div class="teaser_icon highlight size_normal">
                                 <i class="rt-icon2-location"></i>
                              </div>
                              <h3 class="m-uppercase">GPS Tracking</h3>
                              <p>Our Kama location is tracked for their and customers’ safety.</p>
                              <div>
                                 <button  class="btn">
                                 <span>
                                 <i class="rt-icon2-check"></i>
                                 </span>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 to_animate">
                     <div class="teaser text-center padding-top-70 padding-bottom-70">
                        <div class="teaser_icon highlight size_normal">
                           <i class="rt-icon2-ambulance"></i>
                        </div>
                        <h3 class="m-uppercase">Insurance </h3>
                        <p>All our Kamay have travel insurance to protect them while commuting.</p>
                        <div>
                           <button  class="btn">
                           <span>
                           <i class="rt-icon2-check"></i>
                           </span>
                           </button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 to_animate">
                     <div class="teaser text-center padding-top-70 padding-bottom-70">
                        <div class="teaser_icon highlight size_normal">
                           <i class="rt-icon2-star"></i>
                        </div>
                        <h3 class="m-uppercase">Customer rating  </h3>
                        <p>Kama also rates a customer. This helps us in managing their experience.</p>
                        <div>
                           <button  class="btn">
                           <span>
                           <i class="rt-icon2-check"></i>
                           </span>
                           </button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 to_animate">
                     <div class="teaser text-center padding-top-70 padding-bottom-70">
                        <div class="teaser_icon highlight size_normal">
                           <i class="rt-icon2-phone"></i>
                        </div>
                        <h3 class="m-uppercase">Helpline </h3>
                        <p>Kamay Helpline provides a 24/7 access to workers and customers for any kind of assistance.</p>
                        <div>
                           <button  class="btn">
                           <span>
                           <i class="rt-icon2-check"></i>
                           </span>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</div>
</div>
</div>
</section>
<section class="ds call-to-action-alt parallax section_padding_top_100 section_padding_bottom_100">
   <div class="container-fluid">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <div class="fw-call-to-action">
                  <div>
                     <h2>Want to become a Kama today?</h2>
                     <br>										
                     <p>Register Now</p>
                  </div>
                  <div class="fw-action-btn">
                     <a href="#" class="fw-btn fw-btn-1" target="_self">
                     <span>
                     <i class="fa fa-mobile"></i>
                     </span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


  @endsection()