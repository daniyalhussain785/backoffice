
  @extends('layouts.front')
    @section('title', 'Delails')
  @section('content')

   <!-- wrappers for visual page editor and boxed version of template -->
   <div id="canvas">
      <div id="box_wrapper">

        <section class="ds call-to-action-alt parallax section_padding_top_100 section_padding_bottom_100">
				<div class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								

								<div class="row">
									<div class="col-xs-6 text-center" >
                                     <img src="images/download button/get it button-06.png" alt="">
									</div>

									<div class="col-xs-6 text-center" >
										<img src="images/download button/get it button-07.png" alt="">
									</div>
							</div>
							</div>
						</div>
					</div>
				</div>
			
           
			</section>


	


			<section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">

								<div class="col-sm-12">
									<div class="row vertical-tabs">
										<div class="col-lg-3 col-md-5 col-sm-12">

											<!-- Nav tabs -->
											<ul class="nav m-uppercase" role="tablist">
												<li class="active">
													<a href="#vertical-tab1" role="tab" data-toggle="tab">
														<i class="rt-icon2-waves-outline"></i>Plumbing Services
													</a>
													<li>
														<a href="#vertical-tab2" role="tab" data-toggle="tab">
															<i class="rt-icon2-home-outline"></i>Doors & Windows
														</a>
													</li>
													<li>
														<a href="#vertical-tab3" role="tab" data-toggle="tab">
															<i class="rt-icon2-plug"></i>Electrician Services
														</a>
													</li>
													<li>
														<a href="#vertical-tab4" role="tab" data-toggle="tab">
															<i class="rt-icon2-brush"></i>Tiling & Painting
														</a>
													</li>
													<li>
														<a href="#vertical-tab5" role="tab" data-toggle="tab">
															<i class="rt-icon2-puzzle-outline"></i>Wood Flooring
														</a>
													</li>
													<li>
														<a href="#vertical-tab6" role="tab" data-toggle="tab">
															<i class="rt-icon2-spanner-outline"></i>Maintenance
														</a>
													</li>
													<li>
														<a href="#vertical-tab7" role="tab" data-toggle="tab">
															<i class="rt-icon2-weather-sunny"></i>Central Heating
														</a>
													</li>
													<li>
														<a href="#vertical-tab8" role="tab" data-toggle="tab">
															<i class="rt-icon2-lightbulb3"></i>Handyman Services
														</a>
													</li>
											</ul>
										</div>
										<div class="col-lg-9 col-md-7 col-sm-12">

											<!-- Tab panes -->
											<div class="tab-content no-border">

												<div class="tab-pane fade in active" id="vertical-tab1">
													<h2>Plumbing Related Services</h2>
													<div class="row">
														<div class="col-sm-6">
															<strong>Plumbing Installation</strong>
															<p>Lorem ipsum dolor sit amet, consetetur sadipscing</p>
														</div>
														<div class="col-sm-6">
															<strong>Plumbing Inspections</strong>
															<p>Fabore et dolore magna aliquyam erat diam</p>
														</div>
														<div class="col-sm-6">
															<strong>Plumbing Maintenance</strong>
															<p>Elitr sed diam nonumy eirmod tempor invidunt ut</p>
														</div>
														<div class="col-sm-6">
															<strong>Plumbing Maintenance</strong>
															<p>Elitr sed diam nonumy eirmod tempor invidunt ut</p>
														</div>
														<div class="col-sm-6">
															<strong>Plumbing Installation</strong>
															<p>Fabore et dolore magna aliquyam erat diam</p>
														</div>
														<div class="col-sm-6">
															<strong>Plumbing Installation</strong>
															<p>Lorem ipsum dolor sit amet, consetetur sadipscing</p>
														</div>
													</div>

													<div class="padding-top-30">
														<div class="panel-group accordion-big" id="accordion1">
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion1" href="#collapse1" class="collapsed">
																			At vero eos et accusam?
																		</a>
																	</h4>
																</div>
																<div id="collapse1" class="panel-collapse collapse in">
																	<div class="panel-body">
																		At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet lorem ipsum dolor sit amet, consetetur sadipscing.
																	</div>
																</div>
															</div>
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion1" href="#collapse2" class="collapsed">
																			Justo duo dolores et ea rebum clita kasd?
																		</a>
																	</h4>
																</div>
																<div id="collapse2" class="panel-collapse collapse">
																	<div class="panel-body">
																		At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet lorem ipsum dolor sit amet, consetetur sadipscing.
																	</div>
																</div>
															</div>
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a data-toggle="collapse" data-parent="#accordion1" href="#collapse3" class="collapsed">
																			Gubergren, no sea takimata sanctus est ipsum dolor?
																		</a>
																	</h4>
																</div>
																<div id="collapse3" class="panel-collapse collapse">
																	<div class="panel-body">
																		At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet lorem ipsum dolor sit amet, consetetur sadipscing.
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- tab end -->

												<div class="tab-pane fade" id="vertical-tab2">
													<p>
														<i class="rt-icon2-compass"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit voluptate, quas fugit facere possimus facilis odio delectus laborum id nobis expedita vitae molestiae a. Magnam aliquid architecto magni, quos omnis.</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, enim saepe libero iure tenetur optio nisi aliquam molestias ratione magnam ab ut quod possimus hic suscipit doloremque, deleniti ipsa quia!</p>
												</div>

												<div class="tab-pane fade" id="vertical-tab3">
													<p>
														<i class="rt-icon2-laptop"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque repellat, reiciendis sint officia quia iure nam! Dicta omnis ex ipsa fugiat maiores, vero expedita facilis, suscipit quam obcaecati veniam voluptate.</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis est, dolores, ex ducimus cumque iusto ipsam odit voluptatum autem error impedit obcaecati quisquam molestiae, optio porro inventore nostrum deleniti cupiditate.</p>
												</div>

												<div class="tab-pane fade" id="vertical-tab4">
													<p>
														<i class="fa fa-trophy"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium impedit asperiores illum nulla sint itaque laborum perferendis deleniti quo cumque, quisquam repudiandae molestias sunt ea delectus porro odio recusandae!</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus omnis quod eligendi mollitia vel optio neque id, assumenda! Quae at quisquam eum, dolorum ullam, maxime nesciunt ex modi minus illum!</p>
												</div>

												<div class="tab-pane fade" id="vertical-tab5">
													<p>
														<i class="rt-icon2-compass"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit voluptate, quas fugit facere possimus facilis odio delectus laborum id nobis expedita vitae molestiae a. Magnam aliquid architecto magni, quos omnis.</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, enim saepe libero iure tenetur optio nisi aliquam molestias ratione magnam ab ut quod possimus hic suscipit doloremque, deleniti ipsa quia!</p>
												</div>

												<div class="tab-pane fade" id="vertical-tab6">
													<p>
														<i class="rt-icon2-laptop"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque repellat, reiciendis sint officia quia iure nam! Dicta omnis ex ipsa fugiat maiores, vero expedita facilis, suscipit quam obcaecati veniam voluptate.</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis est, dolores, ex ducimus cumque iusto ipsam odit voluptatum autem error impedit obcaecati quisquam molestiae, optio porro inventore nostrum deleniti cupiditate.</p>
												</div>

												<div class="tab-pane fade" id="vertical-tab7">
													<p>
														<i class="fa fa-trophy"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium impedit asperiores illum nulla sint itaque laborum perferendis deleniti quo cumque, quisquam repudiandae molestias sunt ea delectus porro odio recusandae!</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus omnis quod eligendi mollitia vel optio neque id, assumenda! Quae at quisquam eum, dolorum ullam, maxime nesciunt ex modi minus illum!</p>
												</div>

												<div class="tab-pane fade" id="vertical-tab8">
													<p>
														<i class="fa fa-trophy"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium impedit asperiores illum nulla sint itaque laborum perferendis deleniti quo cumque, quisquam repudiandae molestias sunt ea delectus porro odio recusandae!</p>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus omnis quod eligendi mollitia vel optio neque id, assumenda! Quae at quisquam eum, dolorum ullam, maxime nesciunt ex modi minus illum!</p>
												</div>
											</div>

										</div>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>



  @endsection()