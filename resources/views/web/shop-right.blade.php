
  @extends('layouts.front')
    @section('title', 'Shop')
  @section('content')

   <!-- wrappers for visual page editor and boxed version of template -->
   <div id="canvas">
      <div id="box_wrapper">

         <!--<section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40" style="background: url('images/parallax/breadcrumbs.jpg'); padding: 7%;">-->
        <section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h1 class="">Products</h1>
                            
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="ls ms section_padding_top_100 section_padding_bottom_75">
                <div class="container">
                    <div class="row">
            
                        <div class="col-sm-7 col-md-9 col-lg-9">
                        
                            <div class="store-sorting-results pull-left"><p>Showing 1–9 of 60 results</p></div>
                            <div class="store-sorting-right clearfix pull-right">
                                
                                <form class="form-inline">
                                    <div class="arr-for-select">
                                        <i class="rt-icon2-chevron-down"></i>
                                        <select>
                                            <option>Your Name</option>
                                            <option>Your Name 2</option>
                                            <option>Your Name 3</option>
                                        </select>
                                    </div>
                                </form>
            
                            </div>
            
            
                            <div class="columns-3">
                                
                                <ul id="products" class="products list-unstyled grid-view">
                                                            <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/01.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/02.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/03.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/04.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/05.jpg" alt="">
                                                        </a>
                                                        <img class="shop-sale-lable" src="images/sale.png" alt="">
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/06.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/07.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/08.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
                                    <li class="product type-product">
                                        <div class="side-item">
                                            <div class="row">
            
                                                <div class="col-md-6">
                                                    
                                                    <div class="item-media">
                                                        <a href="product-right.html">
                                                            <img src="images/shop/09.jpg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="category-shop-item">
                                                        <span class="amount">Category</span><i class="rt-icon2-heart-outline"></i>
                                                    </div>
                                                    <h3>
                                                        <a href="product-right.html">At vero eos accusam</a>
                                                    </h3>
                                                    <span class="price">
                                                        <span class="amount">$299.00</span>
                                                    </span>
                                                    
                                                </div>
            
                                                <div class="col-md-6">
                                                    <div class="item-content">
            
                                                    </div>
                                                </div>
            
                                            </div>
                                        </div>
                                    </li>
            
            
                                    
                                </ul>
                            
                            </div> <!-- eof .columns-* -->
            
            
                            
                                <div class="col-sm-12 text-left">
                                
            
                                							<ul class="pagination">
            							<li class="active"><a href="#">1</a></li>
            							<li><a href="#">2</a></li>
            							<li><a href="#">3</a></li>
            							<li><a href="#">4</a></li>
            							<li><a href="#">5</a></li>
            						</ul>
                                	
                                </div>
                            
            
                            
                           
                        </div> <!--eof .col-sm-8 (main content)-->
            
            
                        <!-- sidebar -->
                        <aside class="col-sm-5 col-md-3 col-lg-3">
                            
            
            
                                    <div>
                        <div class="widget widget_price_filter">
                        
                        <h3 class="widget-title">Price Filter</h3>
                        <!-- price slider -->
                        <form method="get" action="/" class="form-inline">
                             
                            <div class="slider-range-price"></div>
            
                            <div class="form-group">
                                <label class="grey" for="slider_price_min">From: $</label>
                                <input type="text" class="slider_price_min form-control text-center" id="slider_price_min" readonly>
                            </div>
                              
                            <div class="form-group">
                                <label class="grey" for="slider_price_max"> - $</label>
                                <input type="text" class="slider_price_max form-control text-center" id="slider_price_max" readonly>
                            </div>
            
                            <div class="text-right pull-right">
                                <button type="submit" class="theme_button small_button color1">Filter</button>
                            </div>
                        </form>
                    </div>
                    </div>
            
                    <div class="with_margin_top">
                        <div class="widget widget_search">
            		    <h3 class="widget-title">Search</h3>
            		    <form method="get" class="searchform form-inline" action="/">
            		        <div class="form-group with_border_2">
            		            <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search...">
            		        </div>
            		        <button type="submit" class="theme_button">Search</button>
            		    </form>
            		</div>
                    </div>
                    <div class="with_margin_top">
                        <div class="widget widget_categories_shop">
                        <h3 class="widget-title">Categories</h3>
                        <ul class="darklinks">
                            <li>
                                <a href="#" class="url">Lorem ipsum dolor</a><span>(12)</span>
                            </li>
                            <li>
                                <a href="#" class="url">Consetetur sadipscing</a><span>(3)</span>
                            </li>
                            <li>
                                <a href="#" class="url">Nonumy eirmod</a><span>(8)</span>
                            </li>
                            <li>
                                <a href="#" class="url">Invidunt ut labore</a><span>(13)</span>
                            </li>
                            <li>
                                <a href="#" class="url">Dolore magna</a><span>(34)</span>
                            </li>
                            <li>
                                <a href="#" class="url">Aliquyam erat sed</a><span>(25)</span>
                            </li>
                        </ul>
                    </div>
                    </div>
            
                    <div class="with_margin_top">
                        <div class="widget widget_products widget_popular_entries">
            
                        <h3 class="widget-title">Viewed</h3>
                        <ul class="media-list viewed-shop">
                            <li class="media">
                                <a class="media-left" href="product-right.html">
                                    <img class="media-object" src="images/shop/01.jpg" alt="">
                                </a>
                                <div class="media-body">
                                    <span class="viewed-category">
                                        
                                            <span class="amount">Category</span>
                                        
                                    </span>
                                    <h4>
                                        <a href="product-right.html">Lorem ipsm dolor sit amet</a>
                                    </h4>
                                    <span class="price">
                                        
                                            <span class="amount">$299.00</span>
                                        
                                    </span>
                                </div>
                            </li>
                            
                            <li class="media">
                                <a class="media-left" href="product-right.html">
                                    <img class="media-object" src="images/shop/02.jpg" alt="">
                                </a>
                                <div class="media-body">
                                    <span class="viewed-category">
                                        
                                            <span class="amount">Category</span>
                                        
                                    </span>
                                    <h4>
                                        <a href="product-right.html">Lorem ipsm dolor sit amet</a>
                                    </h4>
                                    <span class="price">
                                        
                                            <span class="amount">$299.00</span>
                                        
                                    </span>
                                </div>
                            </li>
            
                            <li class="media">
                                <a class="media-left" href="product-right.html">
                                    <img class="media-object" src="images/shop/03.jpg" alt="">
                                </a>
                                <div class="media-body">
                                    <span class="viewed-category">
                                        
                                            <span class="amount">Category</span>
                                        
                                    </span>
                                    <h4>
                                        <a href="product-right.html">Lorem ipsm dolor sit amet</a>
                                    </h4>
                                    <span class="price">
                                        
                                            <span class="amount">$299.00</span>
                                        
                                    </span>
                                </div>
                            </li>
                            
                        </ul>
                    </div>
                    </div>
                            
                            
                    
                        </aside> <!-- eof aside sidebar -->
            
            
                    </div>
                </div>
            </section>



  @endsection()