
  @extends('layouts.front')
    @section('title', 'Services')
  @section('content')

   <!-- wrappers for visual page editor and boxed version of template -->
   <div id="canvas">
      <div id="box_wrapper">

         <!--</section>-->
         <section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center" style="padding:4%;">
                            <!--<h1 class="">Services</h1>-->
                            
                        </div>
                    </div>
                </div>
            </section>

         

      
            
           
        <section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <h1>Our Services</h1>
                            <div class="fw-heading">
                            <h5>We Are Specialists In These Areas</h5>
                            </div>
                        </div>
                        <div class="widget widget_search col-sm-12" style="padding:5%">
                			<form method="get" class="searchform form-inline row align-items-center" action="/" style="width: 30%; margin: auto;">
                				<div class="form-group">
                					<input type="text" value="" name="search" class="form-control" placeholder="Search keyword">
                				</div>
                				<button type="submit" class="theme_button">Search</button>
                			</form>
                		</div>
                        
                        <div class="row">
        
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-spanner-outline"></i>
                                    </div>
                                    <h3 class="m-uppercase">Plumber</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-home-outline"></i>
                                    </div>
                                    <h3 class="m-uppercase">Carpenter</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-paintbrush"></i>
                                    </div>
                                    <h3 class="m-uppercase">Cleaner</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                             <a href="detail">
                             <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-paintbrush"></i>
                                    </div>
                                    <h3 class="m-uppercase">Painter</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-plug"></i>
                                    </div>
                                    <h3 class="m-uppercase">Car mechanic</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-waves-outline"></i>
                                    </div>
                                    <h3 class="m-uppercase">Appliance mechanic</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-scissors-outline"></i>
                                    </div>
                                    <h3 class="m-uppercase">Salon at homes</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            <a href="detail">
                            <div class="col-md-3 col-sm-6">
                                <div class="teaser text-center with_background">
                                    <div class="teaser_icon highlight rounded-icon">
                                        <i class="fa fa-medkit"></i>
                                    </div>
                                    <h3 class="m-uppercase">Nurse attendant</h3>
                                    <!--<p>At vero eos et accusam et justo duo dolores et ea rebum.</p>-->
                                    <button type="submit" class="theme_button">Book Now
										</button>
                                </div>
                            </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6">
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-plug"></i>
                                     </div>
                                     <h3 class="m-uppercase">Nanny </h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>											
                                  </div>
                               </div>
                            </a>
                            
                            <a href="detail">
                               <div class="col-md-3 col-sm-6">
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-plug"></i>
                                     </div>
                                     <h3 class="m-uppercase">Electrician </h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>											
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6">
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-plug"></i>
                                     </div>
                                     <h3 class="m-uppercase">Driver </h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>											
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6">
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-brush"></i>
                                     </div>
                                     <h3 class="m-uppercase">House maid </h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>											
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6" style="">
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-puzzle-outline"></i>
                                     </div>
                                     <h3 class="m-uppercase" >Cook & Chef</h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>											
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6" >
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-spanner-outline"></i>
                                     </div>
                                     <h3 class="m-uppercase">Gardener </h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>										
                                  </div>
                               </div>
                            </a>
                            <a href="/detail">
                               <div class="col-md-3 col-sm-6">
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-weather-sunny"></i>
                                     </div>
                                     <h3 class="m-uppercase">Tailor </h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>										
                                  </div>
                               </div>
                            </a>
                            <a href="/detail">
                               <div class="col-md-3 col-sm-6" >
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-lightbulb3"></i>
                                     </div>
                                     <h3 class="m-uppercase">Waiter</h3>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>	
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6" >
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-lightbulb3"></i>
                                     </div>
                                     <h4  class="m-uppercase">Massage therapist  </h4>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>										
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6" >
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-lightbulb3"></i>
                                     </div>
                                     <h4 class="m-uppercase">Fumigation service  </h4>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>										
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6" >
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-lightbulb3"></i>
                                     </div>
                                     <h4 class="m-uppercase">General labour </h4>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>										
                                  </div>
                               </div>
                            </a>
                            <a href="detail">
                               <div class="col-md-3 col-sm-6" >
                                  <div class="teaser text-center with_background">
                                     <div class="teaser_icon highlight rounded-icon">
                                        <i class="rt-icon2-lightbulb3"></i>
                                     </div>
                                     <h5 class="m-uppercase">Disinfectant service </h5>
                                     <p></p>
                                     <button type="submit" class="theme_button">Book Now
                                     </button>	
                                  </div>
                               </div>
                            </a>
        
                           
        
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>



  @endsection()