@extends('layouts.front')
@section('title', 'Supervisor ')
@section('content')
<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
   <div id="box_wrapper">
      <section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40" style="background: url('images/Kamay web slider 2b 600-01.png');">
         <div class="main-slider__scroll m-uppercase" id="main-slider__scroll">
            <!--<a id="scroll-down">-->
            <!--	<i class="fa fa-arrow-down"></i>-->
            <!--	<span>scroll</span>-->
            <!--</>-->
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 0;"></span>scroll</a>
         </div>
         <div class="flexslider" >
            <!-- <img src="{{ asset('images/kama_slider.png')}}" alt=""> -->
            <div class="container">
                  <div class="row">
                     <div class="col-md-4" >
                        <div class="teaser text-center with_background" style="padding: 20px 25px;margin: 60px 0px;">
                              <div class="row">
                                 <h3 style="margin-bottom:30px">Sign up   as supervisor </h3>
                                 <div class="col-sm-12 to_animate">
                                    <form class="contact-form width-6-col columns_padding_5" method="post" action="/">
                                          <div>
                                             <p class="contact-form-name">
                                                <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control lf" placeholder="Full Name">
                                             </p>
                                             <p class="contact-form-email">
                                                <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control lf" placeholder="Email">
                                             </p>
                                             <p class="contact-form-submit text-center topmargin_30">
                                                <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button round-icon round-icon-big colordark">Register Now
                                                <i class="rt-icon2-arrow-right-outline"></i>
                                                </button>
                                             </p>
                                          </div>
                                    </form>
                                 </div>
                              </div>
                        </div>
                     </div>
                     <div class="col-md-7 col-sm-12 to_animate animated slideRight" data-animation="slideRight" data-delay="150" style="padding: 10% 6%;">
                        <p style="color:white;font-size: 30px;font-weight: 300;"> Build & Manage your <br></p>
                        <h2 class="text-uppercase" style="font-size: 48px;">
                        Kamay team</2>
                     </div>
                  </div>
                  <!-- eof .row -->
            </div>
            <!-- eof .container -->
         </div>
         <!-- eof flexslider -->
      </section>
      <section class="ls parallax section_padding_90 dark_testimonials_section">
         <div class="container">
            <div class="row">
                  <div class="col-sm-12">
                     <h3 class="text-center">Why become Supervisor?</h3>
                  </div>
                  <div class="container-fluid super-ds">
                     <div class="row">
                        <div class="col-sm-4 to_animate">
                              <div class="teaser text-center padding-top-70 padding-bottom-70">
                                 <div class="teaser_icon highlight size_normal">
                                    <i class="rt-icon2-info-large-outline"></i>
                                 </div>
                                 <h3 class="m-uppercase">Be a community leader </h3>
                                 <p>Uplift, interact & manage low-income workers in your area.</p>
                                 <div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate left-right-borders padding-top-70 padding-bottom-70">
                              <div class="teaser text-center">
                                 <div class="teaser_icon highlight size_normal">
                                    <i class="fa fa-dollar"></i>
                                 </div>
                                 <h3 class="m-uppercase">Increase income</h3>
                                 <p>You will get commission for every worker you get registered with Kamay </p>
                                 <div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate padding-top-70 padding-bottom-70">
                              <div class="teaser text-center">
                                 <div class="teaser_icon highlight size_normal">
                                    <i class="rt-icon2-pencil3"></i>
                                 </div>
                                 <h3 class="m-uppercase">Grow professionally </h3>
                                 <p>Get opportunities to grow yourself as a business manager.</p>
                                 <div>
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
                  <!-- .col- -->
            </div>
            <!-- .row -->
         </div>
      </section>
      <section class="ls ms section_padding_top_100 section_padding_bottom_75 columns_padding_25">
         <div class="container">
            <div class="row">
                  <div class="col-sm-12">
                     <div class="text-center" style="padding-bottom: 6%;">
                        <h1>Requirements</h1>
                        <div class="fw-heading">
                              <h5>Kamay Supervisors play a critical role in identifying workers and registering them on the Kamay app.
                                 And later acting as their counsellor and overseeing their performance, grievances and training.  
                              </h5>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-4 to_animate">
                              <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                                 <div class="fw-iconbox-image">
                                    <i class="rt rt-icon2-speech-bubble"></i>
                                 </div>
                                 <div class="fw-iconbox-aside">
                                    <div class="fw-iconbox-title">
                                          <h3>Education </h3>
                                    </div>
                                    <div class="fw-iconbox-text">
                                          <p>Minimum a high school diploma — or its equivalent. College degree is preferred.</p>
                                    </div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                              <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                                 <div class="fw-iconbox-image">
                                    <i class="rt rt-icon2-speech-bubble"></i>
                                 </div>
                                 <div class="fw-iconbox-aside">
                                    <div class="fw-iconbox-title">
                                          <h3>Experience </h3>
                                    </div>
                                    <div class="fw-iconbox-text">
                                          <p>2+ years of experience in team management.</p>
                                    </div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                              <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                                 <div class="fw-iconbox-image">
                                    <i class="rt rt-icon2-mobile"></i>
                                 </div>
                                 <div class="fw-iconbox-aside">
                                    <div class="fw-iconbox-title">
                                          <h3>Smart phone </h3>
                                    </div>
                                    <div class="fw-iconbox-text">
                                          <p>The applicant should have a reliable smartphone.</p>
                                    </div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                              <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                                 <div class="fw-iconbox-image">
                                    <i class="rt rt-icon2-speech-bubble"></i>
                                 </div>
                                 <div class="fw-iconbox-aside">
                                    <div class="fw-iconbox-title">
                                          <h3>Worker network </h3>
                                    </div>
                                    <div class="fw-iconbox-text">
                                          <p>The applicant has access to a network of dedicated, skilled blue-collar workers.</p>
                                    </div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                              <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                                 <div class="fw-iconbox-image">
                                    <i class="rt rt-icon2-speech-bubble"></i>
                                 </div>
                                 <div class="fw-iconbox-aside">
                                    <div class="fw-iconbox-title">
                                          <h3>Communication</h3>
                                    </div>
                                    <div class="fw-iconbox-text">
                                          <p>The applicant is comfortable conversing and writing in Urdu and English.</p>
                                    </div>
                                 </div>
                              </div>
                        </div>
                        <div class="col-sm-4 to_animate">
                              <div class="fw-iconbox clearfix fw-iconbox-2 ib-type1">
                                 <div class="fw-iconbox-image">
                                    <i class="rt rt-icon2-speech-bubble"></i>
                                 </div>
                                 <div class="fw-iconbox-aside">
                                    <div class="fw-iconbox-title">
                                          <h3>Work ethics </h3>
                                    </div>
                                    <div class="fw-iconbox-text">
                                          <p>The applicant displays a strong sense of work ethics.</p>
                                    </div>
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </section>
      <section id="experience-main" class="section_padding_top_100 section_padding_bottom_80 hello_section">
         <div class="container">
            <div class="row">
                  <div class="col-sm-12 col-lg-7 to_animate">
                     <div>
                        <div class="title-block">
                              <h1 class="tit le">How to get started</h1>
                        </div>
                     </div>
                     <!-- <div class="fw-heading">
                        <h5>Kamay believes in adding honest, hardworking and motivated pool of workers. For this, 
                        we take our applicants through a number of steps..</h5>
                        </div> -->
                     <div class="fw-divider-line">
                        <hr>
                     </div>
                     <div class="fw-iconbox clearfix ib-type1">
                        <div class="fw-iconbox-image">
                              <i class="fa fa-file-text-o"></i>
                        </div>
                        <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Register</h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>Create an account as a Supervisor on our website or mobile app.</p>
                              </div>
                        </div>
                     </div>
                     <div class="fw-iconbox clearfix ib-type1">
                        <div class="fw-iconbox-image">
                              <i class="fa fa-clock-o"></i>
                        </div>
                        <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>  
                                    Qualify 
                                 </h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>Successfully complete steps like interview, background check, documentation and training.</p>
                              </div>
                        </div>
                     </div>
                     <div class="fw-iconbox clearfix ib-type1">
                        <div class="fw-iconbox-image">
                              <i class="fa fa-thumbs-o-up"></i>
                        </div>
                        <div class="fw-iconbox-aside">
                              <div class="fw-iconbox-title">
                                 <h3>Start work </h3>
                              </div>
                              <div class="fw-iconbox-text">
                                 <p>If you qualify, your Supervisor account will be activated. You can instantly start adding and managing Kamay. And earn.</p>
                              </div>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </section>
      <section class="ds call-to-action-alt parallax section_padding_top_100 section_padding_bottom_100">
         <div class="container-fluid">
            <div class="container">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="fw-call-to-action">
                              <div>
                                 <h2>Want to become a Supervisor today?</h2>
                                 <br>
                                 <p> Register Now </p>
                              </div>
                              <div class="fw-action-btn">
                                 <a href="#" class="fw-btn fw-btn-1" target="_self">
                                 <span>
                                 <i class="fa fa-mobile"></i>
                                 </span>
                                 </a>
                              </div>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </section>
@endsection()