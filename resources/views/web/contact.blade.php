@extends('layouts.front')

@section('title', 'Contact Us')

@section('content')

    <!--<section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40" style="background: url('images/parallax/contact.jpg'); padding: 7%;">-->
    <section class="page_breadcrumbs ds ms parallax section_padding_top_50 section_padding_bottom_40" >
   <!--<img src="{{ asset ('images/contact_us.png')}}" alt="">-->
   <div class="container">
      <div class="row">
         <div class="col-sm-12 text-center">
            <h1 class="">Contact</h1>
            <!-- <ol class="breadcrumb">
               <li>
                  <a href="/">
                     HomePage
                  </a>
               </li>
               <li>
                  <a href="#">Pages</a>
               </li>
               <li class="active">Contact 3</li>
               </ol> -->
         </div>
      </div>
   </div>
</section>
<section  class="ls section_padding_top_75">
   <div class="container">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14479.723907272855!2d67.0723741!3d24.8662069!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1df148ec840225cf!2sHasnain+Tanweer+Associates!5e0!3m2!1sen!2s!4v1559203877311!5m2!1sen!2s" width="100%" height="400" frameborder="0" style="border: 0px;" allowfullscreen=""></iframe>      
   </div>
   </div>
</section>
<!--<section class="ls section_padding_top_75 section_padding_bottom_100">-->
<!--   <div class="container">-->
<!--      <div class="row">-->
<!--         <div class="col-sm-4 to_animate" data-animation="pullDown">-->
<!--            <div class="teaser text-center">-->
<!--               <div class="m-uppercase">-->
<!--                  <h3>Address</h3>-->
<!--               </div>-->
<!--               <p>-->
<!--                  34/J/3, Block-06, PECHS-->
<!--                  <br> Karachi-->
<!--               </p>-->
<!--            </div>-->
<!--         </div>-->
<!--         <div class="col-sm-4 to_animate" data-animation="pullDown">-->
<!--            <div class="teaser text-center">-->
<!--               <div class="m-uppercase">-->
<!--                  <h3>Opening Hours</h3>-->
<!--               </div>-->
<!--               <p>-->
<!--                  Weekdays 8:00 - 17:00-->
<!--                  <br> Saturday 10:00 - 12:00-->
<!--                  <br> Sunday Closed-->
<!--               </p>-->
<!--            </div>-->
<!--         </div>-->
<!--         <div class="col-sm-4 to_animate" data-animation="pullDown">-->
<!--            <div class="teaser text-center">-->
<!--               <div class="m-uppercase">-->
<!--                  <h3>Contact Us</h3>-->
<!--               </div>-->
<!--               +92-21-34301488 / 89-->
<!--               <br>-->
<!--               <a href="mailto:info@kamay.pk">info@kamay.pk</a>-->
<!--            </div>-->
<!--         </div>-->
<!--      </div>-->
<!--   </div>-->
<!--</section>-->
<section class="ls section_padding_top_75 section_padding_bottom_100">
   <div class="container">
      <div class="row">
         <div class="col-sm-4 to_animate animated pullDown" data-animation="pullDown" style="
            box-shadow: 0 60px 120px -50px rgba(0, 0, 0, 0.9), 0 0 70px 0 rgba(0, 0, 0, 0.1);
            border-color: transparent;
            ">
            <div class="teaser text-center">
               <div class="m-uppercase">
                  <h3>Address</h3>
               </div>
               <p>
                  34/J/3, Block-06, PECHS
                  <br> Karachi
               </p>
            </div>
         </div>
         <div class="col-sm-4 to_animate animated pullDown" data-animation="pullDown" style="box-shadow: 0 60px 120px -50px rgba(0, 0, 0, 0.9), 0 0 70px 0 rgba(0, 0, 0, 0.1);border-color: transparent;">
            <div class="teaser text-center" style="
               /* box-shadow: 0 60px 120px -50px rgba(0, 0, 0, 0.9), 0 0 70px 0 rgba(0, 0, 0, 0.1); */
               /* border-color: transparent; */
               ">
               <div class="m-uppercase">
                  <h3>Opening Hours</h3>
               </div>
               <p>
                  Weekdays 8:00 - 17:00
                  <br> Saturday 10:00 - 12:00
                  <br> Sunday Closed
               </p>
            </div>
         </div>
         <div class="col-sm-4 to_animate animated pullDown" data-animation="pullDown" style="
            box-shadow: 0 60px 120px -50px rgba(0, 0, 0, 0.9), 0 0 70px 0 rgba(0, 0, 0, 0.1);
            border-color: transparent;
            ">
            <div class="teaser text-center">
               <div class="m-uppercase">
                  <h3>Contact Us</h3>
               </div>
               +92-21-34301488 / 89
               <br>
               <a href="mailto:info@kamay.pk">info@kamay.pk</a>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="ds parallax contact-section section_padding_top_100 section_padding_bottom_100">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 to_animate">
            <form class="contact-form columns_padding_5" action="{{route('contact_submit')}}" method="POST" enctype="multipart/form-data" >
               <div>
                  <p class="contact-form-name">
                     <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">
                  </p>
                  <p class="contact-form-email">
                     <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email">
                  </p>
                  <p class="contact-form-subject">
                     <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
                  </p>
               </div>
               <div>
                  <p class="contact-form-message">
                     <textarea aria-required="true" rows="6" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea>
                  </p>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <p class="contact-form-submit text-center topmargin_30">
                        <button type="submit" id="" name="" class="theme_button round-icon-big round-icon colormain and-white">Send Message
                        <i class="rt-icon2-tick-outline"></i>
                        </button>
                     </p>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
   
@endsection()