@extends('layouts.front')

@section('title', 'Vendor')

@section('content')

		<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
   <div id="box_wrapper">
      <!-- template sections -->
      <section class="intro_section page_mainslider ds">
         <!--<div class="main-slider__scroll m-uppercase" id="main-slider__scroll">-->
         <!--   <a href="#scroll-down">-->
         <!--   <i class="fa fa-arrow-down"></i>-->
         <!--   <span>scroll</span>-->
         <!--   </a>-->
         <!--</div>-->
         <div class="flexslider">
            <ul class="slides">
               <li>
                  <img src="{{ asset ('images/vendor_slider.png')}}" alt="">
                  <div class="container">
                     <div class="row">
                        <div class="col-sm-12 ">
                           <div class="slide_description_wrapper">
                              <div class="top-corner">
                              </div>
                              <div class="slide_description">
                                 <div class="intro_slider_alt_navigation">
                                 </div>
                                 <div class="intro-layer" data-animation="slideExpandUp">
                                 	<h3 class="text-uppercase topmargin_40 " style="color: black;">Market & monetize your <br/> business with Kamay
                                 </h3>
                                 </div>
                                 <div class="intro-layer" data-animation="slideExpandUp">
                                 	<p class="grey bottommargin_40" style="
                                 	color: black;">Launch your online store and display your products for sale 
                                 	as a <br/>verified vendor. Interact with thousands of customers and receive 
                                 	<br/>business without hassle.
                                 	</p>
                                 </div>
                                 <br><br>
                                 <div class="col-sm-4">
                                    <div>
                                       <a class="theme_button" href="#">learn more</a>
                                       
                                    </div>
                                 </div>
                                 <div class="col-sm-3">
                                    <div>
                                       <a class="theme_button" href="#signup" style="background-color: #f8d808; border: 3px solid #f8d808;">Sign Up</a>
                                    </div>
                                 </div>
                                 
                              </div>
                           </div>
                           <!-- eof .slide_description -->
                        </div>
                        <!-- eof .slide_description_wrapper -->
                     </div>
                     <!-- eof .col-* -->
                  </div>
                  <!-- eof .row -->
         </div>
         <!-- eof .container -->
         </li>
         </ul>
   </div>
   <!-- eof flexslider -->
   </section>
   <section class=" section_padding_top_65 section_padding_bottom_100 recent-posts-carousel">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 text-center">
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="slide_description">
                     <div class="intro-layer" data-animation="slideExpandUp">
                        <h3  class="text-uppercase topmargin_40" style="color: black;">
                           Sell online. 
                           <br/> Create your online store. 
                        </h3>
                     </div>
                     <div class="intro-layer" data-animation="slideExpandUp">
                        <p class="grey bottommargin_40" style="color:black;" >
                           Are you a small or medium-sized vendor? Kamay provides you a wide-scale opportunity to market your business online.
                           <br/><br/>Enjoy a digital presence which promotes and monetizes your business. 
                        </p>
                     </div>
                     <div>
                        <a class="theme_button" href="/shop-right">View marketplace</a>
                     </div>
                  </div>
               </div>
               <!--<div class="col-sm-6">-->
               <!--   <div class="isotope_container isotope masonry-layout" data-filters=".isotope_filters">-->
               <!--      <div class="isotope-item  col-sm-6 plumbing">-->
               <!--         <div class="vertical-item gallery-title-item content-absolute">-->
               <!--            <div class="item-media">-->
               <!--               <img src="images/gallery/01.png" alt="">-->
               <!--               <div class="media-links">-->
               <!--                  <div class="links-wrap">-->
               <!--                     <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{ asset ('images/gallery/01.jpg')}}"></a>-->
               <!--                     <a class="p-link" title="" href="/shop-right"></a>-->
               <!--                     <h4>Sanitary fittings</h4>-->
               <!--                     <p></p>-->
               <!--                  </div>-->
               <!--               </div>-->
               <!--            </div>-->
               <!--         </div>-->
               <!--      </div>-->
               <!--      <div class="isotope-item col-sm-6 plumbing">-->
               <!--         <div class="horizona-item gallery-title-item content-absolute">-->
               <!--            <div class="item-media">-->
               <!--               <img src="{{ asset('images/gallery/02.png')}}" alt="">-->
               <!--               <div class="media-links">-->
               <!--                  <div class="links-wrap">-->
               <!--                     <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{ asset ('images/gallery/02.jpg')}}"></a>-->
               <!--                     <a class="p-link" title="" href="/shop-right"></a>-->
               <!--                     <h4>Wood material</h4>-->
               <!--                     <p></p>-->
               <!--                  </div>-->
               <!--               </div>-->
               <!--            </div>-->
               <!--         </div>-->
               <!--      </div>-->
               <!--      <div class="isotope-item col-sm-6 plumbing" style="padding-top: 20px;">-->
               <!--         <div class="vertical-item gallery-title-item content-absolute">-->
               <!--            <div class="item-media">-->
               <!--               <img src="{{ asset('images/gallery/03.png')}}" alt="">-->
               <!--               <div class="media-links">-->
               <!--                  <div class="links-wrap">-->
               <!--                     <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" -->
               <!--                        href="{{ asset ('images/gallery/03.jpg')}}"></a>-->
               <!--                     <a class="p-link" title="" href="/shop-right"></a>-->
               <!--                     <h4>Hardware & tools</h4>-->
               <!--                      <p>Plumbing</p> -->
               <!--                  </div>-->
               <!--               </div>-->
               <!--            </div>-->
               <!--         </div>-->
               <!--      </div>-->
               <!--      <div class="isotope-item col-sm-6 plumbing" style="padding-top: 20px;">-->
               <!--         <div class="vertical-item gallery-title-item content-absolute">-->
               <!--            <div class="item-media">-->
               <!--               <img src="{{ asset('images/gallery/04.png')}}" alt="">-->
               <!--               <div class="media-links">-->
               <!--                  <div class="links-wrap">-->
               <!--                     <a class="p-view prettyPhoto " -->
               <!--                        title="" data-gal="prettyPhoto[gal]" href="{{ asset ('images/gallery/04.jpg')}}"></a>-->
               <!--                     <a class="p-link" title="" href="/shop-right"></a>-->
               <!--                     <h4>Auto parts</h4>-->
               <!--                      <p>Plumbing</p> -->
               <!--                  </div>-->
               <!--               </div>-->
               <!--            </div>-->
               <!--         </div>-->
               <!--      </div>-->
               <!--       eof .isotope_container.row -->
               <!--   </div>-->
               <!--</div>-->
               <div class="col-sm-3">
                   <div class="isotope_container isotope row masonry-layout" data-filters=".isotope_filters">
                       <div class="isotope-item col-lg-12 col-md-2 col-sm-1 plumbing" style="padding: 0px;">
                          <div class="vertical-item gallery-title-item content-absolute">
                             <div class="item-media">
                                <img src="{{ asset ('images/gallery/01.png')}}" alt="">
                                <div class="media-links">
                                   <div class="links-wrap">
                                      <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/new-01.png')}}"></a>-->
                                      
                                      <h4>Sanitary fittings</h4>
                                      
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="isotope-item col-lg-12 col-md-2 col-sm-1 plumbing" style="padding: 0px;">
                          <div class="vertical-item gallery-title-item content-absolute">
                             <div class="item-media">
                                <img src="{{ asset ('images/gallery/03.png')}}" alt="">
                                <div class="media-links">
                                   <div class="links-wrap">
                                      <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/new-01.png')}}"></a>-->
                                      
                                    <h4>Hardware & tools</h4>
                                    </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
               </div>
               <div class="col-sm-3">
                   <div class="isotope_container isotope row masonry-layout" data-filters=".isotope_filters">
                       <div class="isotope-item col-lg-12 col-md-2 col-sm-1 plumbing" style="padding: 0px;">
                          <div class="vertical-item gallery-title-item content-absolute">
                             <div class="item-media">
                                <img src="{{ asset ('images/gallery/02.png')}}" alt="">
                                <div class="media-links">
                                   <div class="links-wrap">
                                      <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/new-01.png')}}"></a>-->
                                      
                                      <h4>Wood material</h4>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="isotope-item col-lg-12 col-md-2 col-sm-1 plumbing" style="padding: 0px;">
                          <div class="vertical-item gallery-title-item content-absolute">
                             <div class="item-media">
                                <img src="{{ asset ('images/gallery/04.png')}}" alt="">
                                <div class="media-links">
                                   <div class="links-wrap">
                                      <!--<a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]" href="{{asset ('images/gallery/new-01.png')}}"></a>-->
                                      <h4>Auto parts</h4>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .owl-carousel -->
</div>
<!-- .col- -->
</section> 
<section class="ls ms section_padding_top_65 section_padding_bottom_100 recent-posts-carousel" id="signup">
   <div class="container">
      <div class="row">
         <div class="row">
    <div class="col-sm-12">
        <div class="title-block" style="margin: auto; width: 56%; margin-bottom: 5%;">
             <h1 class="title">How to get started</h1>
          </div><div>
        <div class="col-sm-6">
               <div class="fw-iconbox clearfix ib-type1">
                  
               </div>
               <div class="fw-divider-line">
                  <hr>
               </div>
               <div class="fw-iconbox clearfix ib-type1">
                  <div class="fw-iconbox-image">
                     <i class="fa fa-file-text-o"></i>
                  </div>
                  <div class="fw-iconbox-aside">
                     <div class="fw-iconbox-title">
                        <h3>Register</h3>
                     </div>
                     <div class="fw-iconbox-text">
                        <p>Create an account as a Vendor on our website or mobile app. 
                        </p>
                     </div>
                  </div>
               </div>
               <div class="fw-iconbox clearfix ib-type1">
                  <div class="fw-iconbox-image">
                     <i class="fa fa-clock-o"></i>
                  </div>
                  <div class="fw-iconbox-aside">
                     <div class="fw-iconbox-title">
                        <h3>	
                           Verification 
                        </h3>
                     </div>
                     <div class="fw-iconbox-text">
                        <p>Successfully complete steps like interview, background check and documentation.</p>
                     </div>
                  </div>
               </div>
               <div class="fw-iconbox clearfix ib-type1">
                  <div class="fw-iconbox-image">
                     <i class="fa fa-thumbs-o-up"></i>
                  </div>
                  <div class="fw-iconbox-aside">
                     <div class="fw-iconbox-title">
                        <h3>Start Work</h3>
                     </div>
                     <div class="fw-iconbox-text">
                        <p>If you qualify, your Vendor account will be activated. 
                           You can instantly launch your online store and start selling. 
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-6">
               <div class="teaser text-center with_background">
                  <div class="row">
                     <div class="col-sm-12 to_animate animated fadeInUp">
                        <div class="text-center">
                           <form class="contact-form width-6-col columns_padding_5" method="post" action="/">
                              <div>
                                 <h3>Sign up as Vendor	 </h3>
                                 <p class="contact-form-name">
                                    <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control lf" placeholder="Full Name">
                                 </p>
                                 <p class="contact-form-email">
                                    <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control lf" placeholder="Email">
                                 </p>
                                 <p class="contact-form-submit text-center topmargin_30">
                                    <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button round-icon round-icon-big colordark">Register Now
                                    <i class="rt-icon2-arrow-right-outline"></i>
                                    </button>
                                 </p>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- eof .isotope_container.row -->
         </div></div></div>
      </div>
   </div>
   </div>
   <!-- .owl-carousel -->
   </div>
   <!-- .col- -->
</section>

@endsection()