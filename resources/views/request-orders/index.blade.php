@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Request Orders</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Request Orders
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Request Orders List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="col-md-12 request-order-col">
                    @foreach($orders as $order)
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="font-weight-bold mb-1">Order ID#{{$order->order_id}}</h5>
                            <div class="request-order-left">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Created At</th>
                                            <th>Customer Name</th>
                                            <th>Address</th>
                                            <th>Total Amount</th>
                                            <th>Status</th>
                                            <th>Service</th>
                                        </tr>
                                        <tr>
                                            <td>{{$order->order_id}}</td>
                                            <td>{{date("l jS \of F Y h:i:s A", strtotime($order->create_at))}}</td>
                                            <td>{{$order->user->fullName}}</td>
                                            <td>{{$order->full_address}}</td>
                                            <td>Rs. {{$order->total_amount}}</td>
                                            <td>{{$order->status}}</td>
                                            <td>{{$order->service->name}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                @if(count($order->request_orders) != 0)
                                    <div class="row">
                                    @foreach($order->request_orders as $indexKey => $request_orders)
                                        <div class="col-md-4">
                                            <div class="card-content" style="box-shadow: 1px 1px 4px 0px;border-radius: 8px;">
                                                <div class="card-body cleartfix">
                                                    <span class="index-number">{{$indexKey + 1}}</span>
                                                    <h4 class="mb-1">{{$request_orders->user->fullName}} <span class="main-star float-right">
                                                        <?php
                                                            for ($i=0; $i < 5; $i++){
                                                                if (round($request_orders->user->kamay_rating()) > $i) {?>
                                                                    <span class="fa fa-star checked"></span>
                                                        <?php
                                                            }else{?>
                                                        <span class="fa fa-star"></span>
                                                        <?php  }
                                                            }?>
                                                        </span>
                                                    </h4>
                                                    <div class="media align-items-center">
                                                        <div class="align-self-center">
                                                            <a href="#" class="mr-1">
                                                                <img src="{{$request_orders->user->image}}" alt="users view avatar" class="users-avatar-shadow rounded-circle" height="100" width="100" style="object-fit: cover;border: 2px solid #F4F5FA;">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="upper-media-body">
                                                                <h5>Kamay ID: <span>{{$request_orders->user->regId}}</span></h5>
                                                                <h5>Distance: <span>{{$request_orders->distance}} KM</span></h5>
                                                                <h5>Duration: <span>{{$request_orders->duration}}</span></h5>
                                                                <h5><span>{{$request_orders->user->pri_lat}} | {{$request_orders->user->pri_long}}</span></h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="upper-media-body mt-1 mb-1">
                                                        <span class="btn-success btn-sm">
                                                        @foreach($request_orders->user->user_services as $user_services)
                                                            {{$user_services->name}}
                                                        @endforeach
                                                        </span>
                                                    </div>
                                                    <span class="btn-info btn-sm mb-1 d-inline-block">{{date("l jS \of F Y h:i:s A", strtotime($request_orders->created_at))}}</span>
                                                    <h5>Address: <span>-</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col-12">
                            {!! $orders->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush