@extends('layouts.admin')
   
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">My Area</h3>
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Area
                  </li>
                </ol>
              </div>
            </div>
        </div>
    </div>
    
         @if($message = Session::get('success'))
	        <div class="alert alert-success">
	        	<p>{{$message}}</p>
	        </div>
         @endif
        
        
   <div class="row">


          <div id="recent-sales" class="col-12 col-md-12">

            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Area List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                
              </div>
              <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                     	<tr>
		                  	<th>ID</th>
		                    <th>Area</th>
		                   	<th>Status</th>
		                   	<!-- <th>Action</th> -->
		                </tr>
                    </thead>
                    <tbody>
                       @foreach($data as $area)
                       <tr>
                        <td class="text-truncate">{{$area->id}}</td>
                          <td class="text-truncate">{{$area->territory_name}}</td>
                          <!-- <td class="text-truncate">{{$area->status}}</td> -->
                          <td align="center"> 
                            <div class="fonticon-wrap">
                                <a href="{{ route('area.edit', $area->id) }}"><i class="ft-edit"></i></a>
                            </div>
                          </td>

	               		
                         
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@push('scripts')

@endpush