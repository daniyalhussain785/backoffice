@extends('layouts.admin')
   @push('styles')
    <style>
        .pac-container {
            z-index: 9999999 !important;
            top: 65px !important;
            left: 15px !important;
        }
        #address-input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: red;
            opacity: 1; /* Firefox */
        }

        #address-input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: red;
        }

        #address-input::-ms-input-placeholder { /* Microsoft Edge */
            color: red;
        }
    </style>
@endpush
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Edit area</h3>
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a>
                  </li>
<!--                   <li class="breadcrumb-item active">Create Booking
                  </li> -->
                </ol>
              </div>
            </div>
        </div>
    </div>
    @if($errors->any())
        <div class="alert alert-danger">
         <ul>
          @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
         </ul>
        </div>
        @endif

         @if($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{$message}}</p>
          </div>
        @endif
    <div class="content-body">
        <section id="basic-form-layouts">
            <div class="row match-height">
             <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                      <h4 class="card-title" id="basic-layout-form">area Update</h4>
                      <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                         <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                         </ul>
                      </div>
                   </div>
                   <div class="card-content collapse show">
                      
                        <div class="card-body">
                        <form class="form" action="{{route('area.update',$data->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')  
                            <div class="form-body">
                               <div class="row">
                                <div class="col-md-6">
                                     <div class="form-group">
                                      <label for="services">Select Area</label>
                                          <select class="form-control" name="terr_id">
                                             @foreach($territory as $area)
                                            <option value="{{$area->terr_id}}" {{  $area->terr_id ==  $data->terr_id ? 'selected' : ''  }}>{{$area->name}}
                                            </option>
                                            @endforeach
                                          </select>
                                     </div>
                                  </div>
                                 

                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="projectinput1">Select Status</label>
                                       <select name="status" id="status" class="form-control" >
                                       
                                          <option value="">Select Status</option>
                                          <option value="1" {{  $data->status == 1 ? 'selected' : ''  }}>Block</option>
                                          <option value="0" {{  $data->status == 0 ? 'selected' : ''  }}>Unblock</option>   
                                        
                                      </select>     
                                     </div>
                                  </div>

                              </div>
                            </div> 
                            <div class="form-actions text-right">
                               <button type="submit" class="btn btn-primary">
                               <i class="la la-check-square-o"></i> Save
                               </button>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
            
          </div>
       </section>
      
</div>

@stop

@section('script')
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
    $(":input").inputmask();
</script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAggB51ZRomTQ28c6SIlT0Zkjtd2boQ3V0&libraries=places&callback=initialize" async defer></script>
<script>
    var get_city = {!! json_encode($cityData->toArray()) !!};
    function initialize() {
    
      $('form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          return false;
        }
      });
      const locationInputs = document.getElementsByClassName("map-input");
    
      const autocompletes = [];
      const geocoder = new google.maps.Geocoder;
      for (let i = 0; i < locationInputs.length; i++) {
    
        const input = locationInputs[i];
        const fieldKey = input.id.replace("-input", "");
        const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';
    
        const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -33.8688;
        const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 151.2195;
    
        const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
          center: {lat: latitude, lng: longitude},
          zoom: 13
        });
      console.log(map);
        const marker = new google.maps.Marker({
          map: map,
          position: {lat: latitude, lng: longitude},
        });
    
        marker.setVisible(isEdit);
    
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
      }
    
      for (let i = 0; i < autocompletes.length; i++) {
        const input = autocompletes[i].input;
        const autocomplete = autocompletes[i].autocomplete;
        const map = autocompletes[i].map;
        const marker = autocompletes[i].marker;
    
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          marker.setVisible(false);
          const place = autocomplete.getPlace();
    
          geocoder.geocode({'placeId': place.place_id}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              const lat = results[0].geometry.location.lat();
            const lng = results[0].geometry.location.lng();
                        var sublocality;
            for (var i = 0; i < results[0].address_components.length; i++) {
              if (results[0].address_components[i].types[0] == "locality") {
                //this is the object you are looking for City
                city = results[0].address_components[i];
              }
                            var component = results[0].address_components[i];
                            if (!sublocality && component.types.indexOf("sublocality_level_1") > -1){
                                sublocality = component.long_name;
                            }
                        }
                    
                        var checker = 0;
                        for(var i = 0; i < get_city.length; i++){
                            if(get_city[i].name == city.long_name){
                                checker = 1;
                            }
                        }
            if(checker == 1){
                            setLocationCoordinates(autocomplete.key, lat, lng, city, sublocality);
                            $('#address-input').removeClass('error');
            }else{
              $('#address-input').attr('placeholder','Sorry, Right Now, we are not Operational in this City');
                            $('#address-input').val('');
              $('#address-latitude').val('0');
              $('#address-longitude').val('0');
              $('#address-input').addClass('error');
              $('#address-city').val('');
                            $('#selected_city').val('')
                            $('#selected_terr').html('')
            }
            }
          });
    
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            input.value = "";
            return;
          }
    
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
    
        });
      }
      }
    
      function setLocationCoordinates(key, lat, lng, city, sublocality) {
      const latitudeField = document.getElementById(key + "-" + "latitude");
      const longitudeField = document.getElementById(key + "-" + "longitude");
      const cityField = document.getElementById(key + "-" + "city");
            const areaField = document.getElementById(key + "-" + "area");
            const selectedCityField = document.getElementById("selected_city");
      latitudeField.value = lat;
      longitudeField.value = lng;
      cityField.value = city.long_name;
            areaField.value = sublocality;
            selectedCityField.value = city.long_name;
            $.ajax({
                url: "{{url('get-area-by-cityname')}}",
                type: "POST",
                data: {
                    city_id: city.long_name,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    console.log(result);
                    $.each(result.area, function(key, value) {
                        $("#selected_area").append('<option value="' + value.terr_id + '">' + value.name + '</option>');
                        $("#selected_terr").append('<option value="' + value.terr_id + '">' + value.name + '</option>');
                    });
                    //$('#city').html('<option value="">Select City First</option>');
                }
            });
    }
    
    var addressInputElement = $('#address-input');
      addressInputElement.on('focus', function () {
      var pacContainer = $('.pac-container');
      $(addressInputElement.parent()).append(pacContainer);
    })

    $(document).ready(function () {
      $('#nav-reg11 .mregisterform input[type=file]').change(function () {
        $('#btnUpload').show();
        $('#divFiles').html('');
        for (var i = 0; i < this.files.length; i++) {
          console.log(URL.createObjectURL(this.files[i]));
          var fileId = i;
          $("#divFiles").append('<li>' +
                      '<img src="' + URL.createObjectURL(this.files[i]) + '">' +
                    '</li>');
        }
      })
    });
  </script>
@endsection
