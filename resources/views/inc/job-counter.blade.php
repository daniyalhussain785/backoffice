<div class="row">
            
                  
                    
              <div class="col-xl-3 col-lg-6 col-12">
                 <a href="{{ url('all-jobs') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$jobCounter[0]->all_jobs}}</h3>
                                        <h6>All Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('view-unassigned') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning">{{$jobCounter[0]->Unassigned}}</h3>
                                        <h6>Unassigned Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="#">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success">{{$jobCounter[0]->Assigned}}</h3>
                                        <h6>Assigned Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase success font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                 <a href="{{ url('view-cancelled') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="danger">{{$jobCounter[0]->Cancelled}}</h3>
                                        <h6>Cancelled Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>

                 <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('view-inprogress') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$jobCounter[0]->InProgress}}</h3>
                                        <h6>InProgress Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('view-completed') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning">{{$jobCounter[0]->Completed}}</h3>
                                        <h6>Completed Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('view-onhold') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success">{{$jobCounter[0]->OnHold}}</h3>
                                        <h6>On-Hold</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase success font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                 <a href="#">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="danger">{{$jobCounter['schedule']}}</h3>
                                        <h6>Scheduled Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                    

                   
                 
          </div>