<div class="row">
            
                  
                    
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$kamaCounter[0]->AllWorker}}</h3>
                                        <h6>All workers</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('final-approval-kamay') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning">{{$kamaCounter[0]->pending}}</h3>
                                        <h6>Pending Workers</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success">{{$kamaCounter[0]->approved}}</h3>
                                        <h6>Aproved workers</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow success font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                 <a href="{{ url('kamayrequest') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="danger">{{$kamaCounter[0]->newKama}}</h3>
                                        <h6>Newly registered</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                    

                   
                 
          </div>