<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="" style="padding: 6px 0px !important; margin-right: 0;">
                        <img class="brand-logo" alt="KWSB logo" src="{{ asset('admin/img/logo.png') }}" >
                        <h3 class="brand-text">Backoffice</h3>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                    <li class="nav-item nav-search">
                        <a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
                        <div class="search-input open">
                            <input class="input" type="text" placeholder="Search Menu...">
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" id="final-approval" href="#" data-toggle="dropdown"><i class="ficon ft-check-circle"></i><span class="badge badge-pill badge-danger badge-up badge-glow">{{$final_approval_count}}</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right" data-href="{{route('final-approval-notf-show')}}" id="final-approval-notf-show">
                            
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" id="panic" href="#" data-toggle="dropdown"><i class="ficon ft-info"></i><span class="badge badge-pill badge-danger badge-up badge-glow">{{$panic_count}}</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right" data-href="{{route('panic-notf-show')}}" id="panic-notf-show">
                            
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" id="notf_user" href="#" data-toggle="dropdown"><i class="ficon ft-users"></i><span class="badge badge-pill badge-danger badge-up badge-glow">{{$admin_notify_count}}</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right" data-href="{{route('user-notf-show')}}" id="user-notf-show">
                            
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                        <span>{{ Auth::user()->name }}
                        <span class="user-name text-bold-700"></span>
                        </span>
                        <span class="avatar avatar-online">
                        <img src="{{ asset('admin/img/user.png') }}" alt="avatar"><i></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#"><i class="ft-user"></i> Edit Profile</a>
                            <!-- <a class="dropdown-item" href="#"><i class="ft-mail"></i> My Inbox</a>
                                <a class="dropdown-item" href="#"><i class="ft-check-square"></i> Task</a>
                                <a class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a> -->
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="{{ (request()->routeIs('home'))? 'active' : '' }}">
                <a class="menu-item" href="{{ url('home') }}" data-i18n="nav.templates.vert.main"><i class="la la-home"></i>Dashboard</a>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-user"></i><span class="menu-title" data-i18n="nav.templates.main">Kama</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('kama.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('kama/create') }}" data-i18n="nav.templates.vert.main">Register worker</a>
                    </li>
                    <li class="{{ (request()->routeIs('kamayrequest'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('kamayrequest') }}" data-i18n="nav.templates.vert.main">Assign supervisor</a>
                    </li>
                    <li class="{{ (request()->routeIs('assigned-kamay'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('assigned-kamay') }}" data-i18n="nav.templates.vert.main">Review by supervisor</a>
                    </li>
                    <li class="{{ (request()->routeIs('final-approval-kamay'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('final-approval-kamay') }}" data-i18n="nav.templates.vert.main">Backoffice approval</a>
                    </li>
                    <li class="{{ (request()->routeIs('view-approved-worker'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('view-approved-worker') }}" data-i18n="nav.templates.vert.main">Approved workers</a>
                    </li>
                    <li class="{{ ( request()->routeIs('kamayview') || request()->routeIs('kama.edit'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('kamayview') }}" data-i18n="nav.templates.vert.main">All workers</a>
                    </li>
                     <li class="{{ ( request()->routeIs('worker-growth-potential') || request()->routeIs('kama.worker-growth-potential'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('worker-growth-potential') }}" data-i18n="nav.templates.vert.main">Worker growth potential</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-user-secret"></i><span class="menu-title" data-i18n="nav.templates.main">Supervisor</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('supervisor.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('supervisor/create') }}" data-i18n="nav.templates.vert.main">Register supervisor</a>
                    </li>
                    <li class="{{ ( request()->routeIs('supervisor.index') || request()->routeIs('supervisor.edit'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('supervisor') }}" data-i18n="nav.templates.vert.main">Supervisor List</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-users"></i><span class="menu-title" data-i18n="nav.templates.main">Customer</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('customer.index') ||  request()->routeIs('customer.show'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('customer') }}" data-i18n="nav.templates.vert.main">List Customer</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-info-circle"></i><span class="menu-title" data-i18n="nav.templates.main">Panic Reported</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('panic.panic-reported'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('panic-reported') }}" data-i18n="nav.templates.vert.main">Panic Reported</a>
                    </li>
                    <li class="{{ (request()->routeIs('panic.panic-resolved'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('panic-resolved') }}" data-i18n="nav.templates.vert.main">Panic Resolved</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-shopping-cart"></i><span class="menu-title" data-i18n="nav.templates.main">Jobs</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('jobs.view-completed'))? 'active' : '' }}{{ (request()->routeIs('jobs.view-unassigned'))? 'active' : '' }}{{ (request()->routeIs('jobs.view-inprogress'))? 'active' : '' }}{{ (request()->routeIs('jobs.all-jobs'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('all-jobs') }}" data-i18n="nav.templates.vert.main">All jobs</a>
                    </li>
                    <!-- <li class="{{ (request()->routeIs('jobs.view-inprogress'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('view-inprogress') }}" data-i18n="nav.templates.vert.main">Jobs in Progress </a>
                        </li> -->
                    <!-- <li class="{{ (request()->routeIs('jobs.view-cancelled'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('view-cancelled') }}" data-i18n="nav.templates.vert.main">Jobs Cancelled</a>
                        </li> -->
                    <!-- <li class="{{ (request()->routeIs('jobs.view-unassigned'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('view-unassigned') }}" data-i18n="nav.templates.vert.main">Jobs Unassigned</a>
                        </li>
                        <li class="{{ (request()->routeIs('jobs.view-completed'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('view-completed') }}" data-i18n="nav.templates.vert.main">Jobs Completed</a>
                        </li> -->
                    <li class="{{ (request()->routeIs('jobs.today-jobs'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('today-jobs') }}" data-i18n="nav.templates.vert.main">Today jobs</a>
                    </li>
                    <li class="{{ (request()->routeIs('jobs.view-scheduled'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('view-scheduled') }}" data-i18n="nav.templates.vert.main">Scheduled jobs</a>
                    </li>
                    <li class="{{ (request()->routeIs('jobs.jobs-report'))? 'active' : '' }}{{ (request()->routeIs('jobs.jobs-report-range'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('jobs-report') }}" data-i18n="nav.templates.vert.main">Jobs report</a>
                    </li>
                </ul>
            </li>
            <!-- <li class=" nav-item">
                <a href="#"><i class="la la-bar-chart"></i><span class="menu-title" data-i18n="nav.templates.main">Job Performances</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs(''))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('') }}" data-i18n="nav.templates.vert.main">Low Rated Jobs</a>
                    </li>
                    <li class="{{ (request()->routeIs(''))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('') }}" data-i18n="nav.templates.vert.main">View</a>
                    </li>
                    <li class="{{ (request()->routeIs(''))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('') }}" data-i18n="nav.templates.vert.main">High Rated Jobs</a>
                    </li>
                </ul>
            </li> -->
            <?php 
                if (Auth::user()->role == 'admin') { ?>
            <li class=" nav-item">
                <a href="#"><i class="la la-map"></i><span class="menu-title" data-i18n="nav.templates.main">City</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('city.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('city/create') }}" data-i18n="nav.templates.vert.main">Add</a>
                    </li>
                    <li class="{{ (request()->routeIs('city.index'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('city') }}" data-i18n="nav.templates.vert.main">List city</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item {{ (request()->routeIs('no.service.charges'))? 'active' : '' }}">
                <a href="{{ url('no-service-charges') }}"><i class="la la-info-circle"></i><span class="menu-title" data-i18n="nav.templates.main">No Service Charges</span></a>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-share-alt"></i><span class="menu-title" data-i18n="nav.templates.main">Services Shares</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('share.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('share/create') }}" data-i18n="nav.templates.vert.main">Add Shares</a>
                    </li>
                    <li class="{{ ( request()->routeIs('share.index') || request()->routeIs('share.edit') )? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('share') }}" data-i18n="nav.templates.vert.main">List Shares</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item has-sub">
                <a href="#"><i class="la la-cogs"></i><span class="menu-title" data-i18n="Templates">Services</span></a>
                <ul class="menu-content" style="">
                    <li class="has-sub">
                        <a class="menu-item" href="#"><i></i><span data-i18n="Vertical">Main Services</span></a>
                        <ul class="menu-content" style="">
                            <li class="{{ (request()->routeIs('services.create'))? 'active' : '' }}">
                                <a class="menu-item" href="{{ url('services/create') }}" data-i18n="nav.templates.vert.main">Add</a>
                            </li>
                            <li class="{{ ( request()->routeIs('services.index') || request()->routeIs('services.edit') )? 'active' : '' }}">
                                <a class="menu-item" href="{{ url('services') }}" data-i18n="nav.templates.vert.main">List services</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="menu-item" href="#"><i></i><span data-i18n="Horizontal">Sub Services</span></a>
                        <ul class="menu-content" style="">
                            <li class="{{ (request()->routeIs('subservices.create'))? 'active' : '' }}">
                                <a class="menu-item" href="{{ url('subservices/create') }}" data-i18n="nav.templates.vert.main">Add</a>
                            </li>
                            <li class="{{ (request()->routeIs('subservices.index')) || (request()->routeIs('subservices.edit')) ? 'active' : '' }}">
                                <a class="menu-item" href="{{ url('subservices') }}" data-i18n="nav.templates.vert.main">List subservices</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-user-plus"></i><span class="menu-title" data-i18n="nav.templates.main">Agents</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('agent.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('agent/create') }}" data-i18n="nav.templates.vert.main">Add</a>
                    </li>
                    <li class="{{ (request()->routeIs('agent.index'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('agent') }}" data-i18n="nav.templates.vert.main">List agent</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item {{ (request()->routeIs('admin.request.order'))? 'active' : '' }}">
                <a href="{{route('admin.request.order')}}"><i class="la la-connectdevelop"></i><span class="menu-title" data-i18n="nav.templates.main">Request Orders</span></a>
            </li>
            <li class=" nav-item {{ (request()->routeIs('admin.payment'))? 'active' : '' }}">
                <a href="{{route('admin.payment')}}"><i class="la la-cc-visa"></i><span class="menu-title" data-i18n="nav.templates.main">Payments</span></a>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-shield"></i><span class="menu-title" data-i18n="nav.templates.main">Badges</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('badges.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('badges/create') }}" data-i18n="nav.templates.vert.main">Add Badges</a>
                    </li>
                    <li class="{{ ( request()->routeIs('badges.index') || request()->routeIs('badges.edit') )? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('badges') }}" data-i18n="nav.templates.vert.main">List Badges</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item {{ (request()->routeIs('assign-badges.index'))? 'active' : '' }}">
                <a href="{{route('assign-badges.index')}}"><i class="la la-trophy"></i><span class="menu-title" data-i18n="nav.templates.main">Assign Badges</span></a>
            </li>
            <li class=" nav-item {{ (request()->routeIs('bulk.message'))? 'active' : '' }}">
                <a href="{{route('bulk.message')}}"><i class="la la-send"></i><span class="menu-title" data-i18n="nav.templates.main">Bulk Message</span></a>
            </li>
            <li class=" nav-item {{ (request()->routeIs('slider.index'))? 'active' : '' }}">
                <a href="#"><i class="la la-image"></i><span class="menu-title" data-i18n="nav.templates.main">Slider Images</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('slider.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{route('slider.create')}}" data-i18n="nav.templates.vert.main">Add Slider Images</a>
                    </li>
                    <li class="{{ ( request()->routeIs('slider.index') || request()->routeIs('slider.edit') )? 'active' : '' }}">
                        <a class="menu-item" href="{{route('slider.index')}}" data-i18n="nav.templates.vert.main">Slider Images List</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-ban"></i><span class="menu-title" data-i18n="nav.templates.main">Block area</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('area.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('area/create') }}" data-i18n="nav.templates.vert.main">Add</a>
                    </li>
                    <li class="{{ (request()->routeIs('area.index'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('area') }}" data-i18n="nav.templates.vert.main">List area</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">

                <a href="#"><i class="la la-bell"></i><span class="menu-title" data-i18n="nav.templates.main">Notifications</span></a>
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('notification.index') || request()->routeIs('notification.edit') )? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('notification') }}" data-i18n="nav.templates.vert.main">List notification</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item">
                <a href="#"><i class="la la-question"></i><span class="menu-title" data-i18n="nav.templates.main">Faqs</span></a> 
                <ul class="menu-content">
                    <li class="{{ (request()->routeIs('faqs.create'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('faqs/create') }}" data-i18n="nav.templates.vert.main">Add Faqs</a>
                    </li>
                    <li class="{{ ( request()->routeIs('faqs.index') || request()->routeIs('faqs.edit'))? 'active' : '' }}">
                        <a class="menu-item" href="{{ url('faqs') }}" data-i18n="nav.templates.vert.main">List Faqs</a>

                    </li>
                </ul>
            </li>
            <li class=" nav-item {{ (request()->routeIs('schedule.index'))? 'active' : '' }}">
                <a href="{{route('schedule.index')}}"><i class="la la-clock-o"></i><span class="menu-title" data-i18n="nav.templates.main">Schedule Job Time</span></a>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>