
        <li class="dropdown-menu-header">
            <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6>
            <span class="notification-tag badge badge-danger float-right m-0">{{$total_notify_count}} Total</span>
        </li>
        <li class="scrollable-container media-list w-100">
            @foreach($datas as $admin_notifys)
            <a href="{{route('panic.show', $admin_notifys->panic->id) }}">
                <div class="media">
                    <div class="media-left align-self-center"><i class="ft-user-check icon-bg-circle bg-cyan mr-0"></i></div>
                    <div class="media-body">
                        <h6 class="media-heading">{{$admin_notifys->notify->title}} #{{$admin_notifys->panic->order_id}}</h6>
                        <p class="notification-text font-small-3 text-muted">{{$admin_notifys->notify->body}}</p>
                        <p class="notification-text font-small-3 text-muted">Panic Reported By 
                        @if($admin_notifys->panic->role_id == 1)
                        <!-- kamay -->
                        {{$admin_notifys->panic->user->fullName}} (Kamay)
                        @else
                        <!-- customer -->
                        {{$admin_notifys->panic->user->fullName}} (Customer)
                        @endif
                        </p>
                        <small>
                        <time class="media-meta text-muted">{{$admin_notifys->panic->created_at->diffForHumans()}}</time></small>
                    </div>
                </div>
            </a>
            @endforeach
        </li>
        <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all notifications</a></li>