@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Slider Images</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Slider Images
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Slider Images List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($slider as $sliders)
                            <tr>
                                <td class="text-truncate">{{$sliders->id}}</td>
                                <td class="text-truncate">
                                    <img src="{{$sliders->image}}" alt="{{$sliders->title}}" width="150">
                                </td>
                                <td class="text-truncate">{{$sliders->title}}</td>
                                <td class="text-truncate">
                                  @if($sliders->status == 1)
                                    <button class="btn btn-primary btn-sm">Active</button>
                                  @else
                                    <button class="btn btn-primary btn-sm">Deactive</button>
                                  @endif
                                </td>
                                <td align="center">
                                    <div class="fonticon-wrap">
                                        <a class="btn btn-icon btn-info btn-sm" href="{{ route('slider.edit', $sliders->id) }}"><i class="ft-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush