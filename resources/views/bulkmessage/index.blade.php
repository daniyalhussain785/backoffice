@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">Bulk Message</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Bulk Message</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form">Send Bulk Message</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" method="POST" enctype="multipart/form-data">
                                @csrf   
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="p_id">Select Message Type <span>*</span></label>
                                                <fieldset class="radio">
                                                    <label>
                                                    <input type="radio" name="type" value="0" checked>
                                                    Push Notification
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                    <input type="radio" name="type" value="1">
                                                    Send Message
                                                    </label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group skin skin-line">
                                                <label for="b_id">Select Audience <span>*</span></label>
                                                <fieldset class="radio">
                                                    <label>
                                                    <input type="radio" name="audience" value="1" checked>
                                                    Select All Workers
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                    <input type="radio" name="audience" value="3">
                                                    Select All Customers
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                    <input type="radio" name="audience" value="2">
                                                    Select All Supervisors
                                                    </label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="b_id">Select Service</label>
                                            <ul class="service">
                                            @foreach($service as $services)
                                                <li>
                                                    <fieldset class="checkboxsas">
                                                        <label id="service_{{$services->s_id}}">
                                                            <input id="service_{{$services->s_id}}" type="checkbox" value="{{$services->s_id}}" name="service[]">
                                                            {{$services->name}}
                                                        </label>
                                                    </fieldset>
                                                </li>
                                            @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="users_id">All Users <span class="required">*</span></label>
                                                <select name="users_id[]" id="users_id" class="form-control select2" multiple required>
                                                @foreach($person as $persons)
                                                    <option value="{{$persons->regId}}" selected>{{$persons->fullName}}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" id="title" class="form-control" placeholder="Title" name="title">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="message">Message <span>*</span></label>
                                                <textarea name="message" id="message" cols="30" rows="10" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
               <div class="card">
                  <div class="card-header">
                     <h4 class="card-title" id="basic-layout-colored-form-control">Information</h4>
                     <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                     <div class="heading-elements">
                        <ul class="list-inline mb-0">
                           <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                           <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                           <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                           <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="card-content collapse show">
                     <div class="card-body pt-0" id="error_box">
                        @if($errors->any())
                        <div class="alert alert-danger">
                           <ul>
                              @foreach($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('error'))
                        <div class="alert alert-danger">
                           <ul>
                              <li>{!! \Session::get('error') !!}</li>
                           </ul>
                        </div>
                        @endif
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                           <ul>
                              <li>{!! \Session::get('success') !!}</li>
                           </ul>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('scripts')
<script>
    $("input[name='audience']").click(function () {
        var service = [];
        $.each($("input[name='service[]']:checked"), function(){
            service.push($(this).val());
        });
        $.ajax({
            url: "{{url('get-user-by-role')}}",
            type: "POST",
            data: {
                r_id: $(this).val(),
                service: service,
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function(result) {
                if(result.status == true){

                    if(result.data.length != 0){
                        $('#users_id').val(null).trigger('change');
                        var selectedValues = new Array();
                        for(var i = 0; i < result.data.length; i++){
                            var option = new Option(result.data[i].fullName, result.data[i].regId, true, true);
                            $('#users_id').append(option);
                        }
                        // $('#users_id').val(selectedValues);
                        $('#users_id').trigger('change');
                    }else{
                        $('#users_id').val('');
                        $('#users_id').trigger('change');
                    }
                }else{
                    $('#users_id').val('');
                    $('#users_id').trigger('change');
                }
                console.log(result);
            }
        });

    });
    $(".service li label").click(function(){
        var service = [];
        $.each($("input[name='service[]']:checked"), function(){
            service.push($(this).val());
        });

        your_object = {
            service: service,
            audience: $('input[name="audience"]:checked').val()
        };
        if($('input[name="audience"]:checked').val() != 3){
            _ajax.post("{{route('bulk.message.with.service')}}",  your_object, function(response) {
                if(response.status){
                    if(response.data.length != 0){
                        $('#users_id').val(null).trigger('change');
                        var selectedValues = new Array();
                        for(var i = 0; i < response.data.length; i++){
                            var option = new Option(response.data[i].fullName, response.data[i].regId, true, true);
                            $('#users_id').append(option);
                        }
                        // $('#users_id').val(selectedValues);
                        $('#users_id').trigger('change');
                    }else{
                        $('#users_id').val('');
                        $('#users_id').trigger('change');
                    }
                }else{
                    $('#users_id').val('');
                    $('#users_id').trigger('change');
                }
            })
        }
        console.log(service);
    });

    $('.form').on('submit', function(event){
        event.preventDefault();
        var formData = new FormData(this);
        $type = $('input[name="type"]:checked').val();
        $error = '<div class="alert alert-danger"><ul>';
        $error_count = 0;
        if($type == 0){
            $title = $('input[name="title"]').val();
            if($title == ''){
                $error += "<li>Title Field is Required for Push Notification</li>";
                $error_count = 1;
            }
        }else{
            $error_count = 0;
        }
        $error += '</ul></div>';
        if($error_count != 1){
            _ajax.postFormData("{{route('bulk.message.send')}}",formData , function(response) {
                if(response.status){
                    $('#error_box').html('');
                    $('#error_box').html('<div class="alert alert-success"><ul><li>'+response.message+'</li></ul></div>');
                }else{
                    console.log(response);
                    var errorString = '<div class="alert alert-danger"><ul>';
                    $.each( response.data, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('#error_box').html('');
                    $('#error_box').html(errorString);
                }
                window.scrollTo(0, 0);
            })
        }else{
            $('#error_box').html($error);
            $("input.alert-danger").first().focus();
            window.scrollTo(0, 0);
            return false;
        }
    });
</script>
@endpush