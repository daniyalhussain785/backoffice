@extends('layouts.admin')
@section('pageTitle', 'Dashboard')
@section('content')
            <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                        <h3 class="content-header-title mb-0 d-inline-block"><a href="{{url('send-notification')}}" class="btn btn-success"> send notification</a></h3>
                        <div class="row breadcrumbs-top d-inline-block">
                          <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="#">Home</a>
                              </li>
                              <li class="breadcrumb-item active">
                                
                              </li>
                            </ol>
                          </div>
                        </div>
                    </div>
                </div>
     
@endsection
    