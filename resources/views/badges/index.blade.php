@extends('layouts.admin')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">My Badges</h3>
        <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Badges</li>
                    <li class="breadcrumb-item active">List Badges</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif
<div class="row">
    <div id="recent-sales" class="col-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Badges List</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content mt-1">
                <div class="table-responsive" style="padding: 2%;">
                    <table class="table table-striped table-bordered zero-configuration">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Date of Award</th>
                                <th>Date of Expiry</th>
                                <th>Awarded By</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($badges as $badge)
                            <tr>
                                <td class="text-truncate">{{$badge->b_id}}</td>
                                <td class="text-truncate">{{$badge->name}}</td>
                                <td class="text-truncate">{{ \Illuminate\Support\Str::limit($badge->description, 40, $end='...') }}</td>
                                <td class="text-truncate"><img src="{{$badge->image}}"width="50"></td>
                                <td class="text-truncate">{{$badge->date_of_award}}</td>
                                <td class="text-truncate">{{$badge->date_of_expiry}}</td>
                                <td class="text-truncate">{{$badge->awarded_by}}</td>
                                <td class="text-truncate">
                                  @if($badge->status == 1)
                                    <button class="btn btn-success btn-sm">Active</button>
                                  @else
                                    <button class="btn btn-primary btn-sm">Deactive</button>
                                  @endif
                                </td>
                                <td align="center">
                                    <div class="fonticon-wrap">
                                        <a href="{{ route('badges.edit', $badge->b_id) }}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush