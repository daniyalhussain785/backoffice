@extends('layouts.admin')
@section('pageTitle', 'Dashboard')
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Dashboard</h3>
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Dashboard
                  </li>
                </ol>
              </div>
            </div>
        </div>
    </div>
    <div class="content-body">

      <div class="row">
           <div class="col-xl-4 col-lg-6 col-12">
                <a href="{{ url('supervisor') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info display-4">{{$personCounters[0]->supervisors}}</h3>
                                        <h6>All Supervisors</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-4 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning display-4">{{$personCounters[0]->kamay}}</h3>
                                        <h6>All Kamay</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-4 col-lg-6 col-12">
                <a href="{{ url('customer') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success display-4">{{$personCounters[0]->customers}}</h3>
                                        <h6>All Customers</h6>
                                    </div>
                                    <div>
                                        <i class="icon-user-follow success font-large-2 float-right"></i>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
              
        </div>

        <div class="row">
            
                  
                    
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$jobCounter[0]->all_jobs}}</h3>
                                        <h6>All Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('final-approval-kamay') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning">{{$jobCounter[0]->Unassigned}}</h3>
                                        <h6>Unassigned Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success">{{$jobCounter[0]->Assigned}}</h3>
                                        <h6>Assigned Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase success font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                 <a href="{{ url('kamayrequest') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="danger">{{$jobCounter[0]->Cancelled}}</h3>
                                        <h6>Cancelled Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>

                 <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$jobCounter[0]->InProgress}}</h3>
                                        <h6>In-Progress Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('final-approval-kamay') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning">{{$jobCounter[0]->Completed}}</h3>
                                        <h6>Completed Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <a href="{{ url('kamayview') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success">{{$jobCounter[0]->OnHold}}</h3>
                                        <h6>On-Hold</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase success font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
              <div class="col-xl-3 col-lg-6 col-12">
                 <a href="{{ url('kamayrequest') }}">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="danger">{{$schedule}}</h3>
                                        <h6>Scheduled Jobs</h6>
                                    </div>
                                    <div>
                                        <i class="icon-briefcase danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                    

                   
                 
          </div>

        <div class="row">
           <div class="col-sm-6">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Unassigned Jobs</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{ url('view-unassigned')}}" >View all</a></li>
                  </ul>
                </div>
                </div>
                <div class="card-content">
                  <div class="table-responsive">
                    <table id="recent-orders" class="table table-hover table-xl mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0">Job ID</th>
                          <th class="border-top-0">Customer Name</th>
                          <th class="border-top-0">Service</th>
                          <th class="border-top-0">Amount</th>
                          <th class="border-top-0">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($unAssignedJobs as $jobData)
                        <tr>
                          
                          <td class="text-truncate">{{$jobData->order_id}}</td>
                          <td class="text-truncate">{{$jobData->customername}}</td>
                          <td class="text-truncate">{{$jobData->service_name}}</td>
                          <td class="text-truncate">{{$jobData->total_amount}}</td>
                          <td><a class="btn btn-sm btn-danger box-shadow-2 round" href="{{ url('assigned-job-view', $jobData->order_id) }}" >Assign kama</a></td>
                        </tr>
                       @endforeach
                        
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">New Kamay Registration</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{ url('kamayrequest')}}" >View all</a></li>
                  </ul>
                </div>
                </div>
                <div class="card-content">
                  
                @foreach($newKamayReq as $newKamay)
                <div class="card-body pb-0">
                    <div class="row">
                        <div class="col-2">
                        <td class="text-truncate">
                              <!-- <img class="box-shadow-2" src="{{ asset('admin/img/app-1.png') }}" alt="avatar"> -->
                               <img src="{{ asset($newKamay->image) }}" alt="users view avatar" class="box-shadow-2" height="64" width="64">
                            
                          </td>
                        </div>
                        <div class="col-5 pl-2">
                            <h4>{{$newKamay->fullName}}</h4>
                            <h6 class="text-muted">{{$newKamay->phn_no}}</h6>
                            <h6 class="text-muted">{{$newKamay->city_name}}</h6>
                        </div>
                        <div class="col-5 text-right">
                            <h4>{{date("m-d h:i:s a", strtotime($newKamay->created_at))}}</h4>  
                        </div>
                    </div>
                </div>
                
                @endforeach
                </div>
              </div>
            </div>
        </div>

        <div class="row">

 

          <div id="recent-sales" class="col-12 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Panic Reported</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{ url('panic-reported')}}" >View all</a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content mt-1">
                <div class="table-responsive">
                  <table id="recent-orders" class="table table-hover table-xl mb-0">
                    <thead>
                      <tr>
                      <th class="border-top-0">Job ID</th>
                        <th class="border-top-0">Reported By</th>
                        <th class="border-top-0">Reporter Type</th>
                        
                        <th class="border-top-0">Contact</th>
                        <th class="border-top-0">Reason</th>
                        <th class="border-top-0">Job Date</th>
                        <th class="border-top-0">View Details</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($panicReported as $panic)
                      <tr>
                         <td class="text-truncate">{{$panic->pid}}</td>
                          <td class="text-truncate">{{$panic->fullName}}</td>
                          <td class="text-truncate">{{$panic->role_name}}</td>
                          <td class="text-truncate">{{$panic->phn_no}}</td>
                          <td class="text-truncate">{{$panic->panic_reason}}</td>
                          <td class="text-truncate">{{ date("d-m-Y", strtotime($panic->pdate)) }}</td>
                          <td>
                            <a href="{{ route('panic.show', $panic->pid) }}" class="btn btn-sm btn-outline-danger round" >View Details</a>
                          </td>
                        
                      </tr>
                      @endforeach
                  </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>
@endsection