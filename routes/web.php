<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('auth.login');
});







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Agent
Route::resource('agent','agentController');
//customer
Route::resource('customer','customerController');
//Supervisor
Route::resource('supervisor','supervisorController');
//City
Route::resource('city','cityController');
//Slider
Route::get('slider','SliderController@create')->name('slider.create');
Route::get('slider/all','SliderController@index')->name('slider.index');
Route::post('slider','SliderController@store')->name('slider.store');
Route::get('slider/{id}','SliderController@edit')->name('slider.edit');
Route::post('slider/{id}','SliderController@update')->name('slider.update');

//area
Route::resource('area','areaController');
//notification
Route::resource('notification','notificationController');
//services
Route::resource('services','servicesController');
//Sub services
Route::resource('subservices','subservicesController');
//FAQS
Route::resource('faqs','FaqsController');

Route::get('supervisor/assigned-kamay-to-supervisor/{id}','supervisorController@getAssignedKamayToSupervisor')->name('supervisor.assigned-kamay-to-supervisor');

//Badges
Route::resource('badges','BadgeController');

//Person Badges
Route::resource('assign-badges','PerBadgeController');
Route::post('get-badges-by-user','PerBadgeController@getBadgesByUser');

//Kamay Worker
Route::resource('kama','kamaController');
Route::get('/worker-growth-potential', 'kamaGrowthController@kamaGrowth')->name('worker-growth-potential');
Route::get('/kamayview', 'kamaController@kamayView')->name('kamayview');
Route::get('/final-approval-kamay', 'kamaController@getFinalApprovalKamay')->name('final-approval-kamay');
Route::get('/assigned-kamay', 'kamaController@getAssignedKamay')->name('assigned-kamay');
Route::get('/kamayrequest', 'kamaController@kamayRequest')->name('kamayrequest');
Route::post('get-area-by-city','kamaController@getArea');
Route::post('get-area-by-cityname','kamaController@getAreaByCityName');
Route::post('/assign-supervisor','kamaController@assignSupervisor')->name('assign-supervisor');
Route::post('/final-approval/{id}','kamaController@finalApproval')->name('final-approval');
Route::get('/view-approved-worker', 'kamaController@getApprovedWorker')->name('view-approved-worker');
Route::get('/assigned-supervisor-list/{kama_id}','kamaController@supervisorAssignedList')->name('assigned-supervisor-list');

//Jobs
Route::resource('jobs','jobsController');
Route::get('/view-inprogress', 'jobsController@getInprogress')->name('jobs.view-inprogress');
Route::get('/view-cancelled', 'jobsController@getCancelled')->name('jobs.view-cancelled');
Route::get('/view-unassigned', 'jobsController@getUnassigned')->name('jobs.view-unassigned');
Route::get('/view-completed', 'jobsController@getCompleted')->name('jobs.view-completed');
Route::get('/view-onhold', 'jobsController@getOnHold')->name('jobs.view-nohold');
Route::get('/view-scheduled', 'jobsController@getScheduled')->name('jobs.view-scheduled');
Route::get('/all-jobs', 'jobsController@getAllJobs')->name('jobs.all-jobs');
Route::get('/jobs-report', 'jobsController@getJobsReport')->name('jobs.jobs-report');
Route::post('/jobs-report-range', 'jobsController@getJobsReportRange')->name('jobs.jobs-report-range');
Route::get('/today-jobs', 'jobsController@getTodayJobs')->name('jobs.today-jobs');
// Route::post('seller/update_store_slider','API\SellerController@updateSlider');
Route::get('/assigned-job-view/{order_id}','jobsController@viewAssginedJobs')->name('jobs.assigned-job-view');
Route::get('/schedule-details/{order_id}','jobsController@getScheduleDetails')->name('jobs.schedule-details');
Route::post('/unassigned-job-submit','jobsController@unassignedJobSubmit')->name('unassigned-job-submit');
//Panic
Route::get('/panic-reported', 'panicController@getPanicReported')->name('panic.panic-reported');
Route::get('/panic-resolved', 'panicController@getPanicResolved')->name('panic.panic-resolved');
Route::resource('panic','panicController');

Route::get('/request/orders', 'RequestController@index')->name('admin.request.order');

Route::get('/payment', 'PaymentController@index')->name('admin.payment');
Route::post('/payment/{id}', 'PaymentController@update')->name('admin.payment.update');

Route::get('/fmc','notificationController@sendNotification');
Route::get('/send-notification/{test}/{id}','notificationController@checkSendNotification')->name('send-notification');

Route::get('/user/notf/show', 'notificationController@user_notf_show')->name('user-notf-show');
Route::get('/user/final/approval/show', 'notificationController@final_approval_notf_show')->name('final-approval-notf-show');
Route::get('/user/panic/show', 'notificationController@panic_notf_show')->name('panic-notf-show');

Route::resource('share','ShareController');

Route::post('/save-token', 'notificationController@saveToken')->name('save-token');

/* BULK MESSAGE */
Route::get('/bulk/message/', 'BulkMessageController@index')->name('bulk.message');
Route::post('/bulk/message/send', 'BulkMessageController@send')->name('bulk.message.send');
Route::post('/bulk/message/service', 'BulkMessageController@service')->name('bulk.message.with.service');

Route::post('get-user-by-role','BulkMessageController@getUserByRole');

/* SCHEDULE TIME COUNT */
Route::get('/schedule', 'ScheduleController@index')->name('schedule.index');
Route::post('/schedule/{id}', 'ScheduleController@update')->name('schedule.update');

Route::get('/no-service-charges', 'servicesController@noServiceCharges')->name('no.service.charges');
Route::post('/no-service-charges', 'servicesController@noServiceChargesUpdate')->name('no.service.charges.update');

//Update Status
Route::post('/job/status/updated/{id}', 'jobsController@statusUpdate')->name('update.status');