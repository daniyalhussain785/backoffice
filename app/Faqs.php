<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
    protected $table = 'faq_table';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'question','answer','role_id'];

}
