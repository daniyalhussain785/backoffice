<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personInfo extends Model
{
    protected $table = 'per_info';
    protected $primaryKey = 'regId';
    protected $fillable = ['regId', 'fullName', 'nickName', 'attachmeent', 'date_of_birth', 'place_of_birth', 'father_name', 'cnic', 'gender', 'driving_license_no', 'driving_license_expiry', 'ntn', 'hire_date', 'join_date', 'conf_date', 'salary', 'phn_no', 'city_id', 'terr_id', 's_id', 'password', 'email', 'image', 'pri_add', 'sec_add', 'pri_lat', 'sec_lat', 'pri_long', 'sec_long', 'eobi', 'verufied_by','callcenter_id', 'r_id', 'ref1_id', 'ref2_id', 'ref3_id', 'b_id', 'o_id', 'remarks', 'status', 'age', 'imei1', 'phn_reg_no', 'session_id', 'job_status', 'vehicle_no', 'rating_count', 'rating_number'];
    protected $attributes = ['rating_count' => null, 'rating_number' => null];


    public function user_services(){
        return $this->belongsToMany(
            Services::class,
            'user_services',
            'reg_id',
            's_id');
    }

    public function city(){
        return $this->hasOne('App\City', 'city_id', 'city_id');
    }

    public function territory(){
        return $this->hasOne('App\territory', 'terr_id', 'terr_id');
    }

    public function user_territory(){
        return $this->belongsToMany(
            territory::class,
            'kama_territory',
            'reg_id',
            'terr_id');
    }

    public function kamay_rating(){
        if($this->rating_count == 0){
            return 0;
        }else{
            return number_format((int)$this->rating_number/$this->rating_count);
        }
    }

    public function user_firebase(){
        return $this->hasOne('App\Users', 'regId', 'regId');
    }

    public function customer_order(){
        return $this->hasMany('App\Orders', 'regId', 'regId');
    }

    public function address(){
        return $this->hasMany('App\Address', 'regId', 'regId');
    }

    public function supervisor(){
        return $this->belongsTo('App\personInfo', 'assigned_supervisor', 'regId');
    }

    public function nooforders(){
        return $this->hasMany('App\Orders', 'assign_to', 'regId')->count();
    }
    
    public function totalamount(){
        return $this->hasMany('App\Orders', 'assign_to', 'regId')->sum('total_amount');
    }

    public function badges(){
        return $this->hasMany('App\PerBadges', 'p_id', 'regId');
    }
}
