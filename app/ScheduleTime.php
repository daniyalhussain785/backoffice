<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleTime extends Model
{
    protected $table = 'schedule_time';
    protected $primaryKey = 'id';
    protected $fillable = ['time'];

}
