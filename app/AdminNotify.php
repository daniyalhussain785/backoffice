<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminNotify extends Model
{
    protected $table = 'admin_notify';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'notify_status','user_id', 'seen', 'panic_id'];

    public function notify(){
        return $this->hasOne('App\NotifyStatus' , 'id', 'notify_status');
    }

    public function personInfo(){
        return $this->hasOne('App\personInfo' , 'regId', 'user_id');
    }

    public function panic(){
        return $this->hasOne('App\PanicReported' , 'id', 'panic_id');
    }

}
