<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class OrderShare extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'order_share';
        protected $primaryKey = 'id';
        protected $fillable = ['order_id', 'share_id', 'order_details_id', 'price', 'quantity'];

        public function share(){
            return $this->hasOne('App\Shares', 'id','share_id');
        }
        
    }