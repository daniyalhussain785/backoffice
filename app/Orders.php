<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class Orders extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'orders';
        protected $primaryKey = 'order_id';
        protected $fillable = ['order_id', 'regId', 's_id', 'total_amount', 'create_at', 'estimated_time', 'arrived_at', 'assign_to', 'assign_from', 'status', 'start_job_date', 'end_job_date', 'amount_received', 'payment_method', 'diffrence', 'latitude', 'longitude', 'remark_by_kama', 'remark_by_customer', 'rate_by_kama', 'rate_by_customer', 'full_address', 'request_time', 'custom_distance', 'is_schedule', 'schedule_date', 'old_status'];
        
        public function service(){
            return $this->hasOne('App\Services', 's_id','s_id');
        }
        
        public function request_orders(){
            return $this->hasMany('App\RequestOrder', 'order_id', 'order_id');
        }

        public function orderShare(){
            return $this->hasMany('App\OrderShareTotalAmount', 'order_id', 'order_id');
        }
        
        public function user(){
            return $this->hasOne('App\personInfo', 'regId','regId');
        }

        public function job_interest(){
            return $this->belongsToMany(
                personInfo::class,
                'job_interest',
                'order_id',
                'reg_id');
        }

        public function assign_person(){
            return $this->hasOne('App\personInfo', 'regId','assign_to');
        }

        public function get_total_price_by_customer() {
            return "working";
        }

        public function order_details(){
            return $this->hasMany('App\OrdersDetails', 'order_id', 'order_id');
        }

        public function panic(){
            return $this->hasMany('App\PanicReported', 'order_id','order_id');
        }

        public function interested(){
            return $this->hasMany('App\JobInterest', 'order_id', 'order_id');
        }

    }