<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobInterest extends Model
{
    protected $table = 'job_interest';
    protected $primaryKey = 'id';
    protected $fillable = ['order_id', 'reg_id'];

    public function kamay(){
        return $this->hasOne('App\personInfo', 'regId','reg_id');
    }

}
