<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallCenter extends Model
{
    protected $table = 'call_center_agent';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'user_name','email','role','device_token'];

}
