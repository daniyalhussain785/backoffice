<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifyStatus extends Model
{
    protected $table = 'notify_status';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name','title','body'];

}
