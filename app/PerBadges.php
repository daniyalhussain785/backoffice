<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerBadges extends Model
{
    protected $table = 'per_badges';
    protected $primaryKey = 'pb_id';
    protected $fillable = ['b_id', 'p_id','by_whom'];
    
    public function user(){
        return $this->hasOne('App\personInfo', 'regId', 'p_id');
    }

    public function badge(){
        return $this->hasOne('App\Badges', 'b_id', 'b_id');
    }

    public function callcenter(){
        return $this->hasOne('App\CallCenter', 'id', 'by_whom');
    }
}
