<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserServices extends Model
{
    protected $table = 'user_services';
    protected $fillable = ['s_id', 'reg_id'];

}
