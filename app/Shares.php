<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shares extends Model
{
    protected $table = 'shares';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'label','status'];

}
