<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class area extends Model
{
    protected $table = 'area_manage';
    protected $fillable = ['area_name','status'];
}
