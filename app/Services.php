<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';
    protected $primaryKey = 's_id';
    protected $fillable = ['s_id', 'parent_id','o_id','name','description','service_image','price','status', 'is_hourly'];

    public function users(){
        return $this->belongsToMany(
            personInfo::class,
                'user_services',
                's_id',
                'reg_id');
        }

    public function service_share(){
        return $this->hasMany(
            "App\ServicesShare",
                'service_id',
                's_id');
        }
    
    function main_service(){
        return $this->belongsTo('App\Services', 'parent_id');
    }
}
