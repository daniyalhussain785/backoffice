<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Badges;
use App\personInfo;
use DB;
use Validator;
use Auth;


class BadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $badges = Badges::all();
        return view('badges.index', compact('badges'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('badges.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {

        $request->validate([
            'name' =>  'required',
            'description' =>  'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
            'awarded_by' => 'required',
            'date_of_award' => 'required',
            'date_of_expiry' => 'required',
        ]);
        if ($request->hasFile('image')) { 
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/badges'), $imageName);
            $imageName = url('images/badges/'. $imageName);
        }else{
            $imageName = '';
        }

        $badges = new Badges();
        $badges->name = $request->input('name');
        $badges->description = $request->input('description');
        $badges->image = $imageName;
        $badges->awarded_by = $request->input('awarded_by');;
        $badges->date_of_award = $request->input('date_of_award');
        $badges->date_of_expiry = $request->input('date_of_expiry');
        $badges->status = $request->input('status');
        $badges->save();
        return redirect()->back()->with('success', 'Badges Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($b_id)
    {
        $data = Badges::find($b_id);
        return view('badges.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $b_id)
    {

       $request->validate([
            'name' =>  'required',
            'description' =>  'required',
            'status'    =>  'required',
            'awarded_by' => 'required',
            'date_of_award' => 'required',
            'date_of_expiry' => 'required',
        ]);

        $badges = Badges::find($b_id);
        $badges->name = $request->input('name');
        $badges->description = $request->input('description');
        $badges->status = $request->input('status');
        $badges->awarded_by = $request->input('awarded_by');;
        $badges->date_of_award = $request->input('date_of_award');
        $badges->date_of_expiry = $request->input('date_of_expiry');
        if ($request->hasFile('image')) {            
            if(File::exists($badges->image)) {
                File::delete($badges->image);
            }
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/badges'), $imageName);
            $imageName = url('images/badges/'. $imageName);
        }else{
            $imageName = $badges->image;
        }
        $badges->$imageName;
        $badges->save();
        return redirect()->back()->with('success', 'Badges Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}