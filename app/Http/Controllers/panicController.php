<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use Validator;
use Auth;
use App\PanicReported;


class panicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        //
    }
     public function getPanicReported()
    {

        $data = PanicReported::where('status', 2)->get();
        return view('panic.panic-reported',compact('data')); 
    }

     public function getPanicResolved()
    {
        $data = PanicReported::where('status', 1)->get();
        return view('panic.panic-reported',compact('data')); 

        // $data = DB::table('panic_reported')
        //             ->join('per_info', 'panic_reported.reg_id', 'per_info.regId')
        //             ->join('roles', 'per_info.r_id', 'roles.r_id')
        //             ->select('per_info.*','panic_reported.id as pid','panic_reported.order_id as job_id','panic_reported.status as pstatus','panic_reported.reason as panic_reason','panic_reported.status_name as panic_name','panic_reported.created_at as pdate','roles.role_name')
        //             ->where('panic_reported.status',1)
        //             ->get();
                    
        //     return view('panic.panic-resolved',compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = PanicReported::find($id);
        //return $data;exit();
        return view('panic.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request,$id)
    {
        
         
         $request->validate([
            'panic_reason'    =>  'required',
            'status'     =>  'required',
            
        ]);

         $PanicCreatedBy = $request->input('id');
            
            $status = $request->input('status');
             //return $request; exit();
            if($status == 'Resolved'){
                $cstatus = 1;
               
            }else if($status == 'No action required'){
                $cstatus = 1;
            }else if($status == 'False alarm'){
                $cstatus = 1;
            }else if($status == 'Helpline activated'){
                $cstatus = 2;
            }else if($status == 'un resolved'){
                $cstatus = 2;
            }
             

            $form_data = array(
                //'reason'        => $request->input('panic_reason'),
                'status'        => $cstatus,
                'status_name'   => $request->input('status'),
                'resolved_by'   => Auth::user()->id
                );
            
        $perinfo = DB::table('panic_reported')->where('id',$id)->update($form_data);
        
       
        return redirect('panic-reported')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
