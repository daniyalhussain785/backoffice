<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PerBadges;
use App\Badges;
use App\personInfo;
use DB;
use Validator;
use Auth;


class PerBadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $badges = Badges::all();
        $perbadges = PerBadges::groupBy('p_id')->get();
        $user = personInfo::whereIn('r_id', [1,2])->where('status', 1)->get();
        return view('per-badges.index', compact('badges', 'user', 'perbadges'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('badges.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $request->validate([
            'p_id' =>  'required',
            'b_id' =>  'required'
        ]);
        $b_id = $request->input('b_id');
        $p_id = $request->input('p_id');
        $not_in_per = PerBadges::where('p_id', $p_id)->whereNotIn('b_id' ,$b_id)->delete();
        foreach($b_id as $b_ids){
            PerBadges::updateOrCreate(
                ['p_id' => $p_id, 'b_id' => $b_ids],
                ['b_id' => $b_ids, 'by_whom' => auth()->user()->id]
            );
        }
        return redirect()->back()->with('success', 'Assign Badges Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($b_id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $b_id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBadgesByUser(Request $request){
        $id = $request->input('user_id');
        $perbadges = PerBadges::where('p_id', $id)->get();
        return response()->json(array('data'=> $perbadges, 'status' => true), 200);
    }




}