<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\personInfo;
use App\ScheduleTime;
use Validator;


class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct(){
    //     $this->middleware('auth');
    // } 

 


    public function index(){
        $schedule = ScheduleTime::find(1);
        return view('schedule.index', compact('schedule'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'time'    =>  'required',
        ]);
        $schedule = ScheduleTime::find($id);
        $schedule->time = $request->input('time');
        $schedule->save();
        return redirect()->back()->with('message', 'Data updated successfully.');
    }
    
}
 
