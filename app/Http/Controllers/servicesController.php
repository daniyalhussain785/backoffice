<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\Services;
use DB;
use Validator;
use Auth;
use File;
use App\NoServiceCharges;

class servicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $data = Services::where([['parent_id', '=', 0]])->get(['s_id', 'name', 'service_image', 'slider_image', 'description', 'status', 'price', 'is_hourly']);
        return view('services.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {

        $request->validate([
            'services_name'    =>  'required',
            'status'         =>  'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',      
        ]);
        $is_hourly = 0;
        
        if($request->input('is_hourly') == "on"){
            $is_hourly = 1;
        }

        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images/services'), $imageName);
        $imageName = url('images/services/'. $imageName);
        $userInfo = new Services([
            'name'          => $request->input('services_name'),
            'status'        => $request->input('status'),
            'service_image' => $imageName, 
            'is_hourly' => $is_hourly
        ]);
        $userInfo->save();
        return redirect('services')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Services::select('*')->where('s_id',$id)->first();
        return view('services.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'services_name'    =>  'required',
            'status'         =>  'required'
        ]);
        $old_image = Services::where('s_id', $id)->first();
        if ($request->hasFile('image')) {
            if(File::exists($old_image->service_image)) {
                File::delete($old_image->service_image);
            }
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/services'), $imageName);
            $imageName = url('images/services/'. $imageName);
        }else{
            $imageName = $old_image->service_image;
        }
        $is_hourly = 0;
        
        if($request->input('is_hourly') == "on"){
            $is_hourly = 1;
        }
        $form_data = array(
            'name'           => $request->input('services_name'),
            'status'         => $request->input('status'),
            'service_image'  => $imageName,
            'is_hourly' => $is_hourly
        );
        $perinfo = Services::where('s_id',$id)->update($form_data);
        return redirect('services/'.$id.'/edit')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAssignedKamayToservices($id)
    {

        $data = DB::table('kamay_list')
                ->join('per_info as p2', 'kamay_list.assigned_services', 'p2.regId')
                ->select('kamay_list.*','p2.fullName as servicesName','p2.status as cstatus')->where('p2.regId',$id)
                ->get();

                 return view('services.assigned-kamay-to-services', compact('data'));
    }

    public function noServiceCharges(){
        $noServiceCharges = NoServiceCharges::all();
        $service = Services::where('s_id', 0)->first();
        return view('no-service-charges.index', compact('noServiceCharges', 'service'));
    }
    
    public function noServiceChargesUpdate(Request $request){
        $request->validate([
            'service'    =>  'required',   
        ]);
        $total_value = 0; 
        $total_amount = $request->input('total_amount');
        $service = $request->input('service');
        foreach($service as $key => $value){
            $total_value += $value;
        }
        if($total_value != 100){
            return redirect()->back()->with('error', 'Percentage is not Equal to 100');   
        }

        foreach($service as $key => $value){
            $price = ($value/100) * $total_amount;
            $noservice_update =  NoServiceCharges::where('share_id', $key)->first();
            $noservice_update->percentage = $value;
            $noservice_update->price = $price;
            $noservice_update->save();
        }

        return redirect()->back()->with('success', 'Data updated successfully.');

    }



}
