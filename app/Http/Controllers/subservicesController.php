<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\Services;
use DB;
use Validator;
use Auth;
use File;
use App\Shares;
use App\ServicesShare;


class subservicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $data = Services::with("service_share")->where('parent_id', '!=', 0)->get();
        return view('subservices.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Services::where('parent_id', 0)->where('s_id', '!=', 1)->get();
        $share_view = Shares::where('status', 1)->get();
        return view('subservices.create',compact('categories', 'share_view'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $share_data = array();;
        $share_id = $request->input('share_id');
        $share_value = $request->input('share_value');
        $share_key = $request->input('share_key');
        $total_share = 0;
        $services_price = $request->input('services_price');
        foreach($share_id as $key => $value){
            // $share_key[$key]['key'] (1 for Price || 2 for Percentage)
            $share_actual_price = 0;
            if($share_key[$key]['key'] == 2){
                $total_share += ($services_price/100) * $share_value[$key]['price'];
                $share_actual_price = ($services_price/100) * $share_value[$key]['price'];
            }else{
                $total_share += $share_value[$key]['price']; 
                $share_actual_price = $share_value[$key]['price'];
            }
            $share_data[$key] = array(
                                    'share_id' => $key,
                                    'share_price' => $share_value[$key]['price'],
                                    'share_key' => $share_key[$key]['key'],
                                    'share_actual_price' => $share_actual_price
                                );
            
        }

        $validator = Validator::make($request->all(), [
            'services_name'    =>  'required',
            'main_service_id'    =>  'required',
            'status'         =>  'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'services_price' => 'required'
           
        ]);
        if($total_share != $services_price){
            $validator->errors()->add('Share Price', 'Share price are not equal to the Service Price');
        }
        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }

        if(File::exists($request->image)) {
                $imageName = time().'.'.$request->image->extension();  
                $request->image->move(public_path('images/services'), $imageName);  
                $imageName = url('images/services/'. $imageName);
        }else{
            $imageName = '';
        }
        
        $userInfo = new Services([
            'parent_id'                  => $request->input('main_service_id'),
            'name'                       => $request->input('services_name'),
            'price'                      => $request->input('services_price'),
            'status'                     => $request->input('status'),
            'service_image'              => $imageName,
        ]);
        $userInfo->save();

        foreach($share_data as $key => $value){
            $services_share = new ServicesShare();
            $services_share->service_id = $userInfo->s_id;
            $services_share->share_id = $value['share_id'];
            $services_share->share_price  = $value['share_price'];
            $services_share->share_key  = $value['share_key'];
            $services_share->share_actual_price  = $value['share_actual_price'];
            $services_share->save();
        }

        return response()->json(['message' => 'Data Added successfully', 'data' => $request->input(), 'status' => true]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $categories = Services::where('parent_id', 0)->where('s_id', '!=', 1)->get();
        $data = Services::where('s_id', $id)->first();
        $share_view = Shares::where('status', 1)->get();
        return view('subservices.edit', compact('data','categories','share_view'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $share_data = array();;
        $share_id = $request->input('share_id');
        $share_value = $request->input('share_value');
        $share_key = $request->input('share_key');
        $total_share = 0;
        $services_price = $request->input('services_price');
        foreach($share_id as $key => $value){
            // $share_key[$key]['key'] (1 for Price || 2 for Percentage)
            $share_actual_price = 0;
            if($share_key[$key]['key'] == 2){
                $total_share += ($services_price/100) * $share_value[$key]['price'];
                $share_actual_price = ($services_price/100) * $share_value[$key]['price'];
            }else{
                $total_share += $share_value[$key]['price']; 
                $share_actual_price = $share_value[$key]['price'];
            }
            $share_data[$key] = array(
                                    'share_id' => $key,
                                    'share_price' => $share_value[$key]['price'],
                                    'share_key' => $share_key[$key]['key'],
                                    'share_actual_price' => $share_actual_price
                                );
            
        }
        $validator = Validator::make($request->all(), [
            'services_name'    =>  'required',
            'main_service_id'    =>  'required',
            'status'         =>  'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'services_price' => 'required'
           
        ]);

        if($total_share != $services_price){
            $validator->errors()->add('Share Price', 'Share price are not equal to the Service Price');
        }
        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }


        $old_image = Services::where('s_id', $id)->first();
        if ($request->hasFile('image')) {
            if(File::exists($old_image->service_image)) {
                File::delete($old_image->service_image);
            }
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/services'), $imageName);
            $imageName = url('images/services/'. $imageName);
        }else{
            $imageName = $old_image->service_image;
        }

        $form_data = array(
            'parent_id'                  => $request->input('main_service_id'),
            'name'                       => $request->input('services_name'),
            'price'                      => $request->input('services_price'),
            'status'                     => $request->input('status'),
            'service_image'              => $imageName
        );

        $perinfo = Services::where('s_id',$id)->update($form_data);

        foreach($share_data as $key => $value){
            ServicesShare::updateOrCreate(
                ['share_id' => $key, 'service_id' => $id],
                ['share_price' => $value['share_price'],
                 'share_key' => $value['share_key'],
                 'share_actual_price' => $value['share_actual_price']]
            ); 
        }
        return response()->json(['message' => 'Data Updated successfully', 'data' => $request->input(), 'status' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAssignedKamayToservices($id)
    {

        $data = DB::table('kamay_list')
                ->join('per_info as p2', 'kamay_list.assigned_services', 'p2.regId')
                ->select('kamay_list.*','p2.fullName as servicesName','p2.status as cstatus')->where('p2.regId',$id)
                ->get();

                 return view('subservices.assigned-kamay-to-services', compact('data'));
    }



}


