<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use Validator;
use Auth;
use App\Faqs;


class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $faqs = Faqs::all();
        return view('faqs.index', compact('faqs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('faqs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {

        
        $request->validate([
            'question'    =>  'required',
            'answer'    =>  'required',
            'role_id' => 'required'
           
        ]);

        $faqs = new Faqs();
        $faqs->question = $request->input('question');
        $faqs->answer = $request->input('answer');
        $faqs->role_id = $request->input('role_id');
        $faqs->save();
        return redirect()->back()->with('success', 'Faqs Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $data = Faqs::find($id);
            return view('faqs.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

         
        $request->validate([
            'question'    =>  'required',
            'answer'    =>  'required',
            'role_id' => 'required'
           
        ]);
        $faqs = Faqs::find($id);
        $faqs->question = $request->input('question');
        $faqs->answer = $request->input('answer');
        $faqs->role_id = $request->input('role_id');
        $faqs->save();
        return redirect()->back()->with('success', 'Faqs Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}