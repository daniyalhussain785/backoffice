<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\personInfo;
use App\Users;
use App\Services;
use Validator;


class BulkMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct(){
    //     $this->middleware('auth');
    // } 

 


    public function index(){
        $person = personInfo::where('status', 1)->where('r_id', 1)->get();
        $service = Services::where('parent_id', 0)->where('s_id', '!=', 1)->where('status', 1)->get();
        return view('bulkmessage.index', compact('person', 'service'));
    }

    public function service(Request $request){
        $service = $request->input('service');
        $audience = $request->input('audience');
        $person = personInfo::where('status', 1)
                    ->where('r_id', $audience)
                    ->where(function($query) use($service) {
                        foreach($service as $services) {
                            $query->orWhere('s_id', 'like', "%$services%");
                        };
                    })
                    ->get();
        return response()->json(array('data'=> $person, 'status' => true), 200);
    }

    public function send(Request $request){
        $validator = '';
        $validator = Validator::make($request->all(), [
                        'type'    =>  'required',
                        'audience'     =>  'required',
                        'users_id' => 'required',
                        'message' => 'required',
                    ]);
        $type = $request->input('type');
        $users_id = $request->input('users_id');
        $title = $request->input('title');
        $message = $request->input('message');
        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }

        if($type == 0){
            $this->sendBulkMessageNotification($users_id, $title, $message);
            return response()->json(['message' => 'Bulk Push Notification Send Successfully', 'data' => null, 'status' => true]);
        }else{
            $this->sendBulkMessage($users_id, $message);
            return response()->json(['message' => 'Bulk SMS Message Send Successfully', 'data' => null, 'status' => true]);
        }
        
    }

    public function sendBulkMessageNotification($users_id, $title, $message){
        for($i = 0; $i < count($users_id); $i++){
            $users = Users::where('regId', $users_id[$i])->first();
            if($users->firebase_token != null){
                $this->pushNotification($users->firebase_token, 'backoffice_message', $title, $message);
            }
        }
    }

    public function pushNotification($token, $flag, $title, $body){
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $data = array(
            "flag" => $flag,
            "title" => $title,
            "body" => $body
        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1',
            'flag' => $flag,
        );
        $arrayToSend = array(
            'to' => $token,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        return true;
    }

    public function sendBulkMessage($users_id, $message){
        for($i = 0; $i < count($users_id); $i++){
            $users = personInfo::where('regId', $users_id[$i])->first();
            if($users->phn_no != null){
                $string = preg_replace('/\s+/', '%20', $message);
                $response = file_get_contents("https://connect.jazzcmt.com/sendsms_url.html?Username=03060465709&Password=Hta123456&From=HTA&To=".$users->phn_no."&Message=".$string."");
            }
        }
        return true;
    }

    public function getUserByRole(Request $request){
        $r_id = $request->input('r_id');
        $service = $request->input('service');
        if($r_id != 3){
            $person = personInfo::where('status', 1)
                        ->where('r_id', $r_id)
                        ->where(function($query) use($service) {
                            foreach($service as $services) {
                                $query->orWhere('s_id', 'like', "%$services%");
                            };
                        })
                        ->get();
        }else{
            $person = personInfo::where('status', 1)->where('r_id', $r_id)->get();
        }
        return response()->json(array('data'=> $person, 'status' => true), 200);
    }

    public function edit($id){
        
    }

    public function saveToken(Request $request){
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        
    }
    
}
 
