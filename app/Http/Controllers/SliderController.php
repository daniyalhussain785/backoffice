<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\personInfo;
use DB;
use Validator;
use Auth;
use File;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('slider.create');
    }

    public function index()
    {
        $slider = Slider::all();
        return view('slider.index', compact('slider'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = '';
        $validator = Validator::make($request->all(), [
                        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                        'title' =>  'required',
                        'status' =>  'required'
                    ]);
        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }
        $title = $request->input('title');
        $status = $request->input('status');
        $slider = new Slider();
        $slider->title = $title;
        $slider->status = $status;
        $tempFile = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images/slider'), $tempFile);
        $imageName = url('images/slider/'. $tempFile);
        $slider->image = $imageName;
        $slider->local_path = 'images/slider/'. $tempFile;
        $slider->save();
        return response()->json(['message' => 'Slider Added Successfully', 'data' => $request->input(), 'status' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = '';
        $validator = Validator::make($request->all(), [
                        'title' =>  'required',
                        'status' =>  'required'
                    ]);
        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }

        $title = $request->input('title');
        $status = $request->input('status');
        $slider = Slider::find($id);
        $slider->title = $title;
        $slider->status = $status;
        if ($request->hasFile('image')) {
            if(File::exists($slider->local_path)) {
                File::delete($slider->local_path);
            }
            $tempFile = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/slider'), $tempFile);
            $imageName = url('images/slider/'. $tempFile);
            $slider->local_path = 'images/slider/'. $tempFile;
            $slider->image = $imageName;
        }
        
        $slider->save();
        return response()->json(['message' => 'Slider Updated Successfully', 'data' => $request->input(), 'status' => true]);
        
    }

}