<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use File;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Services;
use App\Users;
use App\City;
use App\KamaTerritory;
use App\notification;
use App\AdminNotify;



class notificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct(){
    //     $this->middleware('auth');
    // } 

 


    public function index(){
        $data = notification::all();   
        return view('notification.index', compact('data'));
    }

    public function edit($id){
        $data = notification::where('id', $id)->first();
        return view('notification.edit', compact('data'));
    }

    public function saveToken(Request $request){
        DB::table('call_center_agent')
            ->where('id', 1)
            ->update(
                ['device_token' => $request->token]
            );
        return response()->json(['token saved successfully.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'title'    =>  'required',
            'body' => 'required'
        ]);
            
        $area = notification::find($id);
        $area->title = $request->input('title');
        $area->body = $request->input('body');
        $area->status = 0;
        $area->save();
        return redirect('notification/'.$id.'/edit')->with('success', 'Data updated successfully.');
    }

    public function checkSendNotification(Request $request, $body, $id){
        
        $current_user = DB::table('call_center_agent')
                            ->where('id', 1)
                            ->first();
        $token = $current_user->device_token;
        $from = "AAAAtQi7U6A:APA91bFjY5HQPCgz-rnIRTJq1-2HMciPo78lF7_US3hquZVoDOGyO4Ov5qvlN5-jVjxtUvX_hiCQPnW845bJR1OQd5jgbVAvD-09EIDNEq6SzXdNXdEJ3vOP7vRqD1p9SRx76Pb0fo5A";
        if($body == "new_kama"){
            $last_kama = DB::table('per_info')->where('regId', $id)->first();
            $title = "Kamay";
            $body = "New kama register (".$last_kama->fullName.") successfully review his profile";
            $click_action = route('kama.show', ['kama' => $last_kama->regId]);
        }else if($body == "kama_panic"){
            $last_panic = DB::table('panic_reported')->where('id', $id)->first();
            $panic_name = DB::table('per_info')->where('regId', $last_panic->reg_id)->first();
            $title = "Kamay";
            $body = "Panic Reported By Kama, ( Order ID: ".$last_panic->order_id. " )";
            $click_action = route('panic.show', $last_panic->id);
        }else if($body == "customer_panic"){
            $last_panic = DB::table('panic_reported')->where('id', $id)->first();
            $panic_name = DB::table('per_info')->where('regId', $last_panic->reg_id)->first();
            $title = "Kamay";
            $body = "Panic Reported By Customer, ( Order ID: ".$last_panic->order_id. " )";
            $click_action = route('panic.show', $last_panic->id);
        }else if($body == "callcenter"){
            $last_order = DB::table('orders')->where('order_id', $id)->first();
            $title = "Kamay";
            $body = "Unassigned Job On Your Backoffice, ( Job ID: ".$last_order->order_id. " )";
            $click_action = route('jobs.assigned-job-view', $last_order->order_id);
        }else if($body == "final_approved_supervisor"){
            $last_kama = DB::table('per_info')->where('r_id', 1)->where('regId', $id)->first();
            $title = "Kamay";
            $body = "Supervisor Final Approval, ( Job ID: ".$last_kama->regId. " )";
            $click_action = route('kama.show', ['kama' => $last_kama->regId]);
        }
        
        $msg = array(
                'body'  => $body,
                'title' => $title,
                'icon'  => url('images/Kamay-Logo-02.png'),/*Default Icon*/
                'sound' => 'mySound', /*Default sound*/
                "click_action" => $click_action
              );

        $fields = array(
                    'to'        => $token,
                    'notification'  => $msg
                );

        $headers = array(
                    'Authorization: key=' . $from,
                    'Content-Type: application/json'
                );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        dd($result);
        curl_close( $ch );
    }

    public function user_notf_show(){
        $datas = AdminNotify::whereIn('notify_status', [1,2])->orderBy('id', 'desc')->take(3)->get();
        $count = AdminNotify::whereIn('notify_status', [1,2])->where('seen', 0)->count();
        DB::table('admin_notify')
            ->whereIn('notify_status', [1,2])
            ->update(['seen' => 1]);
        $total_notify_count = AdminNotify::whereIn('notify_status', [1,2])->count();
        return view('inc.notification',compact('datas', 'count', 'total_notify_count'));
    }

    public function final_approval_notf_show(){
        $datas = AdminNotify::where('notify_status', 4)->orderBy('id', 'desc')->take(3)->get();
        $count = AdminNotify::where('notify_status', 4)->where('seen', 0)->count();
        DB::table('admin_notify')
            ->where('notify_status', 4)
            ->update(['seen' => 1]);
        $total_notify_count = AdminNotify::where('notify_status', 4)->count();
        return view('inc.final_approved_notification',compact('datas', 'count', 'total_notify_count'));
    }

    public function panic_notf_show(){
        $datas = AdminNotify::where('notify_status', 3)->orderBy('id', 'desc')->take(3)->get();
        $count = AdminNotify::where('notify_status', 3)->where('seen', 0)->count();
        DB::table('admin_notify')
            ->where('notify_status', 3)
            ->update(['seen' => 1]);
        $total_notify_count = AdminNotify::where('notify_status', 3)->count();
        return view('inc.panic_notification',compact('datas', 'count', 'total_notify_count'));
    }
    
}
 
