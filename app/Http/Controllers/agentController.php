<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\territory;
use App\personInfo;
use DB;
use Validator;
use Auth;
class agentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $data = db::table('call_center_agent')->select('*')->get();
                    

                 return view('agent.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         $cityData = DB::table('city')->select('city_id','name')->get();
        

        return view('agent.create', compact('cityData'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $request->validate([
            'name'      =>  'required',
            'email'     => 'required|string|email|max:255',
            'password'  => 'required|string|min:8|confirmed',
            'role_name'  => 'required',
           
        ]);
        //return $request;exit();
        DB::table('call_center_agent')->insert([
                'name'              => $request->input('name'),
                'email'                 => $request->input('email'),
                'role'                  => $request->input('role_name'),
                'password'              => Hash::make($request->input('password'))
        ]);

        

       
        return redirect('agent')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = db::table('call_center_agent')->select('*')->where('id',$id)->get();

            return view('agent.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $request->validate([
            'name'      =>  'required',
            'email'     => 'required|string|email|max:255',
            'role_name'  => 'required',
           
        ]);

            
            
        $form_data = array(
               'name'                   => $request->input('name'),
                'email'                 => $request->input('email'),
                'role'                  => $request->input('role_name'),
                'password'              => Hash::make($request->input('password'))
        );

        $perinfo = db::table('call_center_agent')->where('id',$id)->update($form_data);
        
       
        return redirect('agent')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
