<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\KamaTerritory;
use DB;
use File;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Services;
use App\Users;
use App\City;
use App\Address;



class kamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    } 


    public function index()
    {
        return view('kama.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cityData = DB::table('city')->select('city_id','name')->get();
        $expertise = Services::where('parent_id', 0)->where('s_id', '!=', 1)->get();
        return view('kama.create', compact('cityData','expertise'));
    }

    public function kamayView()
    {

        // if ($request->ajax()) {
        //     $data = personInfo::select(DB::raw('regId', 'fullName', 'nickName'))->get();
        //     return Datatables::of($data)
        //             ->addIndexColumn()
        //             ->addColumn('action', function($row){
   
        //                    $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
     
        //                     return $btn;
        //             })
        //             ->rawColumns(['action'])
        //             ->make(true);
        // }
      
        // return view('kama.viewkamay');
         
         // $data = DB::table('kamay_list')
         //            ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
         //            ->select('kamay_list.*','p2.fullName as supervisorName')
         //            ->get();

         // $ss = DB::table('per_info')
         //        ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
         //        ->join('services', 'user_services.s_id', '=', 'services.s_id')
         //        ->join('city', 'per_info.city_id', '=', 'city.city_id')
         //        ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
         //        ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'per_info.email','per_info.vehicle_no','per_info.cnic', 'per_info.age', 'per_info.image','per_info.session_id','per_info.r_id',  DB::raw('group_concat(services.name) as expertise'), 'city.name as city', 'territory.name as territory_name')->get();

        $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->join('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'))
                    ->groupBy('kamay_list.regId')
                    
                    ->get();

                 
        
         // dd($data); exit(); 

         $kamaCounter = $this->getKamayCounter();
        
        return view('kama.viewkamay', compact('data','kamaCounter'));

        
    }


    public function getAssignedKamay()
    {

        

        // $data = DB::table('kamay_list')

        //             ->join('city', 'kamay_list.city_id', '=', 'city.city_id')
        //             ->join('per_info as p2', 'kamay_list.assigned_supervisor', '=', 'p2.regId')

        //             ->select('kamay_list.*','city.name','p2.fullName as supervisorName')
        //             ->where('kamay_list.status','pending')
        //             ->whereNull('kamay_list.verufied_by')->get();

        $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->leftjoin('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->leftjoin('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'), 'p2.assigned_supervisor_date')
                    ->groupBy('kamay_list.regId')
                    ->where('kamay_list.status','pending')
                    ->whereNull('kamay_list.verufied_by')
                    ->get();
        
        if (count($data) != 0) {
            foreach($data as $per_review){
                    $review = DB::table('review_checklist_status')
                  ->leftjoin('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
                                    ->select('status_name')
                                    ->where('review_checklist_status.reg_id', $per_review->regId)
                                    ->where('review_checklist_status.review_checklist_id',10)
                                    ->first();
                        if ($review != null) {
                        $per_review->application_status = $review->status_name;
                        }else{
                          $per_review->application_status = 0;
                        }

                        $per_review->diff_time = Carbon::parse($per_review->assigned_supervisor_date)->diffForHumans();

                        $supervisorName = DB::table('per_info')
                        ->join('per_info as p2', 'per_info.assigned_supervisor', 'p2.regId')
                        ->select('p2.fullName as supervisorName')
                        ->where('per_info.regId',$per_review->regId)
                        ->first();
                        
                        if ($supervisorName != null) {
                            $per_review->supervisorNames = $supervisorName->supervisorName;
                            }else{
                              $per_review->supervisorNames = 0;
                            }
                            
                        // dump($supervisorNames);
                        // die();
                        $assign_date = personInfo::where('regId', $per_review->regId)->first();
                        $per_review->assign_date = Carbon::parse($assign_date->assigned_supervisor_date)->diffForHumans();
                    }
                    // $var =  $data[0]->application_status[0]->status_name;
                    // dump($var);
                    // die();

        }

        $kamaCounter = $this->getKamayCounter();
       
          
                    


        return view('kama.assigned-kamay', compact('data','kamaCounter'));

        
    }

     public function getFinalApprovalKamay()
    {

        

        // $data = DB::table('kamay_list')

        //             ->join('city', 'kamay_list.city_id', '=', 'city.city_id')
        //             ->join('per_info as p2', 'kamay_list.assigned_supervisor', '=', 'p2.regId')

        //             ->select('kamay_list.*','city.name','p2.fullName as supervisorName')
        //             ->where('kamay_list.status','pending')
        //             ->whereNotNull('kamay_list.verufied_by')->get();

       $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->leftjoin('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->leftjoin('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'))
                    ->groupBy('kamay_list.regId')
                    ->where('kamay_list.status','pending')
                    ->whereNotNull('kamay_list.verufied_by')
                    ->get();
        
        
        if (count($data) != 0) {
            foreach($data as $per_review){
                   

                        $supervisorName = DB::table('per_info')
                        ->join('per_info as p2', 'per_info.assigned_supervisor', 'p2.regId')
                        ->select('p2.fullName as supervisorName')
                        ->where('per_info.regId',$per_review->regId)
                        ->first();
                        
                        if ($supervisorName != null) {
                            $per_review->supervisorNames = $supervisorName->supervisorName;
                            }else{
                              $per_review->supervisorNames = 0;
                            }
                            
                        // dump($supervisorNames);
                        // die();
                        
                    }
                    // $var =  $data[0]->application_status[0]->status_name;
                    // dump($var);
                    // die();

        }
        
        // $supervisorNames =  DB::table('kamay_list')

        //             ->join('per_info as p2', 'kamay_list.assigned_supervisor', '=', 'p2.regId')
        //             ->select('p2.fullName as supervisorName')
        //             ->where('kamay_list.status','pending')
        //             ->whereNull('kamay_list.verufied_by')->get();
         //dump($data);die();
         $kamaCounter = $this->getKamayCounter();
        
         
        return view('kama.final-approval-kamay', compact('data','kamaCounter'));

        
    }

   public function getApprovedWorker()
    {

        


        $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->join('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'))
                    ->where('kamay_list.status','approved')
                    ->groupBy('kamay_list.regId')
                    
                    ->get();
         
         $kamaCounter = $this->getKamayCounter();
        
         
        return view('kama.view-approved-worker', compact('data','kamaCounter'));

        
    }

    //get new kamay request

    public function kamayRequest()
    {

        $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->leftjoin('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->leftjoin('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'))
                    ->groupBy('kamay_list.regId')
                    ->where('kamay_list.status','new kamay')
                    ->get();

          
        if (count($data) != 0) {
            foreach($data as $getArea){

              $areaKama = DB::table('per_info')
                ->join('city', 'per_info.city_id', '=', 'city.city_id')
                ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
                ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.image','per_info.session_id','per_info.r_id', 'city.name as city', 'territory.name as territory_name')
                ->where('per_info.r_id', '=', 1)
                ->where('per_info.regId',$getArea->regId)
                ->first();
                if ($areaKama != null) {
                    $getArea->territory_name = $areaKama->territory_name;
                }

            }
        }
        $getSupervisor = DB::table('per_info')
                            ->join('city', 'per_info.city_id', '=', 'city.city_id')
                            ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
                            ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'per_info.email','per_info.vehicle_no','per_info.cnic', 'per_info.age','per_info.pri_add', 'per_info.image','per_info.session_id','per_info.r_id', 'city.name as city', 'territory.name as territory_name')
                            ->where('r_id', '=', 2)
                            ->get();
      
      if (count($getSupervisor) != 0) {
            foreach($getSupervisor as $expertise){
                      
                $getExpertise = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select(DB::raw('group_concat(services.name) as expertise'))
                    ->where('r_id', '=', 2)
                    ->where('per_info.regId',$expertise->regId)
                    ->groupBy('per_info.regId')
                    ->first();
                    
                    if ($getExpertise != null) {
                        $expertise->supervisor_expertise = $getExpertise->expertise;
                    }else{
                        $expertise->supervisor_expertise = '';
                    }

                      //getRatings
                      $kamayCount = personInfo::select('per_info *')
                          ->where('assigned_supervisor', $expertise->regId)
                          ->where('r_id', 1)
                          ->count();
                        $ratings = 0;
                        $expertise->worker_count = $kamayCount;
                        $expertise->supervisor_ratings = $ratings;
                       if ($kamayCount != 0) {
                            $rating = DB::table('orders')
                                              ->join('services', 'services.s_id', '=', 'orders.s_id')
                                              ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                              ->select(
                                                  DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                                  DB::raw('count(orders.order_id) as total_customers')
                                                 
                                              )
                                              ->where('per_info.assigned_supervisor',$expertise->regId)
                                              ->first();

                                if ($rating->total_customer_rating != null) {
                                  $ratings = $rating->total_customer_rating / $rating->total_customers;
                                }else{
                                  $ratings = 0;
                                }
                                $expertise->supervisor_ratings = $ratings;
                        }  
                        
            }
             
                    

        }
         
        $kamaCounter = $this->getKamayCounter();
        return view('kama.newkamayrequest', compact('data','kamaCounter','getSupervisor'));
        
    }

    public function supervisorAssignedList($kama_id)
    {
      
          $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->leftjoin('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->leftjoin('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'))
                    ->groupBy('kamay_list.regId')
                    ->where('kamay_list.status','new kamay')
                    ->where('kamay_list.regId', $kama_id)
                    ->get();

          if (count($data) != 0) {
            foreach($data as $getArea){

              $areaKama = DB::table('per_info')
                ->join('city', 'per_info.city_id', '=', 'city.city_id')
                ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
                ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.image','per_info.session_id','per_info.r_id', 'city.name as city', 'territory.name as territory_name')
                ->where('per_info.r_id', '=', 1)
                ->where('per_info.regId',$getArea->regId)
                ->first();
                if ($areaKama != null) {
                    $getArea->territory_name = $areaKama->territory_name;
                }

            }
          }

        $getSupervisor = DB::table('per_info')
            ->join('city', 'per_info.city_id', '=', 'city.city_id')
            ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
            ->select('per_info.s_id', 'per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'per_info.email','per_info.vehicle_no','per_info.cnic', 'per_info.age','per_info.pri_add', 'per_info.image','per_info.session_id','per_info.r_id', 'city.name as city', 'territory.name as territory_name')
            ->where('r_id', '=', 2)
            ->orderBy('per_info.regId', 'DESC')
            ->get();

        $row = $data[0]->s_id;
        $idsArr = explode(',',$row);  
        $getFilterSupervisor = DB::table('per_info')
                                ->select('per_info.s_id', 'per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'per_info.email','per_info.vehicle_no','per_info.cnic', 'per_info.age','per_info.pri_add', 'per_info.image','per_info.session_id','per_info.r_id', 'city.name as city', 'territory.name as territory_name')
                                ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                                ->join('city', 'per_info.city_id', '=', 'city.city_id')
                                ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
                                ->where('per_info.r_id', '=', 2)
                                ->whereIn('user_services.s_id', $idsArr)
                                ->groupby('per_info.regId')
                                ->orderBy('per_info.regId', 'DESC')
                                ->get();
        if (count($getFilterSupervisor) != 0) {
            foreach($getFilterSupervisor as $expertise){
                        
                    $getExpertise = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select(DB::raw('group_concat(services.name) as expertise'))
                    ->where('r_id', '=', 2)
                    ->where('per_info.regId',$expertise->regId)
                    ->groupBy('per_info.regId')
                    ->first();

                    if ($getExpertise != null) {
                        $expertise->supervisor_expertise = $getExpertise->expertise;
                    }else{
                        $expertise->supervisor_expertise = '';
                    }

                        //getRatings
                        $kamayCount = personInfo::select('per_info *')
                            ->where('assigned_supervisor', $expertise->regId)
                            ->where('r_id', 1)
                            ->count();
                        $ratings = 0;
                        $expertise->worker_count = $kamayCount;
                        $expertise->supervisor_ratings = $ratings;
                        if ($kamayCount != 0) {
                            $rating = DB::table('orders')
                                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                                ->select(
                                                    DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                                    DB::raw('count(orders.order_id) as total_customers')
                                                    
                                                )
                                                ->where('per_info.assigned_supervisor',$expertise->regId)
                                                ->first();

                                if ($rating->total_customer_rating != null) {
                                    $ratings = $rating->total_customer_rating / $rating->total_customers;
                                }else{
                                    $ratings = 0;
                                }
                                $expertise->supervisor_ratings = $ratings;
                        }  
                        
            }       
        }
        if (count($getSupervisor) != 0) {
            foreach($getSupervisor as $expertise){
                      
                  $getExpertise = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select(DB::raw('group_concat(services.name) as expertise'))
                    ->where('r_id', '=', 2)
                    ->where('per_info.regId',$expertise->regId)
                    ->groupBy('per_info.regId')
                    ->first();

                    if ($getExpertise != null) {
                        $expertise->supervisor_expertise = $getExpertise->expertise;
                    }else{
                        $expertise->supervisor_expertise = '';
                    }

                      //getRatings
                      $kamayCount = personInfo::select('per_info *')
                          ->where('assigned_supervisor', $expertise->regId)
                          ->where('r_id', 1)
                          ->count();
                        $ratings = 0;
                        $expertise->worker_count = $kamayCount;
                        $expertise->supervisor_ratings = $ratings;
                       if ($kamayCount != 0) {
                            $rating = DB::table('orders')
                                              ->join('services', 'services.s_id', '=', 'orders.s_id')
                                              ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                              ->select(
                                                  DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                                  DB::raw('count(orders.order_id) as total_customers')
                                                 
                                              )
                                              ->where('per_info.assigned_supervisor',$expertise->regId)
                                              ->first();

                                if ($rating->total_customer_rating != null) {
                                  $ratings = $rating->total_customer_rating / $rating->total_customers;
                                }else{
                                  $ratings = 0;
                                }
                                $expertise->supervisor_ratings = $ratings;
                        }  
                        
            }       
        }
        $row = $data[0]->terr_id;
        $kama_territory = DB::table('kama_territory')
                            ->join('territory', 'territory.terr_id', 'kama_territory.terr_id')
                            ->where('kama_territory.reg_id', $data[0]->regId)
                            ->get();
        $kamay_area = array();
        foreach($kama_territory as $kama_territorys){
            array_push($kamay_area, $kama_territorys->terr_id);
        }
        $getAreaFilterSupervisor = DB::table('per_info')
                                    ->select('per_info.*', 'per_terri.name as per_terri_name')
                                    ->selectRaw('group_concat(territory.name) as kama_territory_name')
                                    ->join('kama_territory', 'kama_territory.reg_id', 'per_info.regId')
                                    ->join('territory', 'territory.terr_id', 'kama_territory.terr_id')
                                    ->join('territory as per_terri', 'per_info.terr_id', 'per_terri.terr_id')
                                    ->where('per_info.r_id', 2)
                                    ->where('per_info.status', 1)
                                    ->whereIn('kama_territory.terr_id', $kamay_area)
                                    ->orWhereIn('per_info.terr_id', $kamay_area)
                                    ->groupby('per_info.regId')
                                    ->orderby('per_info.regId', 'desc')
                                    ->get();
        if (count($getAreaFilterSupervisor) != 0) {
            foreach($getAreaFilterSupervisor as $expertise){
                        
                    $getExpertise = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select(DB::raw('group_concat(services.name) as expertise'))
                    ->where('r_id', '=', 2)
                    ->where('per_info.regId',$expertise->regId)
                    ->groupBy('per_info.regId')
                    ->first();

                    if ($getExpertise != null) {
                        $expertise->supervisor_expertise = $getExpertise->expertise;
                    }else{
                        $expertise->supervisor_expertise = '';
                    }

                        //getRatings
                        $kamayCount = personInfo::select('per_info *')
                            ->where('assigned_supervisor', $expertise->regId)
                            ->where('r_id', 1)
                            ->count();
                        $ratings = 0;
                        $expertise->worker_count = $kamayCount;
                        $expertise->supervisor_ratings = $ratings;
                        if ($kamayCount != 0) {
                            $rating = DB::table('orders')
                                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                                ->select(
                                                    DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                                    DB::raw('count(orders.order_id) as total_customers')
                                                    
                                                )
                                                ->where('per_info.assigned_supervisor',$expertise->regId)
                                                ->first();

                                if ($rating->total_customer_rating != null) {
                                    $ratings = $rating->total_customer_rating / $rating->total_customers;
                                }else{
                                    $ratings = 0;
                                }
                                $expertise->supervisor_ratings = $ratings;
                        }  
                        
            }       
        }
        $kamaCounter = $this->getKamayCounter();
        return view('kama.assigned-supervisor-list', compact('data','kamaCounter','getSupervisor', 'getFilterSupervisor', 'getAreaFilterSupervisor'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->cell_no = str_replace('-', '', $request->input('cell_no'));
        $validator = '';
        $validator = Validator::make($request->all(), [
                        'name'    =>  'required',
                        'cell_no'     =>  'required',
                        'selected_expertise' => 'required',
                        'address' => 'required',
                        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]);
        $format_cnic = str_replace('-', '', $request->input('cnic_number'));
        $format_cell = str_replace('-', '', $request->input('cell_no'));
        $check_number = personInfo::where('phn_no', $format_cell)->where('r_id', 1)->first();
        if($check_number != null){
            $validator->errors()->add('Phone', 'Phone number Already Exist');
        }
        $check_cnic = personInfo::where('cnic', $format_cnic)->where('r_id', 1)->first();
        if($check_cnic != null){
            $validator->errors()->add('CNIC', 'CNIC Number Already Exist');
        }

        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }

        if ($request->hasFile('image')) { 
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/customer'), $imageName);
            $imageName = url('images/customer/'. $imageName);
        }else{
                $imageName = '';
        }
          
        $getExpertise = implode(',', $request->input('selected_expertise'));
        
        $r_id = 1;
        $status = 0;
        $pri_lat = $request->input('lat');
        $pri_long = $request->input('long');
        $format_cnic = $format_cnic;
        $format_cell = $format_cell;
        $city_lower = strtolower($request->input('city'));
        $city = City::whereRaw('lower(name) like (?)',["%{$city_lower}%"])->first();
        $city_id = $city->city_id;
        $territory_lower = strtolower($request->input('area'));
        $territory =  territory::whereRaw('lower(name) like (?)',["%{$territory_lower}%"])->first();
        if($territory != null){
            $territory_id = $territory->terr_id;
        }else{
            $territory = new territory();
            $territory->name = $request->input('area');
            $territory->city_id = $city_id;
            $territory->status = 1;
            $territory->save();
            $territory_id = $territory->terr_id;
        }
        $userInfo = new personInfo([
            'fullName'              => $request->input('name'),
            'nickName'              => $request->input('nickname'),
            'phn_no'                => $format_cell,
            'email'                 => $request->input('email_id'),
            'vehicle_no'            => $request->input('vehicle'),
            'cnic'                  => $format_cnic,
            'city_id'               => $city_id,
            'terr_id'               => $territory_id,
            'pri_add'               => $request->input('address'),
            'r_id'                  => $r_id,
            'pri_lat'               => $pri_lat,
            'pri_long'              => $pri_long,
            'status'                => $status,
            'image'                 => $imageName,
            's_id'                  => $getExpertise,
            'callcenter_id'         =>  Auth::user()->id
        ]);

        $userInfo->save();

        $users = new Users(['regId' => $userInfo->regId, 'r_id' => $r_id]);
        $users->save();

        $parts = $request->input('selected_expertise');
        
        for($i = 0; $i < count($parts); $i++){
            DB::table('user_services')->insert(
                ['s_id' => $parts[$i], 'reg_id' => $userInfo->regId]
            );
        }

        $selected_terr = $request->input('selected_terr');
        if($selected_terr != null){
            for($i = 0; $i < count($selected_terr); $i++){
                $kama_territory = new KamaTerritory();
                $kama_territory->terr_id = $selected_terr[$i];
                $kama_territory->reg_id = $userInfo->regId;
                $kama_territory->save();
            }
        }

        return response()->json(['message' => 'Data Added successfully', 'data' => $request->input(), 'status' => true]);

     }

    public function finalApproval(Request $request, $id)
    {
        $validator = '';
        $validator = Validator::make($request->all(), [
                        'status'    =>  'required',
                        'cnic_number'     =>  'required',
                    ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        if ($request->input('status') == 'approved') {
            $statusName = $request->input('status');
            $status = 1;
        }else if ($request->input('status') == 'not_approved') {
            $statusName = $request->input('status');
            $status = 0;
        }else if ($request->input('status') == 'on_hold') {
            $statusName = $request->input('status');
            $status = 0;
        }else if ($request->input('status') == 'deactivate') {
            $statusName = $request->input('status');
            $status = 0;
        }else if ($request->input('status') == 'expired') {
            $statusName = $request->input('status');
            $status = 0;
        }
        $format_cnic_number = str_replace('-', '', $request->input('cnic_number'));
        $check_cnic = personInfo::where('cnic', $format_cnic_number)->where('r_id', 1)->first();
        if($check_cnic != null){
            if($check_cnic->regId != $id){
                $validator->errors()->add('CNIC', 'CNIC Number Already Exist');
            } 
        }
        if(count($validator->errors()) != 0){
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }
        $vehicle_no = $request->input('vehicle_no');

        $form_data = array(
                'status'   => $status,
                'comments'   => $request->input('comments'),
                'kamay_application_status'   => $statusName,
                'vehicle_no' => $vehicle_no,
                'cnic' => $format_cnic_number
        );

        $perinfo = personInfo::where('regId',$id)->update($form_data);
        
        $customerInfo = DB::table('per_info')
                            ->select('per_info.phn_no as cust_number', 'per_info.assigned_supervisor', 'per_info.regId', 'per_info.fullName')
                            ->where('per_info.regId', $id)
                            ->first();

        $getSupervisorNumber = DB::table('per_info')
                            ->select('per_info.phn_no as supervisor_number')
                            ->where('per_info.regId', $customerInfo->assigned_supervisor)
                            ->first();

        // dd($getSupervisorNumber->supervisor_number);die();
        //customer sms
        $toNumbersCsv = $customerInfo->cust_number;
        $messageText = "Kama has been ".$statusName." by backoffice";
        $string = preg_replace('/\s+/', '%20', $messageText);
        $response = file_get_contents("https://connect.jazzcmt.com/sendsms_url.html?Username=03060465709&Password=Hta123456&From=HTA&To=".$toNumbersCsv."&Message=".$string."");
        //supervisor sms
        
        $toNumberSup = $getSupervisorNumber->supervisor_number;
        $messageTextSup = "Kama has been ".$statusName." by backoffice";
        $stringSup = preg_replace('/\s+/', '%20', $messageTextSup);
        $responseSup = file_get_contents("https://connect.jazzcmt.com/sendsms_url.html?Username=03060465709&Password=Hta123456&From=HTA&To=".$toNumberSup."&Message=".$stringSup."");
        
        $getKamaFirbaseToken = DB::table('users')->select('users.firebase_token')
                        ->where('users.regId', '=', $customerInfo->assigned_supervisor)
                        ->first();

        $getNotification = DB::table('notifications')->where('id', 17)->first();
        $getbody = $getNotification->body;

        $search1 = '$statusName';
        $search2 = '$customerInfo->regId';
        $search3 = '$customerInfo->fullName';
        $getbody = str_replace($search1, $statusName, $getbody) ;
        $getbody = str_replace($search2, $customerInfo->regId, $getbody);
        $getbody = str_replace($search3, $customerInfo->fullName, $getbody);

        $title = $getNotification->title;
        $body = $getbody;
        
        // dd($getKamaFirbaseToken->firebase_token);die();
        $this->send_notification($getKamaFirbaseToken->firebase_token, null,null, null, null, $title, $body);
        return response()->json(['message' => 'Data Added successfully', 'data' => $request->input(), 'status' => true]);
    }

    public function show($id)
    {

         

        $data = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->join('roles', 'kamay_list.r_id','roles.r_id')
                    ->join('territory', 'kamay_list.terr_id', 'territory.terr_id')
                    ->select('kamay_list.*','kamay_list.image as cust_image','p2.fullName as supervisorName','territory.name as territory_name','kamay_list.verufied_by as supervisor_verified','p2.status as cstatus','kamay_list.created_at as rdate', 'roles.role_name', 'p2.regId as supervisor_id', 'p2.phn_no as supervisor_number', 'p2.pri_add as supervisor_address','p2.cnic as supervisor_nic', 'p2.email as supervisor_email', 'p2.image')
                    ->where('kamay_list.regId',$id)
                    ->get();
        //dd($data);die();
         $review = DB::table('review_checklist_status')
                  ->leftjoin('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
                                    ->select('status_name')
                                    ->where('review_checklist_status.reg_id', $id)
                                    ->where('review_checklist_status.review_checklist_id',10)
                                    ->first();
                        if ($review != null) {
                        $data[0]->application_status = $review->status_name;
                        }else{
                          $data[0]->application_status = 0;
                        }
                        // dump($data);die();
        $getexpertise = DB::table('kamay_list')
                    ->leftjoin('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
                    ->leftjoin('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                    ->leftjoin('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select( DB::raw('group_concat(services.name) as expertise'))
                    ->where('kamay_list.regId',$id)
                    ->groupBy('kamay_list.regId')
                    ->get();

        $getSupervisorExpertise = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select(DB::raw('group_concat(services.name) as expertise'))
                    ->where('r_id', '=', 2)
                    ->where('per_info.regId',$data[0]->supervisor_id)
                    ->groupBy('per_info.regId')
                    ->first();

        $supRatings = 0;
        $supRating = DB::table('orders')
                      ->join('services', 'services.s_id', '=', 'orders.s_id')
                      ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                      ->select(
                          DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                          DB::raw('count(orders.order_id) as total_customers')
                         
                      )
                      ->where('per_info.assigned_supervisor',$data[0]->supervisor_id)
                      ->first();

        if ($supRating->total_customer_rating != null) {
          $supRatings = $supRating->total_customer_rating / $supRating->total_customers;
        }else{
          $supRatings = 0;
        }



        $user = DB::table('per_info')->where('regId', $id)->first();
            if($user->rating_number != 0){
                $kamaRating = $user->rating_number / $user->rating_count;
            }else{
                $kamaRating = 0;
            }



  //get all supervisor for assigned kama
      
                $getSupervisorAssigned = DB::table('per_info')
                      ->join('city', 'per_info.city_id', '=', 'city.city_id')
                      ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
                      ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'per_info.email','per_info.vehicle_no','per_info.cnic', 'per_info.age','per_info.pri_add', 'per_info.image','per_info.session_id','per_info.r_id', 'city.name as city', 'territory.name as territory_name')
                      ->where('r_id', '=', 2)
                      ->get();
              // dump($getSupervisor);
              //             die();
            
            if (count($getSupervisorAssigned) != 0) {
                  foreach($getSupervisorAssigned as $expertise){
                            
                        $getExpertise = DB::table('per_info')
                          ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                          ->join('services', 'user_services.s_id', '=', 'services.s_id')
                          ->select(DB::raw('group_concat(services.name) as expertise'))
                          ->where('r_id', '=', 2)
                          ->where('per_info.regId',$expertise->regId)
                          ->groupBy('per_info.regId')
                          ->first();

                        if ($getExpertise != null) {
                            $expertise->supervisor_expertise = $getExpertise->expertise;
                        }else{
                          $expertise->supervisor_expertise = '';
                        }

                            //getRatings
                            $kamayCount = personInfo::select('per_info *')
                                ->where('assigned_supervisor', $expertise->regId)
                                ->where('r_id', 1)
                                ->count();
                              $ratings = 0;
                              $expertise->worker_count = $kamayCount;
                              $expertise->supervisor_ratings = $ratings;
                             if ($kamayCount != 0) {
                                  $rating = DB::table('orders')
                                                    ->join('services', 'services.s_id', '=', 'orders.s_id')
                                                    ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                                    ->select(
                                                        DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                                        DB::raw('count(orders.order_id) as total_customers')
                                                       
                                                    )
                                                    ->where('per_info.assigned_supervisor',$expertise->regId)
                                                    ->first();

                                      if ($rating->total_customer_rating != null) {
                                        $ratings = $rating->total_customer_rating / $rating->total_customers;
                                      }else{
                                        $ratings = 0;
                                      }
                                      $expertise->supervisor_ratings = $ratings;
                                  //     dump($expertise->regId);
                                  // dump($ratings);
                                  // exit();
                              }  
                              
                  }
                   
                          

              }
  //end      




        $percentage = DB::table('review_checklist_status')
              ->join('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
              ->where('review_checklist_status.reg_id', $id)
              ->orWhere(function($query) {
                  $query->where('review_status.status_name', 'Completed')
                        ->where('review_status.status_name', 'Not required')
                        ->where('review_status.status_name', 'Approved');
              })
              ->count();
              $getKamaPercentage = $percentage * 10;
        //dump($getKamaPercentage);die();

         $reviewCheckList = DB::select( DB::raw("select a.name as doc_name, a.reg_id,a.id as doc_id, rcs.review_checklist_id, rcs.review_status as active_status
                       ,(select rsa.status_name FROM `review_status` rsa where rsa.id=rcs.review_status) active_status_name
                       , a.status_id_list,a.status_list
                        from (SELECT p.regid as reg_id,rc.id , rc.name,
                        
                        (select GROUP_CONCAT(rs.id) from review_status rs where rs.review_checklist_id=rc.id) status_id_list, 
                        
                        (select GROUP_CONCAT(rs.status_name) from review_status rs where rs.review_checklist_id=rc.id) status_list
                        FROM review_checklist rc,per_info p where p.r_id=1) a  
                        LEFT join 
                        review_checklist_status rcs
                        on rcs.reg_id=a.reg_id and rcs.review_checklist_id = a.id
                        where a.reg_id=:reg_id
                        ORDER BY a.id
                        "), array('reg_id' => $id));
        $approved_counter = array();
        $success_count = 0;
        for($i = 0; $i < count($reviewCheckList); $i++){
            if(($reviewCheckList[$i]->active_status_name == "Completed") || ($reviewCheckList[$i]->active_status_name == "Shared") || ($reviewCheckList[$i]->active_status_name == "Not required") || ($reviewCheckList[$i]->active_status_name == "Approved") || ($reviewCheckList[$i]->active_status_name == "Assign training")){
                $reviewCheckList[$i]->status_count = 1;
                $success_count++;
            }else{
                $reviewCheckList[$i]->status_count = 0;
            }
            $approved_counter[$i] = $reviewCheckList[$i]->status_count;
        }
        $check_approval = count(array_unique($approved_counter)) === 1 && end($approved_counter) === 1;
        return view('kama.view', compact('success_count', 'check_approval', 'data','reviewCheckList','getexpertise','kamaRating','getSupervisorExpertise','supRatings','getKamaPercentage','getSupervisorAssigned'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($regId)
    {
            
            $data = personInfo::select('*')->where('regId',$regId)->first();
            $cityData = City::where('status', 1)->get();
            $getTerritory = DB::table('territory')
                                ->select('terr_id','name')
                                ->where('city_id', $data->city_id)
                                ->get();
            $expertise = DB::table('services')->select('s_id','parent_id','name')->where('parent_id','=','0')->get();


            return view('kama.edit', compact('data','cityData','expertise','getTerritory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $regId)
    {
        $format_cnic = str_replace('-', '', $request->input('cnic_number'));
        $format_cell = str_replace('-', '', $request->input('cell_no'));
        $request->validate([
            'name'    =>  'required',
            'cnic_number'     =>  'required',
            'selected_expertise' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           
        ]);
        // dd($request->all());die();
        $check_number = personInfo::where('phn_no', $format_cell)->where('r_id', 2)->first();
        if($check_number != null){
            if($check_number->regId != $regId){
                return redirect()->back()->with('error', 'Phone Number Already Registered')->withInput();
            }
        }
        $check_cnic = personInfo::where('cnic', $format_cnic)->where('r_id', 2)->first();
        if($check_cnic != null){
            if($check_cnic->regId != $regId){
                return redirect()->back()->with('error', 'CNIC Number Already Registered')->withInput();;   
            }
        }
        $getExpertise = implode(',', $request->input('selected_expertise'));
        $r_id = 1;
        $pri_lat = $request->input('lat');
        $pri_long = $request->input('long');
        $city_lower = strtolower($request->input('city'));
        $city = City::whereRaw('lower(name) like (?)',["%{$city_lower}%"])->first();
        $city_id = $city->city_id;
        $territory_lower = strtolower($request->input('area'));
        $territory =  territory::whereRaw('lower(name) like (?)',["%{$territory_lower}%"])->first();
        if($territory != null){
            $territory_id = $territory->terr_id;
        }else{
            $territory = new territory();
            $territory->name = $request->input('area');
            $territory->city_id = $city_id;
            $territory->status = 1;
            $territory->save();
            $territory_id = $territory->terr_id;
        }

        $old_image = personInfo::where('regId', $regId)->first();
        if ($request->hasFile('image')) {            
            if(File::exists($old_image->image)) {
                File::delete($old_image->image);
            }
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/supervisor'), $imageName);
            $imageName = url('images/supervisor/'. $imageName);
        }else{
            $imageName = $old_image->image;
        }
        //dd($format_cnic);die();

            $form_data = array(
                'fullName'              => $request->input('name'),
                'nickname'              => $request->input('nickname'),
                'phn_no'                => $format_cell,
                'email'                 => $request->input('email_id'),
                'vehicle_no'            => $request->input('vehicle'),
                'cnic'                  => $format_cnic,
                'city_id'               => $city_id,
                'terr_id'               => $territory_id,
                'pri_add'               => $request->input('address'),
                'r_id'                  => $r_id,
                'pri_lat'               => $pri_lat,
                'pri_long'              => $pri_long,
                'image'                 => $imageName,
                's_id'                  => $getExpertise,
                'callcenter_id'         =>  Auth::user()->id
        );

        $perinfo = personInfo::where('regId',$regId)->update($form_data);
        
        
        DB::table('user_services')->where('reg_id', $regId)->delete();
        $parts = $request->input('selected_expertise');
        for($i = 0; $i < count($parts); $i++){
            DB::table('user_services')->insert(
                ['s_id' => $parts[$i], 'reg_id' => $regId]
            );
        }

        DB::table('kama_territory')->where('reg_id', $regId)->delete();
        $selected_terr = $request->input('selected_terr');
        if($selected_terr != null){
            for($i = 0; $i < count($selected_terr); $i++){
                $kama_territory = new KamaTerritory();
                $kama_territory->terr_id = $selected_terr[$i];
                $kama_territory->reg_id = $regId;
                $kama_territory->save();
            }   
        }
        return redirect()->back()->with('success', 'Data updated successfully.'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getArea(Request $request)
    {
        $data['area'] = territory::where("city_id",$request->city_id)->get(["name","terr_id"]);
        return response()->json($data);
    }

    public function getAreaByCityName(Request $request){
        $term = strtolower($request->city_id);
        $city = City::whereRaw('lower(name) like (?)',["%{$term}%"])->first();
        $data['area'] = territory::where("city_id",$city->city_id)->get(["name","terr_id"]);
        return response()->json($data);
    }

     public function assignSupervisor(Request $request){
       
        $validator = Validator::make($request->all(), [ 
            'supervisorId' => 'required', 
            'kamaId' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $regId = $request->input('kamaId');
        $assignDate = Carbon::today()->toDateString();

        $affected = personInfo::where('regId', $regId)->update([
                'assigned_supervisor' => $request->input('supervisorId'),
                'assigned_supervisor_date' => $assignDate,
                'callcenter_id' =>  Auth::user()->id,
            ]);

        $customerInfo = DB::table('per_info')
                            ->select('per_info.phn_no as cust_number','per_info.fullName')
                            ->where('per_info.regId', $regId)
                            ->first();

        $supervisorInfo = DB::table('per_info')
                            ->select('per_info.fullName as name', 'per_info.regId as supervisor_id','per_info.phn_no as supervisor_number')
                            ->where('per_info.regId', $request->input('supervisorId'))
                            ->first();
        //SMS
        $toNumbersCsv = $customerInfo->cust_number;
        if ($request->input('updateSupervisor') == 'updateSupervisor') {
          $messageText = $supervisorInfo->name . " supervisor has been changed successfully";
        }else{
            //send to kamay message
          $messageText = "You have been assigned Supervisor " . $supervisorInfo->name . " who will contact you shortly";
        }

        $string = preg_replace('/\s+/', '%20', $messageText);
        $response = file_get_contents("https://connect.jazzcmt.com/sendsms_url.html?Username=03060465709&Password=Hta123456&From=HTA&To=".$toNumbersCsv."&Message=".$string."");
        if(!Empty($response)){ 
            
        }
        //end SMS
         //supervisor sms
        
        $toNumberSup = $supervisorInfo->supervisor_number;
        //send to supervisor
        $messageTextSup = "You have been assigned as a Supervisor to newly registered worker " . $customerInfo->fullName . ". Please contact him within 24 hours for completing his next steps";
        $stringSup = preg_replace('/\s+/', '%20', $messageTextSup);
        $responseSup = file_get_contents("https://connect.jazzcmt.com/sendsms_url.html?Username=03060465709&Password=Hta123456&From=HTA&To=".$toNumberSup."&Message=".$stringSup."");

        //Push Notification
        $getKamaFirbaseToken = DB::table('users')->select('users.firebase_token')
                        ->where('users.regId', '=', $supervisorInfo->supervisor_id)
                        ->first();
        $getNotification = DB::table('notifications')->where('id', 16)->first();
        $getbody = $getNotification->body;

        $search2 = '$customerInfo->fullName';
        $getbody = str_replace($search2, $customerInfo->fullName, $getbody) ;

        $title = $getNotification->title;
        $body = $getbody;
        $this->send_notification($getKamaFirbaseToken->firebase_token, null,null, null, null, $title, $body, 'kama_assigned');
        return redirect('kamayrequest')->with('success', 'Supervisor Assign successfull.');
    }

    public function getKamayCounter(){

      $kamaCounter = DB::select( DB::raw("select sum(pending) 'pending',
                        sum(new_kamay) 'newKama',
                        sum(approved) 'approved',
                        sum(all_Kamay) 'AllWorker' from
                        (select 
                        (case when status='pending' then 1 else 0 end) 'pending'
                        ,(case when status='new kamay' then 1 else 0 end) 'new_kamay'
                        ,(case when status='approved' then 1 else 0 end) 'approved'
                        , 1 'all_Kamay'
                        FROM kamay_list) A"));
      return $kamaCounter;
    }




    public function send_notification($token, $fullname, $longitude, $latitude, $total_amount, $title, $body, $flag = null)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        if($flag == null){
            $flag = "";
        }
        //$token = "eGK3k4t8RBiKmyiqQQjjjY:APA91bFex_QPCX1OluKDUEKAz4jNszwBJDZuu1wQz-r9YnwIdOzg9jit1vX8KsuWKiVUc_lNv1CJ7CTEWZO6OGOusAj4k8dNSMybeB29EmEJIgCpYfIBnKH98RT3Co_M51VbS3X2GT3J";
        //// $token = "fYTLx-sHS96IpD_S8ZEHrW:APA91bGL4tPHiM-OjocfXPfkO79hOuIT9CsWCcc50CGyDznPs_WjptJn52t4pwVt6Ea9D2Y4UGXYMS9PwiFt74WfYVNM1grAbLeND-BhicxqgjvLk7z2vKTDFcgWYY70UlBsQZYpEHML";
        // $serverKey = 'AAAAPai5eTo:APA91bGAS-xDMQpSytsstRwKcOrHnug3CyGjQ1vdqwYi2t2g0Ne1dpO59lW7oQXtDVKQazWhK-KUu4bjmC2L932rUFIajcF8ddhFzMQ6zIVP-hkavs8e-huXwwEzEmqLF_bpkud3tsup';
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
       
        // if($fullname != null){
        //     $title = "New Kama Assigned";
        //     $flag = "New kama";
        //     $body = "New kama " .$fullname. " has been assign to you!";
        // }else{
        //     $title = "New Kama Assigned";
        //     $flag = "New kama";
        //     $body = "New kama " .$fullname. " has been assign to you!";
        // }
        
        
        $data = array(
            // "order" => $order,
            "title" => $title,
            "body" => $body,
            "flag" => $flag

        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            //'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        //dd($json);exit();
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }

    
}
 
