<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use App\Orders;
use Validator;
use Auth;

class RequestController extends Controller
{
    public function index(){
        $orders = Orders::orderBy('order_id', 'DESC')->paginate(10);
        return view('request-orders.index', compact('orders'));
    }
}