<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use Validator;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function updateSlider(Request $request)

    {
       $session_id = $request->header('Authorization');
        if($session_id != ''){
            $user = DB::table('users')->where('session_id', $session_id)->first();

             if($user == ''){
                    return response()
                        ->json(array(
                        "status" => "false",
                        "message" => "Invalid session",
                        "data" => $user,
                    )); 
                }

                // $getBanners = DB::table('users as u')
                // ->join('vendor_store_sliders as v', 'u.id', '=', 'v.users_id')
                // ->select('v.id','v.slider as store_slider')->where('session_id','=',$session_id)->get();

                // if($getBanners[0]->store_slider != null) {
                //     foreach($getBanners as $bannersimg){
                //         $getAllSlider[] ='/assets/vendor/images/'. $bannersimg->store_slider;
                //     }
                //     return response()->json(array(
                //     "status" => "true",
                //     "message" => "Get banner",
                //     "data" => $getAllSlider,
                    
                // ));
                     
                //} 

            if ($request->hasFile('image')) {

                    // File::delete($getAllSlider);
                    DB::table('vendor_store_sliders')->where('users_id', $user->id)->delete();

                    foreach($request->file('image') as $image)
                    {
                        $name=$image->getClientOriginalName();
                        $imageName = time().$name; 
                        $image->move(public_path().'/assets/vendor/images/', $imageName);  
                         DB::table('vendor_store_sliders')->insert(
                                ['slider' => $imageName, 'users_id' => $user->id]
                            );

                    }

                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Update Image",
                        "data" => $imageName,
                    ));
               
            }else{
            return response()->json(array(
                "status" => "false",
                "message" => "Image not found",
                "data" => ''
            ));
            }
  
        }else{
            return response()->json(array(
                "status" => "false",
                "message" => "Invalid Authorization",
                "data" => null
            ));
        }
            
          
    }
    public function index()
    {


        $personCounters = DB::select( DB::raw("select 
                SUM(case when r_id=2 and status = 1 then 1 else 0 end) 'supervisors'
               ,SUM(case when r_id=1 then 1 else 0 end) 'kamay'
               ,SUM(case when r_id=3 and status = 1 then 1 else 0 end) 'customers'
               FROM per_info"));

        $jobCounter = DB::select( DB::raw("select 
                        sum(getUnassignedJobs) 'Unassigned',
                        sum(Assigned) 'Assigned',
                        sum(Cancelled) 'Cancelled',
                        sum(OnHold) 'OnHold',
                        sum(InProgress) 'InProgress',
                        sum(Completed) 'Completed',
                        sum(all_jobs) 'all_jobs' from
                        (select 
                        (case when status='getUnassignedJobs' then 1 else 0 end) 'getUnassignedJobs'
                        ,(case when status='Assigned' then 1 else 0 end) 'Assigned'
                        ,(case when status='Cancelled' then 1 else 0 end) 'Cancelled'
                        ,(case when status='OnHold' then 1 else 0 end) 'OnHold'
                        ,(case when status='InProgress' then 1 else 0 end) 'InProgress'
                        ,(case when status='Completed' then 1 else 0 end) 'Completed'
                        , 1 'all_jobs'
                        FROM order_info_v) A"));

                $data = DB::table('order_info_v')
                ->limit(5000)
                ->where('status','InProgress')
                    ->get();

        $newKamayReq = DB::table('kamay_list')->join('city', 'kamay_list.city_id', '=', 'city.city_id')->select('kamay_list.*','city.name')->where('kamay_list.status','new kamay')->limit(2)->get();

        $unAssignedJobs = DB::table('order_info_v')
                ->limit(2)
                ->where('status','getUnassignedJobs')
                ->whereNull('assigned_worker')
                    ->get();


        $panicReported = DB::table('panic_reported')
                    ->join('per_info', 'panic_reported.reg_id', 'per_info.regId')
                    ->join('roles', 'per_info.r_id', 'roles.r_id')
                    ->select('per_info.*','panic_reported.id as pid','panic_reported.order_id as job_id','panic_reported.status as pstatus','panic_reported.reason as panic_reason','panic_reported.created_at as pdate','roles.role_name')
                    ->where('panic_reported.status',2)
                    ->limit(5)
                    ->get();

        $schedule = DB::table('schedule_orders')->count();

        return view('home', compact('personCounters','jobCounter','newKamayReq','unAssignedJobs','panicReported','schedule'));
    }




}
