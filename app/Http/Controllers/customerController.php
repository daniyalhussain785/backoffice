<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\Orders;
use DB;
use Validator;
use Auth;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        //$data = personInfo::select('*')->where('r_id',3)->get();

         $data = DB::table('per_info')->join('city', 'per_info.city_id', '=', 'city.city_id')
                ->join('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'territory.name as area','per_info.pri_add','per_info.cnic', 'per_info.email', 'per_info.age', 'per_info.image','per_info.created_at as register_date', 'city.name as city')
                ->where([
                    ['r_id', 3],
                    ])
                ->get();
            
        if (count($data) != 0) {
            
            foreach ($data as $customer) {
                $count = DB::table('orders')
                            ->select('order_id')
                            ->where('orders.regId',$customer->regId)
                            ->count();
            $customer->orders_count = $count;
            }

                      
        }
        // dump($data);die();
           
        
 

                 return view('customer.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = personInfo::find($id);
        $data->completed_order = Orders::where('status', 'Completed')
                                    ->where('regId', $id)
                                    ->count();
        $data->completed_not_order = Orders::where('status', '!=' ,'Completed')
                                        ->where('regId', $id)
                                        ->count();
        $data->amount_recieved = Orders::where('regId', $id)
                                    ->where('amount_received', '!=', null)
                                    ->sum('amount_received');
        $data->amount_not_recieved = Orders::where('status', '!=' ,'Completed')
                                    ->where('regId', $id)
                                    ->sum('total_amount');
        $data->total_amount = Orders::where('regId', $id)
                                ->sum('total_amount');
        return view('customer.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
