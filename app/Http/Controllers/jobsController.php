<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\Orders;
use App\RequestOrder;
use DB;
use Carbon\Carbon;
use Validator;
use Auth;

class jobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {

        $this->middleware('auth');
    } 

    public function index()
    {
               
    }
    public function viewAssginedJobs($order_id)
    {
        $data = DB::table('order_info_v')
                    ->where('order_id',$order_id)
                    ->get();
        $order = Orders::where('order_id', $order_id)->first();
        
        $allKamay = DB::select(
            'SELECT status, s_id, verufied_by, regId as cust_id , fullName as cust_name , r_id ,image as cust_image , phn_no as cust_number, pri_lat, job_status, pri_add as address, pri_long, terr_id , city_id FROM
                per_info
                WHERE r_id = 1
                AND job_status = 1
                AND verufied_by IS NOT NULL
                AND status = 1
                AND regId NOT IN (SELECT worker_id FROM order_info_v WHERE status = "InProgress" and worker_id is not null)
                AND FIND_IN_SET(' .$order->s_id .' , s_id) > 0;
            ');
        foreach($allKamay as $allKamays){
            $allKamays->terr_name = DB::table('territory')->where('terr_id', $allKamays->terr_id)->first();
            $allKamays->city = DB::table('city')->where('city_id', $allKamays->city_id)->first();
        }

        /*Near By 3 */
        $circle_radius = 3959;
        $max_distance = 3;
        $lat = $order->latitude;
        $lng = $order->longitude;
        $sId = $order->s_id;

        
        $results = DB::select(
            'SELECT status, s_id, verufied_by, regId as cust_id , fullName as cust_name , r_id ,image as cust_image , phn_no as cust_number, pri_lat, job_status, pri_long, terr_id, city_id FROM
                per_info
                WHERE r_id = 1
                AND job_status = 1
                AND verufied_by IS NOT NULL
                AND status = 1
                AND regId NOT IN (SELECT worker_id FROM order_info_v WHERE status = "InProgress" and worker_id is not null)
                AND regId not in (select b.reg_id FROM request_orders b WHERE b.order_id = ' . $order_id . ')
                AND regId NOT IN (select c.reg_id FROM request_orders c WHERE c.flag = 1)
                AND FIND_IN_SET(' .$sId .' , s_id) > 0;
            ');
        $distance_array = array() ;           
        for($i = 0; $i < count($results); $i++){
            $per_lat = $results[$i]->pri_lat;
            $pri_long = $results[$i]->pri_long;
            $per = $per_lat.','.$pri_long;
            $order_lat = $lat.','.$lng;
            $distance = $this->googleMapFunction($per, $order_lat);
            $distance_pieces = explode(" ", $distance);
            if ($distance_pieces[0] <= 3) {
                $distance_array[] = array(
                                        "distance" => $distance_pieces[0],
                                        "unit" => $distance_pieces[1],
                                        "id" => $i,
                                        "status" => $results[$i]->status,
                                        "s_id" => $results[$i]->s_id,
                                        "verufied_by" => $results[$i]->verufied_by,
                                        "cust_id" => $results[$i]->cust_id,
                                        "cust_name" => $results[$i]->cust_name,
                                        "r_id" => $results[$i]->r_id,
                                        "cust_image" => $results[$i]->cust_image,
                                        "job_status" => $results[$i]->job_status,
                                        "cust_number" => $results[$i]->cust_number,
                                        "terr_id" => $results[$i]->terr_id,
                                        "city_id" => $results[$i]->city_id,
                                    );
            }
            
        }
        
        
        foreach ($distance_array as $key => $row) {
            $distance[$key]  = $row['distance'];
            $id[$key] = $row['id'];
        }
        $distance  = array_column($distance_array, 'distance');
        $id = array_column($distance_array, 'id');
        array_multisort($distance, SORT_ASC, $id, SORT_ASC, $distance_array);
        $getNearByKamay = $distance_array;
        for($i = 0; $i < count($getNearByKamay); $i++){
            $getNearByKamay[$i]['terr_name'] = DB::table('territory')->where('terr_id', $getNearByKamay[$i]['terr_id'])->first();
            $getNearByKamay[$i]['city_name'] = DB::table('city')->where('city_id', $getNearByKamay[$i]['city_id'])->first();
        }

        return view('jobs.assign-job',compact('order', 'allKamay', 'getNearByKamay'));
    }

    public function googleMapFunction($from_latlong, $to_latlong){
        // $from_latlong = '24.86631,67.07232';
        // $to_latlong = '24.87298,67.06055';

        $distance_data = file_get_contents(
                                'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='.$from_latlong.'&destinations='.$to_latlong.'&key=AIzaSyAggB51ZRomTQ28c6SIlT0Zkjtd2boQ3V0'
                            );
        $distance_arr = json_decode($distance_data);
        $pieces = explode(" ", $distance_arr->rows[0]->elements[0]->distance->text);
        $number = (float) $pieces[0];
        if($pieces[1] == 'm'){
            $number = $number * 0.001;
        }
        return $number . ' km';
        
    }
    
    public function unassignedJobSubmit(Request $request)
    {
        $assignedJob = Orders::where('order_id', $request->input('order_id'))->first(); 
        $assignedJob->request_time = Carbon::now();
        $assignedJob->assign_to = $request->input('kama_id');
        $assignedJob->status = 'Assigned';
        $assignedJob->save();

        $assignedAccept =  DB::table('assign_jobs')->insert([
                                'order_id' => $request->input('order_id'),
                                'regId' => $request->input('kama_id'),
                                'is_accept' => 1
                            ]);
        $order = DB::table('orders')
                    ->select('orders.*', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y %I:%i%p") as order_date'))
                    ->where('order_id', $request->input('order_id'))
                    ->first();
        $counter = 0;
        if(count($assignedJob->job_interest) != 0){
            foreach($assignedJob->job_interest as $job_interest){
                $user = DB::table('users')->where('regId', $job_interest->regId)->first();
                if($request->input('kama_id') != $job_interest->regId){
                    
                    $getNotification = DB::table('notifications')->where('id', 13)->first();
                    $search = '$order->order_id';
                    $getbody = str_replace($search, $order->order_id, $getNotification->body) ;
                    $body = $getbody;
                    $title = $getNotification->title;
                    $flag = "not_assigned";
                    $this->unassigned_send_notification($user->firebase_token,  $request->input('order_id'), $order->latitude, $order->longitude, null, $order->order_date, null, $flag, $body, $title);
                }else{
                    // $title = "Unassigned Job " . $request->input('order_id') . " assigned to you";
                    $getNotification = DB::table('notifications')->where('id', 14)->first();
                    $search = '$order->order_id';
                    $getbody = str_replace($search, $order->order_id, $getNotification->body) ;
                    $body = $getbody;
                    $title = $getNotification->title;

                    $flag = "Assigned";
                    $current_user = DB::table('per_info')->where('regId', $request->input('kama_id'))->first();
                    $per = $current_user->pri_lat.','.$current_user->pri_long;
                    $order_lat = $order->latitude.','.$order->longitude;
                    $distance = $this->googleMapFunction($per, $order_lat);
                    $distance_pieces = explode(" ", $distance);
                    $this->unassigned_send_notification($user->firebase_token,  $request->input('order_id'), $order->latitude, $order->longitude, null, $order->order_date, $distance_pieces[0] , $flag, $body, $title);
                }
                $counter = 1;
            }
        }

        if($counter == 0){
            $user = DB::table('users')->where('regId', $request->input('kama_id'))->first();
            $getNotification = DB::table('notifications')->where('id', 14)->first();
            $search = '$order->order_id';
            $getbody = str_replace($search, $order->order_id, $getNotification->body) ;
            $body = $getbody;
            $title = $getNotification->title;
            $flag = "Assigned";
            $current_user = DB::table('per_info')->where('regId', $request->input('kama_id'))->first();
            $per = $current_user->pri_lat.','.$current_user->pri_long;
            $order_lat = $order->latitude.','.$order->longitude;
            $distance = $this->googleMapFunction($per, $order_lat);
            $distance_pieces = explode(" ", $distance);
            $this->unassigned_send_notification($user->firebase_token,  $request->input('order_id'), $order->latitude, $order->longitude, null, $order->order_date, $distance_pieces[0] , $flag, $body, $title);
        }
        

        $getWorkerName = personInfo::select('fullName')
                          ->where('regId', $request->input('kama_id'))
                          ->where('r_id', 1)
                          ->first();

        if($request->input('customer_id') != null) {
            $getCustomerToken = DB::table('users')->where('regId', $request->input('customer_id'))->first();
            
            $getNotification = DB::table('notifications')->where('id', 15)->first();
            $getbody = $getNotification->body;

            $search = '$getWorkerName->fullName';
            $search2 = '$order->order_id';
            $getbody = str_replace($search, $getWorkerName->fullName, $getbody) ;
            $getbody = str_replace($search2, $order->order_id, $getbody);
            $body = $getbody;
            $title = $getNotification->title;
            $flag = "service";
            $this->unassigned_send_notification($getCustomerToken->firebase_token,  $request->input('order_id'), $order->latitude, $order->longitude, null, $order->order_date, null, $flag, $body, $title);
        }
        $getSupervisor = personInfo::select('assigned_supervisor')
                            ->where('regId', $request->input('kama_id'))
                            ->where('r_id', 1)
                            ->first();

        if ($getSupervisor != null) {
            $getSupervisorToken = DB::table('users')->where('regId', $getSupervisor->assigned_supervisor)->first();
            $body = "Your kama applied on job " . $request->input('order_id') . ", which has been assigned to him";
            $flag = "job_assigned";
            $this->unassigned_send_notification($getSupervisorToken->firebase_token,  $request->input('order_id'),  $order->latitude, $order->longitude, null, $order->order_date, null, $flag, $body);      
        }
       

        return redirect('view-unassigned')->with('success', 'job has been assigned successfully.');

        
    }
      
    public function getInprogress()
    {           
        $jobCounter = $this->getJobCounts();

                $data = DB::table('order_info_v')
                ->limit(5000)
                ->where('status','InProgress')
                    ->get();
               
        return view('jobs.view-inprogress',compact('data','jobCounter'));
    }

    public function getAllJobs()
    {           
        
        $jobCounter = $this->getJobCounts();
        $data = Orders::all();
        return view('jobs.all-jobs',compact('data','jobCounter'));
    }

    public function getJobsReport(Request $request)
    {           //dump($request->all());exit();
                $data = DB::table('order_info_v')
                ->limit(5000)
                //->where('status','InProgress')
                    ->get();
               
        return view('jobs.jobs-report',compact('data'));
    }
    public function getJobsReportRange(Request $request)
    {           


        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $data = DB::table('order_info_v')
                ->select('order_info_v.*')
                ->whereBetween('sdate' , [$from_date." 00:00:00", $to_date." 23:59:59"])
                ->get();
                
        //dump($data);exit();
               
               
        return view('jobs.desplay-report',compact('data','from_date','to_date'));
    }

    public function getTodayJobs()
    {           
        
        $jobCounter = $this->getJobCounts();
        
                $data = DB::table('order_info_v')
                ->limit(5000)
                ->whereDate('sdate', Carbon::today()->toDateString())
                    ->get();
          //echo Carbon::today()->toDateString();die(); 
        //  dump($data);die();    
        return view('jobs.today-jobs',compact('data','jobCounter'));
    }
    public function getCancelled()
    {           

                 $jobCounter = $this->getJobCounts();

                $data = DB::table('order_info_v')
                ->limit(5000)
                ->where('status','Cancelled')
                    ->get();
               
        return view('jobs.view-cancelled',compact('data','jobCounter'));
    }
    public function getUnassigned()
    {           
        $jobCounter = $this->getJobCounts();

        $data = DB::table('order_info_v')
                    ->limit(5000)
                    ->where('status','getUnassignedJobs')
                    ->whereNull('assigned_worker')
                    ->get();
               
        return view('jobs.view-unassigned',compact('data','jobCounter'));
    }
    public function getCompleted()
    {           
                $jobCounter = $this->getJobCounts();

                $data = DB::table('order_info_v')
                ->limit(5000)
                ->where('status','Completed')
                    ->get();
               
        return view('jobs.view-completed',compact('data','jobCounter'));
    }

    public function getOnHold()
    {           
                $jobCounter = $this->getJobCounts();

                $data = DB::table('order_info_v')
                ->limit(5000)
                ->where('status','OnHold')
                    ->get();
               
        return view('jobs.view-onhold',compact('data','jobCounter'));
    }

    public function getScheduled()
    {           
                $jobCounter = $this->getJobCounts();

                $data = DB::select("select `wi`.`regId` AS `worker_id`,`wi`.`fullName` AS `workername`,`ci`.`fullName` AS `customername`,`ci`.`pri_add` AS `cust_address`,`o`.`s_id` AS `s_id`,`s`.`name` AS `service_name`,`s`.`service_image` AS `service_image`,(select ifnull(group_concat(`s1`.`name`,'(',`o1`.`quantity`,') ' separator ','),'Only Visit Charges') from (`schedule_order_details` `o1` join `services` `s1`) where `o1`.`s_id` = `s1`.`s_id` and `o1`.`order_id` = `o`.`id`) AS `services`,`o`.`id` AS `order_id`,`o`.`payment_method` AS `payment_method`,`o`.`total_amount` AS `total_amount`,`o`.`full_address` AS `order_address`, DATE_FORMAT(`o`.`create_at`, '%d-%b-%y %h:%m %p') as sdate, `o`.`assign_to` AS `assigned_worker` from (((`schedule_orders` `o` left join `per_info` `wi` on(`wi`.`regId` = `o`.`assign_to`)) left join `per_info` `ci` on(`ci`.`regId` = `o`.`regId`)) left join `services` `s` on(`s`.`s_id` = `o`.`s_id`))");
               //dump($data);die();

        return view('jobs.view-scheduled',compact('data','jobCounter'));
    }

    public function getScheduleDetails($order_id){

        $order = DB::table('schedule_orders')
                                ->join('services', 'services.s_id', '=', 'schedule_orders.s_id')
                                ->select('id as order_id','regId', 'total_amount','full_address','services.name as service_name', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y") as service_date'), 'amount_received', 'payment_method', 'assign_to')
                                ->where('id', $order_id)
                                ->first();
                            
        $customer = DB::table('per_info')
                            ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                            ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                            ->select('image',
                                     'fullName as name',
                                     'per_info.regId',
                                     'per_info.email',
                                     'per_info.phn_no as cust_number',
                                     'city.name as location',
                                     'territory.name as area'
                            )
                            ->where('per_info.regId', $order->regId)
                            ->first();
                    
                    
        $service_details = DB::table('schedule_orders')
                ->join('schedule_order_details', 'schedule_order_details.order_id', '=', 'schedule_orders.id')
                ->join('services', 'services.s_id', '=', 'schedule_order_details.s_id')
                ->select('schedule_order_details.order_id',
                         'services.name', 
                         'schedule_order_details.quantity',
                         'schedule_orders.assign_to as assign_to'
                )
                ->where('schedule_orders.id', $order_id)
                ->get();

        // dump($order); 
        // dump($customer); 
        // dump($service_details);die(); 

        return view('jobs.schedule-details', compact('order','customer','service_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($order_id)
    {
        $data = Orders::where('order_id', $order_id)->first();   
        return view('jobs.view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function send_notification($token, $order_id, $longitude, $latitude, $total_amount, $created_at, $distance)
    {
        $url = "https://fcm.googleapis.com/fcm/send";

        //$token = "eGK3k4t8RBiKmyiqQQjjjY:APA91bFex_QPCX1OluKDUEKAz4jNszwBJDZuu1wQz-r9YnwIdOzg9jit1vX8KsuWKiVUc_lNv1CJ7CTEWZO6OGOusAj4k8dNSMybeB29EmEJIgCpYfIBnKH98RT3Co_M51VbS3X2GT3J";
        //// $token = "fYTLx-sHS96IpD_S8ZEHrW:APA91bGL4tPHiM-OjocfXPfkO79hOuIT9CsWCcc50CGyDznPs_WjptJn52t4pwVt6Ea9D2Y4UGXYMS9PwiFt74WfYVNM1grAbLeND-BhicxqgjvLk7z2vKTDFcgWYY70UlBsQZYpEHML";
        // $serverKey = 'AAAAPai5eTo:APA91bGAS-xDMQpSytsstRwKcOrHnug3CyGjQ1vdqwYi2t2g0Ne1dpO59lW7oQXtDVKQazWhK-KUu4bjmC2L932rUFIajcF8ddhFzMQ6zIVP-hkavs8e-huXwwEzEmqLF_bpkud3tsup';
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $title = "New Booking";
        $body = "new job arrived and Job id#" . $order_id;
        $order = DB::table('orders')->where('order_id',$order_id)->first();
        $service = DB::table('services')->where('s_id', $order->s_id)->first();
        $data = array(
            // "order" => $order,
            "order_id" => $order_id,
            "service_name" => $service->name,
            "longitude" => $longitude,
            "latitude" => $latitude,
            "total_amount" => $total_amount,
            "created_at" => $created_at,
            "service_image" => $service->service_image,
            "distance" => $distance

        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }

    public function getJobCounts()
    {           
        $jobCounter = DB::select( DB::raw("select 
                        sum(getUnassignedJobs) 'Unassigned',
                        sum(Assigned) 'Assigned',
                        sum(Cancelled) 'Cancelled',
                        sum(OnHold) 'OnHold',
                        sum(InProgress) 'InProgress',
                        sum(Completed) 'Completed',
                        sum(all_jobs) 'all_jobs' from
                        (select 
                        (case when status='getUnassignedJobs' then 1 else 0 end) 'getUnassignedJobs'
                        ,(case when status='Assigned' then 1 else 0 end) 'Assigned'
                        ,(case when status='Cancelled' then 1 else 0 end) 'Cancelled'
                        ,(case when status='OnHold' then 1 else 0 end) 'OnHold'
                        ,(case when status='InProgress' then 1 else 0 end) 'InProgress'
                        ,(case when status='Completed' then 1 else 0 end) 'Completed'
                        , 1 'all_jobs'
                        FROM order_info_v) A"));

        $schedule = DB::table('schedule_orders')->count();
        // dump( );exit(); 
        if ($schedule != 0) {
            $jobCounter['schedule'] = $schedule;
        }else{
             $jobCounter['schedule'] = 0;
        }
        
             // dump($jobCounter['schedule']);exit();
        return $jobCounter;
    }

    public function unassigned_send_notification($token, $order_id, $longitude, $latitude, $total_amount, $created_at, $distance, $flag, $body, $title = null)
    {
        $url = "https://fcm.googleapis.com/fcm/send";

        //$token = "eGK3k4t8RBiKmyiqQQjjjY:APA91bFex_QPCX1OluKDUEKAz4jNszwBJDZuu1wQz-r9YnwIdOzg9jit1vX8KsuWKiVUc_lNv1CJ7CTEWZO6OGOusAj4k8dNSMybeB29EmEJIgCpYfIBnKH98RT3Co_M51VbS3X2GT3J";
        //// $token = "fYTLx-sHS96IpD_S8ZEHrW:APA91bGL4tPHiM-OjocfXPfkO79hOuIT9CsWCcc50CGyDznPs_WjptJn52t4pwVt6Ea9D2Y4UGXYMS9PwiFt74WfYVNM1grAbLeND-BhicxqgjvLk7z2vKTDFcgWYY70UlBsQZYpEHML";
        // $serverKey = 'AAAAPai5eTo:APA91bGAS-xDMQpSytsstRwKcOrHnug3CyGjQ1vdqwYi2t2g0Ne1dpO59lW7oQXtDVKQazWhK-KUu4bjmC2L932rUFIajcF8ddhFzMQ6zIVP-hkavs8e-huXwwEzEmqLF_bpkud3tsup';
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        //$flag = "unassigned";
        $order = DB::table('orders')->where('order_id',$order_id)->first();
        $service = DB::table('services')->where('s_id', $order->s_id)->first();
        $data = array(
            // "order" => $order,
            "order_id" => $order_id,
            "service_name" => $service->name,
            "longitude" => $longitude,
            "latitude" => $latitude,
            "total_amount" => $total_amount,
            "created_at" => $created_at,
            "service_image" => $service->service_image,
            "distance" => $distance,
            "full_address" => $order->full_address,
            'service' => $service->name,
            'status' => $order->status,
            'payment_method' => $order->payment_method,
            "title" => $title,
            "body" => $body,
            "flag" => $flag

        );
        $notification = array(
            // 'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            //'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }

    public function statusUpdate(Request $request, $id){
        $status = $request->input('status');
        $order = Orders::where('order_id',$id)->first();
        if($status != 'continue'){
            $order->old_status = $order->status;
            $order->status = $request->input('status');

            //Request Order
            $request_order = RequestOrder::where('order_id', $id)->where('reg_id', $order->assign_to)->get();
            foreach($request_order as $request_orders){
                $request_orders->flag = 0;
                $request_orders->save();
            }

        }else{
            $order->status = $order->old_status;
        }
        $order->save();
        return redirect()->back()->with('success', 'Status Updated Successfully');
    }

}
