<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\City;
use App\Services;
use DB;
use Validator;
use File;
use Auth;
use App\Users;
use App\KamaTerritory;
use App\area;


class areaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        //$data = area::all();
        $data = area::join('territory', 'area_manage.terr_id', '=', 'territory.terr_id')
                ->select('territory.name as territory_name','area_manage.id','area_manage.status')
               ->get();
               // dd($data);die();     
			return view('area.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cityData = City::where('status', 1)->get();
        $expertise = Services::where('parent_id', 0)->where('s_id', '!=', 1)->get();
        $data = territory::all();
        // dd($data);die();
        return view('area.create', compact('cityData','expertise','data'));
    	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {

        
        $request->validate([
            'terr_id'    =>  'required',
            'status' => 'required'
           
           
        ]);
        // print_r($request->all() );exit();

        
        $area = new area();
        $area->terr_id = $request->input('terr_id');
        $area->status = $request->input('status');
        $area->save();

       

       
        return redirect('area')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
             $cityData = City::where('status', 1)->get();
             $data = area::where('id', $id)->first();
             $territory = territory::all();
             // dd($data);die();
            

            return view('area.edit', compact('cityData','data','territory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

         
        $request->validate([
             'terr_id'    =>  'required',
            'status' => 'required'
           
           
        ]);
        // print_r($request->all() );
       // dd($request->input('area'));exit();

            
            $area = area::find($id);

            $area->terr_id = $request->input('terr_id');
            $area->status = $request->input('status');

            $area->save();
       
        return redirect('area/'.$id.'/edit')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}