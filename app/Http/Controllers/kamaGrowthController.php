<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\KamaTerritory;
use DB;
use File;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Services;
use App\Users;
use App\City;
use App\Address;



class kamaGrowthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    } 


    public function kamaGrowth(){
      $data = personInfo::where('r_id', 1)->where('status',1)->get();
      return view('kama.worker-growth-potential', compact('data'));
    }

  

    
}
 
