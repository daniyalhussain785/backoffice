<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\Shares;
use App\personInfo;
use DB;
use Validator;
use Auth;


class ShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $data = Shares::all();
        $kama_share = Shares::find(6);
        $company_share = Shares::find(7);
        return view('service_share.index', compact('data', 'kama_share', 'company_share'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('service_share.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){        
        $validator = Validator::make($request->all(), [
            'label' => 'required',
            'status' => 'required',
            'label' => 'unique:shares,label',
        ]);
        if ($validator->fails()) {    
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }
        $share = new Shares();
        $share->label = $request->input('label');
        $share->status = $request->input('status');
        $share->save();
        return response()->json(['message' => 'Data Added successfully', 'data' => $request->input(), 'status' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($city_id)
    {
        $data = Shares::find($city_id);
        return view('service_share.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$city_id)
    {
        $validator = Validator::make($request->all(), [
            'label' => 'required',
            'status' => 'required',
            'label' => 'unique:shares,label,'. $city_id,
        ]);
        if ($validator->fails()) {    
            return response()->json(['data' => $validator->errors(), 'status' => false]);
        }
        $share = Shares::find($city_id);
        $share->label = $request->input('label');
        $share->status = $request->input('status');
        $share->save();
        return response()->json(['message' => 'Data Updated Successfully', 'data' => $request->input(), 'status' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}