<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use App\City;
use App\Services;
use DB;
use Validator;
use File;
use Auth;
use App\Users;
use App\KamaTerritory;

class supervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        
        $data = personInfo::join('city', 'per_info.city_id', '=', 'city.city_id')
                ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'territory.name as area','per_info.pri_add','per_info.cnic', 'per_info.email', 'per_info.age', 'per_info.image', 'city.name as city')
                ->where('r_id', 2)
                ->get();
        return view('supervisor.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cityData = City::where('status', 1)->get();
        $expertise = Services::where('parent_id', 0)->where('s_id', '!=', 1)->get();
        return view('supervisor.create', compact('cityData','expertise'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $format_cell = str_replace('-', '', $request->input('cell_no'));
        $format_cnic = str_replace('-', '', $request->input('cnic_number'));
        $request->validate([
            'supervisor_name' =>  'required',
            'cnic_number' => 'required',
            'selected_expertise' => 'required',
            'address' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $check_number = personInfo::where('phn_no', $format_cell)->where('r_id', 2)->first();
        if($check_number != null){
            return redirect()->back()->with('error', 'Phone Number Already Registered')->withInput();;   
        }

        $check_cnic = personInfo::where('cnic', $format_cnic)->where('r_id', 2)->first();
        if($check_cnic != null){
            return redirect()->back()->with('error', 'CNIC Number Already Registered')->withInput();;   
        }
        if ($request->hasFile('image')) { 
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/supervisor'), $imageName);
            $imageName = url('images/supervisor/'. $imageName);
        }else{
            $imageName = '';
        }
        $getExpertise = implode(',', $request->input('selected_expertise'));
        $r_id = 2;
        $status = 1;
        $pri_lat = $request->input('lat');
        $pri_long = $request->input('long');
        $city_lower = strtolower($request->input('city'));
        $city = City::whereRaw('lower(name) like (?)',["%{$city_lower}%"])->first();
        $city_id = $city->city_id;
        $territory_lower = strtolower($request->input('area'));
        $territory =  territory::whereRaw('lower(name) like (?)',["%{$territory_lower}%"])->first();
        if($territory != null){
            $territory_id = $territory->terr_id;
        }else{
            $territory = new territory();
            $territory->name = $request->input('area');
            $territory->city_id = $city_id;
            $territory->status = 1;
            $territory->save();
            $territory_id = $territory->terr_id;
        }

        $userInfo = new personInfo([
            'fullName'              => $request->input('supervisor_name'),
            'phn_no'                => $format_cell,
            'email'                 => $request->input('email_id'),
            'cnic'                  => $format_cnic,
            'city_id'               => $city_id,
            'terr_id'               => $territory_id,
            'pri_add'               => $request->input('address'),
            's_id'                  => $getExpertise,
            'r_id'                  => $r_id,
            'pri_lat'               => $pri_lat,
            'pri_long'              => $pri_long,
            'status'                => $status,
            'image'                 => $imageName,
            'callcenter_id'         =>  Auth::user()->id
        ]);

        $userInfo->save();
        $users = new Users(['regId' => $userInfo->regId, 'r_id' => $r_id]);
                    
                    $users->save();
        $parts = $request->input('selected_expertise');
        
        for($i = 0; $i < count($parts); $i++){
            DB::table('user_services')->insert(
                ['s_id' => $parts[$i], 'reg_id' => $userInfo->regId]
            );
        }
        
        $selected_terr = $request->input('selected_terr');
        if($selected_terr != 0){
            for($i = 0; $i < count($selected_terr); $i++){
                $kama_territory = new KamaTerritory();
                $kama_territory->terr_id = $selected_terr[$i];
                $kama_territory->reg_id = $userInfo->regId;
                $kama_territory->save();
            }
        }


        $messageText = "Your Supervisor account has been registered on kamay, Please login to your account";
        
        $this->sendSmsMessage($format_cell, $messageText);
        return redirect('supervisor')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = personInfo::join('city', 'per_info.city_id', '=', 'city.city_id')
                ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'territory.name as area','per_info.pri_add','per_info.cnic', 'per_info.email', 'per_info.age', 'per_info.image','per_info.created_at as register_date', 'city.name as city')
                ->where([
                    ['r_id', 2],
                    ['per_info.regId',$id]
                    ])
                ->get();


        $kamayCount = personInfo::select('per_info *')
                ->where('assigned_supervisor', $id)
                ->where('r_id', 1)
                ->count();
                
        $rating = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                    DB::raw('count(orders.order_id) as total_customers')
                                   
                                )
                                ->where('per_info.assigned_supervisor',$id)
                                ->first();
        //dump($rating);die();
        if($rating->total_customers != 0){
                    $ratings = $rating->total_customer_rating / $rating->total_customers;
                }else{
                    $ratings = 0;
                }
        
        $getSupervisorExpertise = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->select(DB::raw('group_concat(services.name) as expertise'))
                    ->where('r_id', '=', 2)
                    ->where('per_info.regId',$id)
                    ->groupBy('per_info.regId')
                    ->first();
        //dump($getSupervisorExpertise);die();


        return view('supervisor.view',compact('data','kamayCount','ratings','getSupervisorExpertise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($regId)
    {
        $data = personInfo::select('*')->where('regId',$regId)->first();
        $cityData = City::where('status', 1)->get();
        $expertise = Services::where('parent_id', 0)->where('s_id', '!=', 1)->get(); 
        return view('supervisor.edit', compact('data','cityData', 'expertise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$regId)
    {
        $format_cnic = str_replace('-', '', $request->input('cnic_number'));
        $format_cell = str_replace('-', '', $request->input('cell_no'));
        $request->validate([
            'supervisor_name' =>  'required',
            'cnic_number' => 'required',
            'selected_expertise' => 'required',
            'address' => 'required'
        ]);
        $check_number = personInfo::where('phn_no', $format_cell)->where('r_id', 2)->first();
        if($check_number != null){
            if($check_number->regId != $regId){
                return redirect()->back()->with('error', 'Phone Number Already Registered')->withInput();
            }
        }
        $check_cnic = personInfo::where('cnic', $format_cnic)->where('r_id', 2)->first();
        if($check_cnic != null){
            if($check_cnic->regId != $regId){
                return redirect()->back()->with('error', 'CNIC Number Already Registered')->withInput();
            }
        }
        $getExpertise = implode(',', $request->input('selected_expertise'));
        $r_id = 2;
        $status = 1;
        $pri_lat = $request->input('lat');
        $pri_long = $request->input('long');
        $city_lower = strtolower($request->input('city'));
        $city = City::whereRaw('lower(name) like (?)',["%{$city_lower}%"])->first();
        $city_id = $city->city_id;
        $territory_lower = strtolower($request->input('area'));
        $territory =  territory::whereRaw('lower(name) like (?)',["%{$territory_lower}%"])->first();
        if($territory != null){
            $territory_id = $territory->terr_id;
        }else{
            $territory = new territory();
            $territory->name = $request->input('area');
            $territory->city_id = $city_id;
            $territory->status = 1;
            $territory->save();
            $territory_id = $territory->terr_id;
        }

        $old_image = personInfo::where('regId', $regId)->first();
        if ($request->hasFile('image')) {            
            if(File::exists($old_image->image)) {
                File::delete($old_image->image);
            }
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/supervisor'), $imageName);
            $imageName = url('images/supervisor/'. $imageName);
        }else{
            $imageName = $old_image->image;
        }

        $form_data = array(
            'fullName'              => $request->input('supervisor_name'),
            'phn_no'                => $format_cell,
            'email'                 => $request->input('email_id'),
            'cnic'                  => $format_cnic,
            'city_id'               => $city_id,
            'terr_id'               => $territory_id,
            'pri_add'               => $request->input('address'),
            's_id'                  => $getExpertise,
            'r_id'                  => $r_id,
            'pri_lat'               => $pri_lat,
            'pri_long'              => $pri_long,
            'status'                => $status,
            'image'                 => $imageName,
            'callcenter_id'         =>  Auth::user()->id
        );
        $perinfo = personInfo::where('regId',$regId)->update($form_data);
        
        DB::table('user_services')->where('reg_id', $regId)->delete();
        $parts = $request->input('selected_expertise');
        for($i = 0; $i < count($parts); $i++){
            DB::table('user_services')->insert(
                ['s_id' => $parts[$i], 'reg_id' => $regId]
            );
        }
        DB::table('kama_territory')->where('reg_id', $regId)->delete();
        
        $selected_terr = $request->input('selected_terr');
        if($selected_terr != null){
            for($i = 0; $i < count($selected_terr); $i++){
                $kama_territory = new KamaTerritory();
                $kama_territory->terr_id = $selected_terr[$i];
                $kama_territory->reg_id = $regId;
                $kama_territory->save();
            }
        }
        return redirect('supervisor/'.$regId.'/edit')->with('success', 'Data updated successfully.');
    }

    public function sendSmsMessage($toNumbersCsv, $messageText){
        $string = preg_replace('/\s+/', '%20', $messageText);
        $response = file_get_contents("https://connect.jazzcmt.com/sendsms_url.html?Username=03060465709&Password=Hta123456&From=HTA&To=".$toNumbersCsv."&Message=".$string."");
        if(!Empty($response)){ 
            
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAssignedKamayToSupervisor($id)
    {

        // $data = DB::table('kamay_list')
        //         ->join('per_info as p2', 'kamay_list.assigned_supervisor', 'p2.regId')
        //         ->select('kamay_list.*','p2.fullName as supervisorName','p2.status as cstatus')->where('p2.regId',$id)
        //         ->get();

        $data = DB::table('kamay_list')
                ->join('user_services', 'user_services.reg_id', '=', 'kamay_list.regId')
                ->join('services', 'user_services.s_id', '=', 'services.s_id')
                ->select('kamay_list.*', DB::raw('group_concat(services.name) as expertise'))->where('kamay_list.assigned_supervisor',$id)
                ->groupBy('regId')
                ->get();

        $supervisorName = personInfo::select('fullName')
                ->where('regId',$id)
                ->first();

                 return view('supervisor.assigned-kamay-to-supervisor', compact('data','supervisorName'));
    }



}
