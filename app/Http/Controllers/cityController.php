<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use Validator;
use Auth;


class cityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $data = DB::table('city')->select('*')->where('status', 1)->get();
                    
			return view('city.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {

        
        $request->validate([
            'city_name'    =>  'required',
           
           
        ]);
        //print_r($request->all() );exit();

        
        $form_data = DB::table('city')->insert([
			    'name'              => $request->input('city_name'),
                'status'                => $request->input('status'),
                'country_id'                => 1
			]);

       

       
        return redirect('city')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($city_id)
    {
            
            $data = DB::table('city')->select('*')->where('city_id',$city_id)->first();
            

            return view('city.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$city_id)
    {

         
       $request->validate([
            'city_name'    =>  'required',
            'status'    =>  'required',
           
           
        ]);
        //print_r($request->all() );exit();

            
            $form_data = array(
                'name'              => $request->input('city_name'),
                'status'                => $request->input('status'),
                'country_id'                => 1
        	);

        $perinfo = DB::table('city')->where('city_id',$city_id)->update($form_data);
        
       
        return redirect('city')->with('success', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}