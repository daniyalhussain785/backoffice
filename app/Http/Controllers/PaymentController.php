<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\territory;
use App\personInfo;
use DB;
use App\Payment;
use Validator;
use Auth;

class PaymentController extends Controller
{
    public function index(){
        $payments = Payment::all();
        $jazzcash_credentials = Payment::find(2);
        return view('payment.index', compact('payments', 'jazzcash_credentials'));
    }

    public function update(Request $request, $id){
        $payment = Payment::find($id);
        if($id == 2){
            $request->validate([
                'merchant_id' => 'required',
                'password' => 'required',
                'integrity_salt' => 'required',
                'currency_code' => 'required',
                'version' => 'required',
                'language' => 'required',
            ]);
            if($request->input('sand_box') == null){
                $payment->sand_box = 0;
            }else{
                $payment->sand_box = 1;
            }
            $payment->merchant_id = $request->input('merchant_id');
            $payment->password = $request->input('password');
            $payment->integrity_salt = $request->input('integrity_salt');
            $payment->currency_code = $request->input('currency_code');
            $payment->version = $request->input('version');
            $payment->language = $request->input('language');
            $payment->save();
        }else{
            $request->validate([
                'name' => 'required',
            ]);
            $payment->name = $request->input('name');
            $payment->description = $request->input('description');
            $payment->status = $request->input('status');
            $payment->save();
        }
        
        return redirect()->back()->with('success', 'Payment Updated Successfully');
    } 
}