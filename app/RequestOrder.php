<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class RequestOrder extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'request_orders';
        protected $fillable = ['order_id', 'reg_id', 'distance', 'duration', 'created_at'];
        
        public function user(){
            return $this->hasOne('App\personInfo', 'regId','reg_id');
        }
    }