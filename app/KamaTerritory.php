<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KamaTerritory extends Model
{
    protected $table = 'kama_territory';
    protected $fillable = ['terr_id','reg_id'];

}
