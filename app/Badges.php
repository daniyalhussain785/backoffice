<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badges extends Model
{
    protected $table = 'badges';
    protected $primaryKey = 'b_id';
    protected $fillable = ['b_id', 'name','description','status', 'image', 'awarded_by', 'date_of_award', 'date_of_expiry'];

}
