<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class territory extends Model
{
    protected $table = 'territory';
    protected $primaryKey = 'terr_id';
    protected $fillable = ['terr_id', 'name','city_id','status'];
}
