<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\AdminNotify;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view) {
            $view->with('admin_notify', AdminNotify::whereIn('notify_status', [1,2])->orderBy('id', 'desc')->get());
            $view->with('admin_notify_count', AdminNotify::whereIn('notify_status', [1,2])->where('seen', 0)->count());
            $view->with('panic', AdminNotify::where('notify_status', 3)->orderBy('id', 'desc')->count());
            $view->with('panic_count', AdminNotify::where('notify_status', 3)->where('seen', 0)->count());
            $view->with('final_approval', AdminNotify::where('notify_status', 4)->orderBy('id', 'desc')->get());
            $view->with('final_approval_count', AdminNotify::where('notify_status', 4)->where('seen', 0)->count());
        });
    }
}
