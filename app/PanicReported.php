<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanicReported extends Model
{
    protected $table = 'panic_reported';
    protected $primaryKey = 'id';
    protected $fillable = ['order_id', 'reg_id','role_id','reason', 'status', 'resolved_by', 'status_name', 'supervisor_id'];

    public function user(){
        return $this->hasOne('App\personInfo' , 'regId', 'reg_id');
    }

    public function order(){
        return $this->hasOne('App\Orders' , 'order_id', 'order_id');
    }

}
