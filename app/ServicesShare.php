<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesShare extends Model
{
    protected $table = 'services_share';
    protected $fillable = ['id', 'service_id' ,'share_id', 'share_price','share_key','share_actual_price'];

    public function share(){
        return $this->hasOne("App\Shares","id","share_id");
    }
}
