<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class Payment extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'payment_option';
        protected $fillable = ['id', 'name', 'description', 'merchant_id', 'password', 'integrity_salt', 'currency_code', 'version', 'language', 'sand_box' ,'status'];
        
    }