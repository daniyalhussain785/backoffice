<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class OrdersDetails extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'order_details';
        protected $primaryKey = 'order_details_id';
        protected $fillable = ['order_details_id', 'order_id', 's_id', 'amount', 'quantity', 'add_work'];

        public function service(){
            return $this->hasOne('App\Services', 's_id','s_id');
        }

        public function order_share(){
            return $this->hasMany('App\OrderShare', 'order_details_id', 'order_details_id');
        }
        
    }