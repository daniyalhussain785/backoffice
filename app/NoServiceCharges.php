<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoServiceCharges extends Model
{
    protected $table = 'no_service_charges';
    protected $primaryKey = 'id';
    protected $fillable = ['main_ser_id', 'share_id','price','percentage'];
    
    public function share(){
        return $this->hasOne('App\Shares', 'id', 'share_id');
    }
}
