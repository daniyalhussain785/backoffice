<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    // protected $primaryKey = 'regId';
    public $timestamps = false;
    protected $fillable = ['regId','token','r_id','firebase_token' ];

}
