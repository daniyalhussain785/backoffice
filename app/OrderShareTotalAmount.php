<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

    class OrderShareTotalAmount extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'order_share_total_amount';
        protected $primaryKey = 'id';
        protected $fillable = ['order_id', 'share_id', 'total_amount'];

        public function share(){
            return $this->hasOne('App\Shares', 'id','share_id');
        }
    }